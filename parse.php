<?php
date_default_timezone_set('Europe/Berlin');

include '/var/customers/webs/Mirko/verwackeltes/test/cod4parser/core/misc/parser.php';
include '/var/customers/webs/Mirko/verwackeltes/test/cod4parser/core/extern/GameQ/GameQ.php';
include '/var/customers/webs/Mirko/verwackeltes/test/cod4parser/core/stats/process_players.php';
include '/var/customers/webs/Mirko/verwackeltes/test/cod4parser/core/stats/process_weapons.php';



function curl_get_file_size($url)
{
	$curl = curl_init($url);

	// Issue a HEAD request and follow any redirects.
	curl_setopt($curl, CURLOPT_NOBODY, true);
	curl_setopt($curl, CURLOPT_HEADER, true);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2450.0 Iron/46.0.2450.0');

	$data = curl_exec($curl);
	curl_close($curl);

	if($data)
	{
		$arr = explode("\n", $data);
		if(strpos($arr[0], "HTTP/1.1 302 Redirect") !== false)
		{
			if(strpos($arr[7], "HTTP/1.1 200 OK") !== false)
			{
				if(strpos($arr[8], "Content-Length:") !== false)
				{
					$s = substr($arr[8], 16);
					return intval($s);
				}
			}
		}
	}
	return false;
}

// Scann des Server
$server['cod'] = array('cod4', '37.120.178.217', '28960');
$gq = new GameQ();
$gq->addServers($server);
$data = $gq->requestData();

if(array_key_exists('mapname', $data['cod']))
{
	$clients = $data['cod']['mapname']." ".$data['cod']['g_gametype'];
}
else
{
	$clients = "";
}

if($clients == "") return;

$oldClients = file_get_contents('logs/parser_clients.log');
$multi_round_gtyles = array('sab', 'dom', 'ctf', 'ass', 'ta', 'tgr');
$split = explode(" ", $oldClients);

if(in_array($split[0], $multi_round_gtyles)) {

	if($split[1] == $data['cod']['g_gametype']) {
		$clients = $data['cod']['mapname'];
		$oldClients = $split[0];
	}
}

if($clients != $oldClients)
{
	//Prüfen ob Änderungen über 2000 bytes liegen.
	$newLength = curl_get_file_size("http://37.120.178.217/redirect/Mods/openwarfare_k4f/games_mp.log");
	$oldLength = file_get_contents("logs/games_mp_logsize.log");
	file_put_contents("logs/games_mp_logsize.log", $newLength);
	
	$diff = $newLength - $oldLength;

	if($diff <= 0 || $diff > 2000)
	{
	    $time = time();
        $logline = $time." ".$clients." ".$newLength." ".$oldLength." ".$diff." ";
        file_put_contents('logs/parser.log', $logliine, FILE_APPEND);

		//Datei holen und parsen
		exec("wget -q -O logs/games_mp_actual.log http://37.120.178.217/redirect/Mods/openwarfare_k4f/games_mp.log");
		$time = $time." ".$newLength." ".$clients." ".$diff."\n";
		file_put_contents('logs/games_mp_times.log', $time, FILE_APPEND);
		$game_start_id = parseFile("logs/games_mp_actual.log");
	}
}

$clients = $data['cod']['mapname']." ".$data['cod']['g_gametype'];
file_put_contents('logs/parser_clients.log', $clients);

if(isset($game_start_id))
{
	if($game_start_id > 0)
	{
        $game_start_id--;
		processPlayersPerRound($game_start_id);
		processPlayersChallenges($game_start_id);
		processWeaponsPerRound($game_start_id);
	}
}

?>