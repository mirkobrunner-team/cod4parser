ALTER stats_rounds_players ADD sum_killstreak_3 INT(4) NULL;
ALTER stats_rounds_players ADD sum_killstreak_6 INT(4) NULL;
ALTER stats_rounds_players ADD sum_killstreak_9 INT(4) NULL;
ALTER stats_rounds_players ADD sum_killstreak_12 INT(4) NULL;
ALTER stats_rounds_players ADD sum_deathstreak_3 INT(4) NULL;
ALTER stats_rounds_players ADD sum_deathstreak_6 INT(4) NULL;
ALTER stats_rounds_players ADD sum_deathstreak_9 INT(4) NULL;
ALTER stats_rounds_players ADD sum_deathstreak_12 INT(4) NULL;
ALTER stats_rounds_players ADD distance DOUBLE(14,6) NULL;

-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server Version:               10.1.19-MariaDB - mariadb.org binary distribution
-- Server Betriebssystem:        Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Exportiere Struktur von Tabelle mirkosql4.stats_weapons_all_players
CREATE TABLE IF NOT EXISTS `stats_weapons_all_players` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `gameid` int(11) NOT NULL DEFAULT '0',
    `playerid` int(11) NOT NULL DEFAULT '0',
    `w1` int(11) DEFAULT '0',
    `w2` int(11) DEFAULT '0',
    `w3` int(11) DEFAULT '0',
    `w4` int(11) DEFAULT '0',
    `w5` int(11) DEFAULT '0',
    `w6` int(11) DEFAULT '0',
    `w7` int(11) DEFAULT '0',
    `w8` int(11) DEFAULT '0',
    `w9` int(11) DEFAULT '0',
    `w10` int(11) DEFAULT '0',
    `w11` int(11) DEFAULT '0',
    `w12` int(11) DEFAULT '0',
    `w13` int(11) DEFAULT '0',
    `w14` int(11) DEFAULT '0',
    `w15` int(11) DEFAULT '0',
    `w16` int(11) DEFAULT '0',
    `w17` int(11) DEFAULT '0',
    `w18` int(11) DEFAULT '0',
    `w19` int(11) DEFAULT '0',
    `w20` int(11) DEFAULT '0',
    `w21` int(11) DEFAULT '0',
    `w22` int(11) DEFAULT '0',
    `w23` int(11) DEFAULT '0',
    `w24` int(11) DEFAULT '0',
    `w25` int(11) DEFAULT '0',
    `w26` int(11) DEFAULT '0',
    `w27` int(11) DEFAULT '0',
    `w28` int(11) DEFAULT '0',
    `w29` int(11) DEFAULT '0',
    `w30` int(11) DEFAULT '0',
    `w31` int(11) DEFAULT '0',
    `w32` int(11) DEFAULT '0',
    `w33` int(11) DEFAULT '0',
    `w34` int(11) DEFAULT '0',
    `w35` int(11) DEFAULT '0',
    `w36` int(11) DEFAULT '0',
    `w37` int(11) DEFAULT '0',
    `w38` int(11) DEFAULT '0',
    `w39` int(11) DEFAULT '0',
    `w40` int(11) DEFAULT '0',
    `w41` int(11) DEFAULT '0',
    `w42` int(11) DEFAULT '0',
    `w43` int(11) DEFAULT '0',
    `w44` int(11) DEFAULT '0',
    `w45` int(11) DEFAULT '0',
    `w46` int(11) DEFAULT '0',
    `w47` int(11) DEFAULT '0',
    `w48` int(11) DEFAULT '0',
    `w49` int(11) DEFAULT '0',
    `w50` int(11) DEFAULT '0',
    `w51` int(11) DEFAULT '0',
    `w52` int(11) DEFAULT '0',
    `w53` int(11) DEFAULT '0',
    `w54` int(11) DEFAULT '0',
    `w55` int(11) DEFAULT '0',
    `w56` int(11) DEFAULT '0',
    `w57` int(11) DEFAULT '0',
    `w58` int(11) DEFAULT '0',
    `w59` int(11) DEFAULT '0',
    `w60` int(11) DEFAULT '0',
    `w61` int(11) DEFAULT '0',
    `w62` int(11) DEFAULT '0',
    `w63` int(11) DEFAULT '0',
    `w64` int(11) DEFAULT '0',
    `w65` int(11) DEFAULT '0',
    `w66` int(11) DEFAULT '0',
    PRIMARY KEY (`id`),
    KEY `gameid` (`gameid`),
    KEY `playerid` (`playerid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Daten Export vom Benutzer nicht ausgewählt
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
