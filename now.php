<?php
include_once 'core/misc/parser.php';
include_once 'core/misc/class.extendedArray.php';
include_once 'core/extern/GameQ/GameQ.php';

$starttime = microtime_float();

if(lastCall() == false) return;

$maps = new ExtendedArray();
$types = new ExtendedArray();
$maps->fill('maps');
$types->fill('gametypes');


function lastCall()
{
	$t = file('logs/now_log.log');
	$time = $t[0];

	if($time > (time() -1))
	{
		$re = file_get_contents('logs/now_result.log');
		header('Content-Type: application/json');
		echo $re;
		return false;
	}
	
	file_put_contents('logs/now_log.log', "".time());
	return true;
}


function getServerStart()
{
	global $logs, $c;

	for($i=($c-1);$i>0;$i--)
	{
		if(processUnusableLines($logs[$i]) == true)
		{
			if(strpos($logs[$i], 'InitGame:') !== false)
			{
				return getServerStartFromLine($logs[$i]);
			}
		}
	}
}

function getServerStartFromLine(&$theLine)
{
	global $maps, $types;

	$sp1 = explode("\\", $theLine);
    $arr = array();
    $s = explode(" ", $sp1[0]);
    $field = "";
    $cd = count($sp1);

    for($i=0;$i<$cd;$i++)
    {
	    if($field == "g_gametype")
	    {
		   $arr['gametype'] = $types->getid($sp1[$i]);
		   $arr['teamplay'] = $types->getAditionalContentFromField("team", $arr['gametype']);
		   $arr['gametypename'] = $types->getLogName($arr['gametype']);
	    }

		if($field == "gamestartup")
		{
			$arr['time'] 	= strtotime($sp1[$i]);
		}

		if($field == "mapname")
		{
			$arr['map'] 	= $maps->getId( $sp1[$i]);

			if($maps->added == true)
			{
				$maps->addAditionalContentToField(extractMapNameFromLog($sp1[$i]), "name");
				$maps->added = false;
			}

			$arr['mapname'] = $maps->getAditionalContentFromField("name", $arr['map']);
		}

        if($field == '_ModUpdate')
        {
            $arr['modv'] =  $sp1[$i];
        }

        if($field == '_Maps')
        {
            $arr['mapsv'] =  $sp1[$i];
        }

		$field = $sp1[$i];
    }

	$arr['ltime'] 	= processLogTime($s[0]);

    return $arr;
}


//get basic server info´s
$servers['cod'] = array('cod4', '37.120.178.217', '28960');
$gq = new GameQ();
$gq->addServers($servers);
$data = $gq->requestData();

if(array_key_exists('players', $data['cod']))
{
	if(count($data['cod']['players']) == 0)
	{
		$now_retarray = array('server' => $data, 'game' => '', 'logline' => '', 'parsetime' => (microtime_float() - $starttime), 'memPeak' => memory_get_peak_usage(), 'memNorm' => memory_get_usage());
	
		$jsonString = json_encode($now_retarray);
		file_put_contents('logs/now_result.log', $jsonString);
	
		header('Content-Type: application/json');
		echo $jsonString;
		return;
	}
}
else
{
	$now_retarray = array('server' => $data, 'game' => '', 'logline' => '', 'parsetime' => (microtime_float() - $starttime), 'memPeak' => memory_get_peak_usage(), 'memNorm' => memory_get_usage());
	
	$jsonString = json_encode($now_retarray);
	file_put_contents('logs/now_result.log', $jsonString);
	
	header('Content-Type: application/json');
	echo $jsonString;
	return;
}




$pathToServer = "http://37.120.178.217/redirect/Mods/openwarfare_k4f/games_mp.log";
$pathToLocal = "logs/games_test.log";

$logs = file($pathToServer);
$c = count($logs);

$notAllowed = array('OWFYI' => 0, 'ExitLevel: executed' => 1, 'ShutdownGame:' => 2, '------------------------------------------------------------' => 0);

$line = splitLogLine($logs[$c-1]);
$serverVars = getServerStart();
$rline = array();

if(array_key_exists($line[1], $notAllowed))
{
	$rline[0] = $line[1];
}
else
{
	$line[0] = processLogTime($line[0]) - $serverVars['ltime'];
	$rline = $line;
}


$now_retarray = array('server' => $data, 'game' => $serverVars, 'logline' => $rline, 'parsetime' => (microtime_float() - $starttime), 'memPeak' => memory_get_peak_usage(), 'memNorm' => memory_get_usage());

$jsonString = json_encode($now_retarray);
file_put_contents('logs/now_result.log', $jsonString);

header('Content-Type: application/json');
echo $jsonString;

?>