<?php
require_once ('GameQ.php');

$servers['cod'] = array('cod4', '37.120.178.217', '28960');
$gq = new GameQ();

$gq->addServers($servers);
//$gq->setOption('raw', true);

// Request the data, and display it
//header('Cache-Control: no-cache, must-revalidate');
//header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Content-type: application/json');

try {
    $data = $gq->requestData();
	echo json_encode($data);
}
catch (GameQ_Exception $e)
{
    echo '{warning: "An error occurred"}';
}
	
?>
