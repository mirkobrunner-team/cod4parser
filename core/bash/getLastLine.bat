# Liest die letze Zeile aus der angegebenen Datei

powershell -nologo "& "Get-Content -Path c:\Path To\mp_games.log -Tail 1"

# besser eine Datei show.CMD erzeugen mit Obrigen Code.

# Das untere Script funktioniert ohne TAIL (kein windows standard)
# START

@echo OFF

:: Get the number of lines in the file
set LINES=0
for /f "delims==" %%I in (data.txt) do (
    set /a LINES=LINES+1
)

:: Print the last 10 lines (suggestion to use more courtsey of dmityugov)
set /a LINES=LINES-10
:: more +%LINES% < data.txt 
set LINE = +%LINES% < data.txt
echo LINE > line_clan.txt
# END