<?php

class ExtendedArray
{
	public $data;
	public $added;
    public $key;
    public $tablename;
    private $db;

    private $name_log;
    private $name;

    private $notfound;

    public $lastId;
    public $lastValue;

    public $count;

	public function __construct()
	{
		$this->data = array();
		$this->added = false;
		$this->db = new mbdb();

        $this->name_log = 'name_log';
        $this->name = 'name';
        $this->notfound = -1;
		$this->added = false;
        $this->lastId = -1;
        $this->lastValue = NULL;
        
        $this->count = 0;
	}


    public function setNotFound($v)
    {
        $old = $this->notfound;
        $this->notfound = $v;
        return $old;
    }

    public function getNotFound()
    {
        return $this->notfound;
    }

    public function setNameLog($v)
    {
        $old = $this->name_log;
        $this->name_log = $v;
        return $old;
    }

    public function getNameLog()
    {
        return $this->name_log;
    }

    public function setName($v)
    {
        $old = $this->name;
        $this->name = $v;
        return $old;
    }

    public function getNameKey()
    {
        return $this->name;
    }

    public function fillArrayWithCondition($tablename, $keyname, $where = "")
    {
        $this->key = $keyname;
        $this->tablename = $tablename;

        if($where <> "")
        {
            $where = " WHERE ".$where;
        }

        //because we will it fastes is possible
		$this->db->unsave_query("SELECT * FROM ".$tablename.$where);
		$dat = array();

		while($r = mysqli_fetch_assoc($this->db->result))
		{
	        $dat[$r[$keyname]] = $r;
    	}
    	$this->data = $dat;
    }

	public function fillArray($tablename, $keyname)
	{
        $this->fillArrayWithCondition($tablename, $keyname);
	}

    public function fill($table)
    {
        $this->fillArrayWithCondition($table, $this->name_log);

    }


	public function getId($value)
	{
	    if($this->key == NULL) return false;
		if($value == NULL) return;
		
		$this->added = false;
		
		if($this->lastValue == $value) return $this->lastId;
		
		$this->lastValue = $value;
		
		if(array_key_exists($value, $this->data))
	    {
	        $this->lastId = $this->data[$value]['id'];
	        return $this->lastId;
	    }
	    else
	    {
		    if($value == NULL) return;
	        // was neues!
	        $this->added = true;      
	        $this->addNewItem($value);
	        $this->fill($this->tablename);
            $this->lastId = $this->data[$value]['id'];

	        return $this->lastId;
	    }

        return -1;
	}

	public function getLogName($id)
	{
	    $ln = $this->name_log;

		foreach($this->data as $val)
		{
			if($id == $val['id']) return $val[$ln];
		}
	}
	
	public function getName($id)
	{
	    $n = $this->name;

		foreach($this->data as $val)
		{
			if($id == $val['id']) return $val[$n];
		}
	}
	
	public function addNewItem($value)
	{
		$table = $this->tablename;
		$key = $this->key;
		
		$sql = "INSERT INTO $table ($key) VALUE ('$value')";
		$this->db->unsave_query($sql);
	}
	
	public function addAditionalContentToField($content, $field, $id=NULL)
	{
	    //echo "addAditionalContentToField<br />";
		$lId = ($id == NULL) ? $this->db->get_last_id() : $id;
		$this->db->unsave_query("UPDATE ".$this->tablename." SET ".$field." = '$content' WHERE id = '$lId'");
        //echo "UPDATE ".$this->tablename." SET ".$field." = '$content' WHERE id = '$lId'<br /><br />";
	}
	
	public function getAditionalContentFromField($field, $id=NULL)
	{
		$lId = ($id == NULL) ? $this->db->get_last_id() : $id;
		$dat = $this->data;
		
		foreach($dat as $val)
		{
			if($id == $val['id']) return $val[$field];
		}
	}

    public function sortByField($field, $order = "<")
    {
        if(array_key_exists($fiels, $this->data[0]))

        $order = strtoupper($order);

		if($order == "ASC") $order = ">";
        else if($order == "DESC") $order = "<";

        usort($this->data, function($a, $b)
        {
            return (eval("{$a[$field]} {$order} {$b[$field]}"));
        });
    }
}


class Player extends ExtendedArray
{
	public function __construct()
	{
		parent::__construct();
		
		if(!isset($this->db))
		{
			$this->db = new serv_db();
		}
	}
	
	public function checkIfGIUDhasChanged($guid, $name)
	{
		// oki neue GUID oder Name doch schon vorhanden?
		if(array_key_exists($guid, $this->data)) return false;
		
		$dat = $this->data;

		foreach($dat as $val)
		{
			if($name == $val['hash'])
			{
				if($guid != $val['name_log'])
				{
					return $val['id'];
				}
				//oki den gibt es schon
				return false;
			}
		}
	}
	
	public function updatePlayersGUID($guid, $name, $id)
	{
		$dat = $this->data;
		
		if($id > 0)
		{
			foreach($dat as $val)
			{
				if($id == $val['id'])
				{
					$oldG = $val['name_log'];
					$oName = $val['hash'];
					$t = time();
					$m = $val['rang'];
			
					$this->db->unsave_query("INSERT INTO aliases_archive VALUES ('', '$oName', '$oldG', '$t', '$m')");
			
					$this->db->unsave_query("UPDATE aliases SET name_log = '$guid' WHERE hash = '$name'");

					$this->fill($this->tablename);

                    break;
				}
			}
			return true;
		}
		return false;
	}
}






?>