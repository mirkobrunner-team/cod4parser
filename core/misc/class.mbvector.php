<?php

class MBVector
{
    public function __construct()
    {
        $this->x = $this->y = $this-z = 0.0;

    }

    public function init($x, $y, $z)
    {
        $this->x = $x;
        $this->y = $y;
        $this->z = $z;
    }

    public function distanceTo($vector)
    {
        return $this->fast_distance($this->x, $this->y, $this->z, $vector->x, $vector->y, $vector->z);
    }

    public function fast_distance($x1, $y1, $z1, $x2, $y2, $z2)
    {
        $nx = $x2 - $x1;
        $ny = $y2 - $y1;
        $nz = $z2 - $z1;

        $nx*=$nx;
        $ny*=$ny;
        $nz*=$nz;

        $d = sqrt($nx + $ny + $nz);

        return $d;
    }

    public function normalizes()
    {
        $a = $this->fast_normalizes($this-x, $this-y, $this->z);

        $t = new MBVector();

        $t->x = $a[0];
        $t->y = $a[1];
        $t->z = $a[z];

        return $t;
    }

    public function fast_normalizes($x, $y, $z)
    {
    	$a = sqrt(pow($x, 2) + pow($y, 2) + pow($z, 2));
        $a = 1 / $a;

    	$x *= $a;
    	$y *= $a;
    	$z *= $a;

    	return array($x, $y, $z);
    }

    public function add(float $x, float, $y, float)
    {
        $this->x += $x;
        $this->y += $y;
        $this->z += $z;

        return $this;
    }

}







?>