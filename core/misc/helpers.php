<?php
//error_reporting(E_ALL);
//ini_set('error_reporting', E_ALL);

function d($o)
{
    echo '<pre>';
    var_dump($o);
    echo '</pre>';
}

function microtime_float()
{
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}


function secToTime($sek)
{
	$min = floor($sek / 60);
	$sek = $sek - $min * 60;

	$a = nullDavor($min);
	$b = nullDavor($sek);

	return $a.":".$b;
}



function nullDavor($num)
{
	if(strlen($num) == 1)
	{
		return "0".$num;
	}
	return $num;
}

function removeLastChar(&$str)
{
	return substr($str, 0, strlen($str)-1);
}


function checkSumPlayer($name)
{
	return trim($name);
}


function extractWeaponGroupName($name)
{
	$pos = strpos($name, "_");
	if(!$pos) return $name;
	return substr($name, 0, $pos);
}

//mp_bla_blub
function extractWeaponNameFromLog($name)
{
	$arr = explode("_", $name);
	$c = count($arr);
	
	if($c <= 1) return $name;
	
	$c--;
	
	if($c >= 1)
	{
		$ret = "";
		
		for($i=0;$i<$c;$i++)
		{
			if($i==0)
			{
				if(preg_match('#[0-9]#', $arr[$i]) == true)
				{
				    $ret.= strtoupper($arr[$i]);
				}
				else
				{
					$ret.= ucfirst($arr[$i]);
				}
			}
			else
			{
				$ret.= ucfirst($arr[$i]);
			}
			 		
			if($i<($c-1)) $ret.= " ";
		}		
		
		return $ret;
	}
}

function extractMapNameFromLog($name)
{
	$arr = explode("_", $name);
	$c = count($arr);
	
	if($c <= 1) return $name;
	
	if($c == 2)
	{
		return ucfirst($arr[1]);
	}
	else
	{
		$ret = "";
		
		for($i=1;$i<$c;$i++)
		{
			if($i==1)
			{
				if(strlen($arr[1]) < 4)
				{
					$ret.=strtoupper($arr[1]);
				}
				else
				{
					$ret.=ucfirst($arr[1]);
				}
			}
			else
			{
				$ret.= ucfirst($arr[$i]);
			}
			if($i<($c-1)) $ret.= " ";
		}		
		
		return $ret;
	}
}

function extractHitLocationFromLog($name)
{
	$arr = explode("_", $name);
	$c = count($arr);
	
	if($c <= 1) return $name;
	
	$ret = "";
		
	for($i=0;$i<$c;$i++)
	{
		$ret.= ucfirst($arr[$i]);
		if($i<($c-1)) $ret.= " ";
	}		
	
	return $ret;
}


function buildTable($arr)
{
	if(!isset($arr) || count($arr) == 0) return;
	
	$keys = array_keys(array_values($arr)[0]);
	
	$cont.= '<table>';
	$cont.= '<tr>';
	
	foreach($keys as $key)
	{
		$cont.= '<td>'.$key.'</td>';		
	}
	
	$cont.= '</tr>';
	
	foreach($arr as $r)
	{
		$cont.= '<tr>';
		
		foreach($keys as $key)
		{
			$cont.= '<td>'.$r[$key].'</td>';		
		}
		
		$cont.= '</tr>';
	}
	
	$cont.= '</table>';
	
	return $cont;
}


?>