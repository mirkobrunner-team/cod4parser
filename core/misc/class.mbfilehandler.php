<?php

class MBFileHandler
{
	public $conf_user;
	public $conf_pass;
	public $conf_host;
	public $conf_port;
	public $conf_destination_folder;
	public $conf_log_path_global;
	public $conf_log_path;
	public $conf_transaction_type;
	
	public $transaction_id;
	public $transaction_address;
	public $transaction_path;
	

	/*
	*	params: config array
	*				- user
	*				- pass
	*				- host
	*				- port
	*				- destination_fodlder
	*				- log file path
	*				- transaction_type (optional, scp is default)
	*/
	public function setConfig($config)
	{
		if(array_key_exists('user', $config))
		{
			$this->conf_user = $config['user'];
		}
		
		if(array_key_exists('pass', $config))
		{
			$this->conf_pass = $config['pass'];
		}
		
		if(array_key_exists('host', $config))
		{
			$this->conf_host = $config['host'];
		}
		
		if(array_key_exists('port', $config))
		{
			$this->conf_port = $config['port'];
		}
		
		if(array_key_exists('log_folder', $config))
		{
			$this->conf_log_path = $config['log_folder'];
		}
		
		if(array_key_exists('destination_folder', $config))
		{
			$this->conf_destination_folder = $config['destination_folder'];
		}
		
		if(array_key_exists('transaction_type', $config))
		{
			$this->conf_transaction_type = $config['transaction_type'];
		}
		
		if(array_key_exists('transaction_id', $config))
		{
			$this->transaction_id = $config['transaction_id'];
		}	
			
		if(array_key_exists('transaction_address', $config))
		{
			$this->transaction_address = $config['transaction_address'];
		}	
	}
	
	
	public function downloadFile($transactionId = "", $address = "", $protocol = "")
	{
		if($transactionId != "" && $transactionId != NULL)
		{
			$logFile = $transactionId.".log";
			$this->transaction_id = $transactionId;
		}
		else
		{
			$logFile = $this->transaction_id.".log";
		}
		
		if($address != "" && $address != NULL)
		{
			$this->transaction_address = $address;
		}
		else
		{
			//nichts was wir tun müssen	
		}
		
		
		if(isset($protocol) && $protocol != "")
		{
			if($this->_c_transaction_type($protocol) == true)
			{
				$this->conf_transaction_type = $protocol;
			}
			else
			{
				return "ERROR! Unsupported protocol specified!";
			}
		}
		
		
		if($this->conf_transaction_type != 'wget')
		{
			if(!isset($this->conf_user)) return "ERROR! User is not set!";
			if(!isset($this->conf_pass)) return "ERROR! Pass is not set!";
		}
		
	
		if(!isset($this->conf_port))
		{
			$this->conf_port = 22;
		}


		if(!isset($this->conf_host))
		{
			return "ERROR! Host is not set!";
		}
		
		if(!isset($this->conf_destination_folder))
		{
			return "ERROR! Destination Folder is not set!";
		}
		
		if($this->conf_log_path == "" || $this->conf_log_path == NULL)
		{
			$this->conf_log_path = $this->conf_destination_folder;
		}
		
		if(!isset($this->conf_transaction_type))
		{
			return "ERROR! Transaction Type is not set!";
		}
		
		if(!isset($this->conf_log_path)) 
		{
			return "ERROR! Log File Path is not set!";
		}
		
		if(!isset($this->transaction_id))
		{
			return "ERROR! Transaction ID is not set!";
		}
		
		$this->extractFileFromPath();
				
		$chk = false;

		
		if($this->conf_transaction_type == "scp")
		{
			$chk = $this->_d_scp();
		}
		else if($this->conf_transaction_type == "wget")
		{
			$chk = $this->_d_wget();
		}
		else if($this->conf_transaction_type == "ftp")
		{
			$chk = $this->_d_ftp();
		}
		else if($this->conf_transaction_type == "sftp")
		{
			$chk = $this->_d_sftp();
		}
		else
		{
			
		}
		
		if($chk == true)
		{
			return true;
		}
		else
		{
			return " During error´s!";
		}
	}
	

	private function _d_scp()
	{
		$conn = ssh2_connect($this->conf_host, $this->conf_port);
		
		if($conn == false)
		{
			return "ERROR! SFTP can`t reach server!";
		}
		
		ssh2_auth_password($conn, $this->conf_user, $this->conf_pass);

		$chk = ssh2_scp_recv($conn, $this->transaction_path.$this->transaction_address, $this->conf_destination_folder.$this->transaction_address);

		if($chk == false)
		{
			return "ERROR! SCP file download fails!";
		}

		return $chk;
	}

	private function _d_wget()
	{
		$command = "wget ".$this->conf_destination_folder."games_mp.log ".$this->conf_host.$this->transaction_address;

		$output;
		$returnVal;
		
		exec($command, $output, $returnVal);
	}

	private function _d_ftp()
	{
		$cid = ftp_connect($this->conf_host); 
		
		if($cid == false)
		{
			return "ERROR! FTP can`t reach server";
		}
		
		$login = ftp_login($cid, $this->conf_user, $this->conf_pass); 
		
		if($login == false)
		{
			return "ERROR! login failed";
		}
		
		// Achtung wir laden direkt in das Verzeichnis
		// Das ist nur zu Testzwecke
		$chk = ftp_chdir($cid, "logs/");
		
		if($chk == false)
		{
			$tmp = $this->conf_destination_folder;
			return "ERROR! FTP can´t open path: $tmp";
		}
		
		$this->d($this->conf_destination_folder);
		$this->d($this->transaction_address);
		
		$chk = ftp_get($cid, $this->conf_destination_folder.$this->transaction_address, $this->transaction_address, FTP_BINARY);

		if($chk == false)
		{
			$tmp = $this->transaction_address;
			return "ERROR! FTP can´t download file: $tmp";
		}
		
		ftp_close($cid);
	}
	
	
	
	private function _d_sftp()
	{
		$remote_file = $this->transaction_address;
		
		$conn = ssh2_connect($this->conf_host, $this->conf_port);
		
		if($conn == false)
		{
			return "ERROR! SFTP can`t reach server!";
		}
		
		ssh2_auth_password($conn, $this->conf_user, $this->conf_pass);
		$sftp = ssh2_sftp($conn);
		
		if($sftp == false)
		{
			return "ERROR! SFTP login failed!";
		}
		
		$stream = fopen("ssh2.sftp://$sftp/$remote_file", 'rb');
		
		if($stream == false)
		{
			return "ERROR! SFTP No such file or directory!";
		}
		
		$contents = fread($stream, filesize("ssh2.sftp://$sftp/$remote_file"));
		
		if($contents == false)
		{
			return "ERROR! SFTP Can´t read file from server!";
		}
		
        file_put_contents ($this->conf_destination_folder."/".$this->transaction_address, $contents);
        fclose($stream);
		
		return true;
	}
	
	
	
	public function unzipArchive()
	{
		if(!isset($this->transaction_id))
		{
			return "ERROR! UNZIP Transaction ID is not set!";
		}
		
		if(!isset($this->conf_destination_folder))
		{
			return "ERROR! UNZIP Destination Folder is not set!";
		}
		
		
		$zip = new ZipArchive();
		
		$chk = $zip->open($this->conf_destination_folder.$this->transaction_address);
		
		
	
		if($chk == false)
		{
			return "ERROR! No such file or directory!!";
		}
	
		$chk = $zip->extractTo($this->conf_destination_folder);
		
		if($chk == false)
		{
			return "ERROR! UNZIP Unable to extract archive";
		}
		
		$zip->close();
		
		return true;
	}
	
	public function zipFolder()
	{
		if(!isset($this->transaction_id))
		{
			return "ERROR! UNZIP Transaction ID is not set!";
		}
		
		if(!isset($this->conf_destination_folder))
		{
			return "ERROR! UNZIP Destination Folder is not set!";
		}
		
		$destination = $this->conf_destination_folder.$this->transaction_address;
		$source = $this->conf_destination_folder;
		
		$this->deleteIfFileExists($destination);
		
		$zip = new ZipArchive();
		
		
		$chk = $zip->open($destination, ZipArchive::CREATE);
		
		$files = new RecursiveIteratorIterator(
			new RecursiveDirectoryIterator($this->conf_destination_folder),
			RecursiveIteratorIterator::SELF_FIRST //LEAVES_ONLY
		);
		
		$source = str_replace('\\', '/', realpath($source));

	    if(is_dir($source) === true)
	    {
	        $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);
			
	        foreach($files as $file)
	        {
	            $file = str_replace('\\', '/', $file);
	            
	            if(in_array(substr($file, strrpos($file, '/')+1), array('.', '..')))
	            {
	                continue;
				}
	
	            $file = realpath($file);
	
	            if(is_dir($file) === true && basename($file) != $this->transaction_address)
	            {
	                $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
	            }
	            else if(is_file($file) === true)
	            {
	                $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
	            }
	        }
	    }
	    else if(is_file($source) === true)
	    {
	        $zip->addFromString(basename($source), file_get_contents($source));
	    }
		
		$zip->close();
		
		//remove files:
		$this->rrmdir($source);
		
		return true;
	}

	
	private function _c_transaction_type($t)
	{
		if($t == 'wget') return true;
		else if($t == 'scp') return true;
		else if($t == 'ftp') return true;
		else if($t == 'sftp') return true;
        else return false;
	}
	
	
	public function extractFileFromPath()
	{
		$file = strrchr($this->transaction_address, "/");
		$path = $this->transaction_address;
		
		$p = strrpos($path, "/");
		
		$this->transaction_path = substr($path, 0, $p+1);
		$this->transaction_address = substr($file, 1);
	}
	
	public function deleteIfFileExists($file)
	{
		if(file_exists($file))
		{
			unlink($file);
		}
		else
		{
		}
	}

	// Achtung LogDatei nicht löschen !!!!
	private function rrmdir($dir)
	{ 
		if(is_dir($dir))
		{ 
			$objects = scandir($dir);
			
			foreach($objects as $object)
			{ 
				if($object != "." && $object != "..")
				{ 
					if(filetype($dir."/".$object) == "dir")
					{
						$this->rrmdir($dir."/".$object); 
					}
					else
					{ 
						if($object != $this->transaction_address && $object !== $this->transaction_id.".log")
						{
							unlink($dir."/".$object);
						}
					}
				} 
			} 
			reset($objects); 
		} 
	}
	
	public function d($e)
	{
		echo '<pre>';
		var_dump($e);
		echo '</pre>';
	}
}
	
?>