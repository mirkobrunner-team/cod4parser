<?php
//error_reporting(E_ALL);
//ini_set('error_reporting', E_ALL);
date_default_timezone_set('Europe/Berlin');

include 'serv_db.inc.php';
include 'class.extendedArray.php';
include 'helpers.php';

$extrem_fast_log = true;   // bewahr hight memory usage possible


$last_game_time = 0;
$server_start_time = 0;
$last_log_init_time = 0;
$needupdate = false;
$exitReached = false;

$sql_query_actions = "INSERT INTO actions_full VALUES ";
$sql_query_rounds = "INSERT INTO rounds_full VALUES ";
$sql_query_teams = "INSERT INTO teams VALUES ";
$sql_query_games = "INSERT INTO games_full VALUES ";

$old_line = "";
$arr = array();
$db = new mbdb();

$counted_rounds = 0;
$counted_inserts = 0;
$game_index = 0;
$global_logtime = 0;


$players = new Player();
$weapons = new ExtendedArray();
$maps = new ExtendedArray();
$types = new ExtendedArray();
$hits = new ExtendedArray();
$gameActions = new ExtendedArray();
$mods = new ExtendedArray();
$spawnPoints = new ExtendedArray();

$players->fill('aliases');
$weapons->fill('weapons');
$maps->fill('maps');
$types->fill('gametypes');
$hits->fill('hitlocations');
$gameActions->fill('gameactions');
$mods->fill('mods');
$spawnPoints->fill('spawnpoints');

$roundBuffer = array();

function getLastLoggedTime()
{
    global $db;

	$db->query_db("SELECT gametime FROM logupdate ORDER BY id DESC LIMIT 1");
	$obj = mysqli_fetch_array($db->result);
	
	if($obj == 0 || $obj == NULL) return 0;
	
	return $obj['gametime'];
}

function getLastLoggesInfos()
{
	global $db;
	
	$db->query_db("SELECT * FROM logupdate ORDER BY id DESC LIMIT 1");
	$obj = mysqli_fetch_array($db->result);
	
	if($obj == 0 || $obj == NULL) return 0;
	
	return $obj;
}

function getLastIdFromActionsFull()
{
	global $db;
	
	$db->query_db("SELECT id FROM actions_full ORDER BY id DESC LIMIT 1");
	$a = mysqli_fetch_array($db->result);
	
	if(!$a) return 0;
	
	return $a['id'];
}

function getLastRoundId()
{
	global $db;
	
	$db->query_db("SELECT id FROM rounds_full ORDER BY id DESC LIMIT 1");
	$a = mysqli_fetch_array($db->result);

	if(!$a) return 1;
	$b = $a['id'];
	$b = $b +1;
	return $b;
}

function getLastGameId()
{
	global $db;
	
	$db->query_db("SELECT id FROM games_full ORDER BY id DESC LIMIT 1");
	$a = mysqli_fetch_array($db->result);
	if(!$a) return 1;
	$b = $a['id'];
	$b = $b +1;
	return $b;
}


/*
	return array
*/
function splitLogLine(&$buff)
{
	$arr = array();

	// first 14 char = time
	$p = strpos($buff, ":") + 4;
	$timeLine = trim(substr($buff, 0, $p));
	$rest = substr($buff, $p);
	$rest = str_replace(array("\r", "\n"), "", $rest);
	
	$arr = explode(";", $rest);
	
	array_unshift($arr, $timeLine);

	unset($timeLine);
	unset($rest);

	return $arr;
}


function saveQueryStrings()
{
    global $db, $sql_query_actions, $sql_query_teams;

    $sql_query_actions = removeLastChar($sql_query_actions);
    $db->unsave_query($sql_query_actions);

    $sql_query_teams = removeLastChar($sql_query_teams);
    $db->unsave_query($sql_query_teams);
    
   // $sql_query_games = substr($sql_query_games, 0, strlen($sql_query_games)-1);
   // $db->unsave_query($sql_query_games);
}



/*
	return true or false
*/
function processUnusableLines(&$buff)
{
	if($buff[0] == '#' || $buff[0] == '-') return false;
	return true;
}

/*
	return time in secounds
*/
function processLogTime(&$logtime)
{
	$ar = explode(":", $logtime);
	$time = $ar[0] * 60;
	$time+= $ar[1];
	unset($ar);
	return $time;
}

/*
	718:49 InitGame: \_Admin\Bloody/Painkiller/Slaughterman\_Email\info@k4f-in-berlin.de\_Location\Berlin/Germany\_Maps\K4F-Mappack\_ModUpdate\OpenWarfare Edit\_Website\k4f-in-berlin.de\fs_game\mods/openwarfare_k4f\g_compassShowEnemies\0\g_gametype\war\gamename\Call of Duty 4\gamestartup\12/12/2015 22:38:07\mapname\mp_sps_bismarck_s\protocol\6\shortversion\1.7\sv_allowAnonymous\0\sv_disableClientConsole\1\sv_floodprotect\1\sv_hostname\^1K4F ^7Berlin Warfare ^1HC\sv_maxclients\16\sv_maxPing\0\sv_maxRate\25000\sv_minPing\0\sv_privateClients\0\sv_punkbuster\1\sv_pure\1\sv_voice\0\ui_maxclients\32


	 11:31 InitGame: \_Admin\Bloody/Painkiller/Slaughterman\_Email\info@k4f-in-berlin.de\_Location\Berlin/Germany\_Maps\K4F-Mappack\_ModUpdate\OpenWarfare Edit\_Website\k4f-in-berlin.de\fs_game\mods/openwarfare_k4f\g_compassShowEnemies\0\g_gametype\dm\gamename\Call of Duty 4\gamestartup\10/24/15 21:31:53\mapname\mp_arbor_day\protocol\6\shortversion\1.7\sv_allowAnonymous\0\sv_disableClientConsole\1\sv_floodprotect\1\sv_hostname\^1K4F ^7(German Clan)\sv_maxclients\16\sv_maxPing\300\sv_maxRate\25000\sv_minPing\0\sv_privateClients\0\sv_punkbuster\1\sv_pure\1\sv_voice\0\ui_maxclients\32


*/
function processGameInit(&$linebuffer)
{
   	global $maps, $types, $counted_inserts, $server_start_time, $arr;

    $sp1 = explode("\\", $linebuffer);
    $arr = array();
    $s = explode(" ", $sp1[0]);
    $sc = count($s);
    $b = "";
    
    foreach($sp1 as $field)
    {
	    if($b == "g_gametype")
	    {
		   $arr['gametype'] = $types->getId($field);
		   $arr['teamplay'] = $types->getAditionalContentFromField("team", $arr['gametype']);
	    }
	    
		if($b == "gamestartup")
		{
			$arr['time'] 	= strtotime($field);
			$server_start_time = $arr['time'];
/*
            if($server_start_time <= 0)
            {
                $server_start_time = $arr['time'];
            }
*/
		}
		
		if($b == "mapname")
		{
			$arr['map'] 	= $maps->getId($field);

			if($maps->added == true)
			{
				$maps->addAditionalContentToField(extractMapNameFromLog($field), "name");	
				$maps->added = false;
			}
		}

        if($b == '_ModUpdate')
        {
            $arr['modv'] = $field;
        }

        if($b == '_Maps')
        {
            $arr['mapsv'] = $field;
        }
				
	    $b = $field;
    }

	if($sc == 3)
	{
		$arr['ltime'] = $s[0];
	}
	else if($sc == 4)
	{
		$arr['ltime'] = $s[1];
	}
	else if($sc == 5)
	{
		$arr['ltime'] = $s[2];
	}
   // $arr['ltime'] 	= $s[2];
	$arr['insert_id'] = $counted_inserts + 1;

    return $arr;
}

/*
	$starter_array given from processGameInit()
*/
function processGameShutdown($time, &$linebuffer, $starter_array, $rid, $roundtime, $action)
{
	global $sql_query_actions, $counted_inserts, $sql_query_rounds, $server_start_time, $last_game_time, $last_log_init_time, $counted_rounds, $roundBuffer, $sql_query_games;
	// hier  wissen wir ein spiel ist zu ende.
	// das Array aus processGameInit() kann hier gespeichert werden +
	// zusätztliche Informationen.

    /*
        $server_start_time = absolute server start zeit
        $time = reine runden zeit
        $roundtime = origzeit aus der Log

        Orig Spielzeit (start) $server_start_time + $roundtime
    */
    
	$sql_query_actions.= "('', '$rid', '$time', 'ShutdownGame', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),";

    $counted_inserts++;
	$last_game_time = $server_start_time + $last_log_init_time;
	
    processRoundEnd($last_game_time, $time, $line, $rid);
}


function processRoundEnd($time, $duration, &$line, $rid)
{
	global $sql_query_actions, $counted_inserts, $arr, $sql_query_rounds, $counted_rounds, $roundBuffer;

	//oki lass uns die Runde speichern
	$gm = $arr['gametype'];
	$map = $arr['map'];
	$ids = $arr['insert_id'];

	array_push($roundBuffer, array('map' => $map, 'type' => $gm, 'duration' => $duration, 'id' => $counted_rounds, 'time' => $time));

	if($duration > 0 && $time > 0)
	{
    	$sql_query_rounds.= "('', '$time', '$duration', '$gm', '$map', '$ids'),";
	}
}



function processGameExit($action, $time, &$line, &$db, $rid)
{
	global $sql_query_rounds, $counted_rounds, $roundBuffer, $sql_query_games, $game_index, $counted_inserts, $sql_query_actions;
	
	$time = $roundBuffer[0]['time'];
    $map = $roundBuffer[0]['map'];
    $type = $roundBuffer[0]['type'];
    
    $duration = 0;
    $rnds = "";
    
    foreach($roundBuffer as $round)
    {
	    $duration+= $round['duration'];
	    $rnds.= $round['id'].",";
    }

    $rnds = removeLastChar($rnds);

    $sql_query_games.= "('', '$time', '$rnds', '$map', '$type', '$duration'),";
    
    $roundBuffer = array();

    $game_index++;
}


/*
	
	Death by World:
	
349:51 D; 9c44fc29f2143c8517462f1ffb1dbbc7; 2; 	axis;	Partyatze	;									;-1	;world	;				;none		;	4;		MOD_FALLING;		none
349:53 K; 9c44fc29f2143c8517462f1ffb1dbbc7; 2; 	axis;	Partyatze	;									;-1	;world	;				;none		;	28;		MOD_FALLING;		none

197:39 D; 5d3985a6d77c8c410aaba50f30a7b179;	0;	axis;	Dusk		;									;-1	;world	;				;none		;	5;		MOD_TRIGGER_HURT;	none

normal:
342:47 K; 9c44fc29f2143c8517462f1ffb1dbbc7;	2;	axis;	Partyatze	;a87eae698712eac693bd208312b478e9	;1	;allies	;SlaughterMan	;barrett_mp	;	140	;	MOD_HEAD_SHOT;		head

ist am der selben Position wie Teamname beim Enemyplayer.

*/
function processKillDeathActions($action, $time, &$line, &$db, $rid)
{
	global $weapons, $hits, $players, $mods, $sql_query_actions, $counted_inserts;
	
	$puid =     $players->getId(checkSumPlayer($line[2])); //2 is Cod4 uid
	$pteam =    ($line[4] == 'axis') ? 0 : 1;
	$eteam =    ($line[8] == 'axis') ? 0 : 1;
	$ename =    $line[9];
	$euid =     $players->getId(checkSumPlayer($line[6])); //6 is Cod4 uid
	
	// das geht hier nicht sofort wir beauchen einen virtuellen Spieler 'world'
    //$eteam =    ($line[8] == 'world') ? $eteam : -1;
	if($line[8] == 'world')
	{
		//wir machen daraus ein Suizid / Unfall
		$eteam = $pteam;
		$euid = $puid;
	}
	
	$weapon =   $weapons->getId($line[10]);
	
	if($weapons->added == true)
	{
		computeWeaponGroup($weapon, $line[10]);
		$name = extractWeaponNameFromLog($line[10]);
		$weapons->addAditionalContentToField($name, 'name', $weapon);
		
		$weapons->added = false;
	}

	
	$health =   $line[11];
	$mod =      $mods->getId($line[12]);
	$hit =      $hits->getId($line[13]);
	
	if($hits->added == true)
	{
		$hits->addAditionalContentToField(extractHitLocationFromLog($line[13]), "name");
		$hits->added = false;
	}
	
	$x = $y = $z = 0;
	
	if(isset($line[14]))
	{
		$pos = explode(" ", $line[14]);
		
		$x = $pos[0];
		$y = $pos[1];
		$z = $pos[2];
	}
	
	$sql_query_actions.= "('', '$rid', '$time', '$action', '$puid', '$pteam', '', '$euid', '$eteam', '', '$weapon', '$health', '$mod', '$hit', '', '$x', '$y', '$z'),";
    $counted_inserts++;
}

/*
494:37 J;73c514a8d5f508b3d636d39d05e07602;2;Bloody
494:37 L;73c514a8d5f508b3d636d39d05e07602;2;Bloody
494:39 JT;58418a2336c205b453b2061d8da80454;1;axis;Drake;
*/
function processPlayerTeamActions($action, $time, &$line, &$db, $rid)
{
	global $players, $sql_query_actions, $counted_inserts, $arr, $sql_query_teams, $counted_rounds, $game_index, $types, $arr;
	
	$name = $line[4];
	$team = 0;

	$chks = "";

	if($action == 'JT')
	{
		$name = $line[5];
		$team = ($line[4] == 'axis') ? 0 : 1;
	    $auid = $players->getId($line[2]);
		
        $sql_query_teams.="('','$auid','".$counted_rounds."','$game_index','$time','$team'),";
	}
	

	if($name == 'axis' || $name == 'allies' || $name == '')
	{

	}
	else if($action == "J")
	{
		$test_id = $players->checkIfGIUDhasChanged($line[2], $line[4]);
		
		if($test_id > 0)
		{
			$players->updatePlayersGUID($line[2], $line[4], $test_id);
		}
		
		$auid = $players->getId(checkSumPlayer($line[2]));

		if($players->added == true)
		{
            $players->addAditionalContentToField($line[4], 'hash');
            $added = $arr['time'] + $global_logtime + $time;
            $players->addAditionalContentToField($added, 'added', $auid);
			$players->added = false;
		}
		else
		{
			$oName = $players->getAditionalContentFromField('hash', $auid);
			
			if($oName != $name && $oName != NULL)
			{
				$hash = $players->getLogName($auid);
				$players->addAditionalContentToField($name, 'hash', $auid);
				$ntime = time();

				//store old name with timestamp
				$db->unsave_query("INSERT INTO aliases_archive VALUES ('', '$oName', '$hash', '$ntime')");
				$players->fill('aliases');
			}
		}

		$sql_query_actions.= "('', '$rid', '$time', '$action', '$auid', '$team', '', '', '', '', '', '', '', '', '', '', '', ''),";
        $counted_inserts++;
	}
	else if($action == "L")
	{
		$auid = $players->getId(checkSumPlayer($line[2]));
		
		$sql_query_actions.= "('', '$rid', '$time', '$action', '$auid', '$team', '', '', '', '', '', '', '', '', '', '', '', ''),";
        $counted_inserts++;
	}

}

// 333:39 Weapon;58418a2336c205b453b2061d8da80454;1;Drake;frag_grenade_mp
function processWeaponLog($action, $time, &$line, &$db, $rid)
{
	global $weapons, $players, $old_line, $sql_query_actions, $counted_inserts;

    //Achtung!&nbsp;Bug in CoD4;&nbsp;Bewirkt das n mal ein und der selbe Weapon Log geschrieben wird.
	$tmp = $line[3].$line[4];

    if($old_line == $tmp)
	{
		return;
	}

	$old_line = $tmp;

	$auid = $players->getId(checkSumPlayer($line[2]));
	//$name = $line[4];
	$weap = $weapons->getId($line[5]);
	
	if($weapons->added == true)
	{
		computeWeaponGroup($weap, $line[5]);
		$name = extractWeaponNameFromLog($line[5]);
		$weapons->addAditionalContentToField($name, 'name', $weap);
		
		$weapon->added = false;
	}

	$sql_query_actions.= "('', '$rid', '$time', '$action', '$auid', '', '', '', '', '', '$weap', '', '', '', '', '', '', ''),";
    $counted_inserts++;
}

// 341:38 say;73c514a8d5f508b3d636d39d05e07602;2;Bloody;Herr xman, ts?
function processInGameChat($action, $time, &$line, &$db, $rid)
{
	global $players, $sql_query_actions, $counted_inserts;
	
	$auid = $players->getId(checkSumPlayer($line[2]));
	$msg = $line[5];	

	$insertsql = "INSERT INTO chats VALUES ('', '$rid', '$auid', '$msg')";
	
	$db2 = new mbdb();
	$db2->query_db($insertsql);
	$muid = $db2->s->insert_id;
/*	
	mysql_query($insertsql, $db->linkid);
	$muid = mysql_insert_id($db->linkid);
*/
	$sql_query_actions.= "('', '$rid', '$time', '$action', '$auid', '', '', '', '', '', '', '', '', '', '$muid', '', '', ''),";
    $counted_inserts++;
}

//349:39 BP;58418a2336c205b453b2061d8da80454;2;Drake

//425:18 AA;LM;73c514a8d5f508b3d636d39d05e07602;2;axis;Bloody;ctf;mp_overgrown
//197:21 AA;FM;73c514a8d5f508b3d636d39d05e07602;2;allies;Bloody;sab;mp_k4f_raid 
//1643:12 AA;RM;c2b48791089874de087e50627db820fb;4;axis;PainKiller;sab;mp_k4f_raid 
//262:32 T;c2b48791089874de087e50627db820fb;5;PainKiller < game_mp_150710.log



function processOtherGameActions($action, $time, &$line, &$db, $rid)
{
	global $players, $mods, $gameActions, $sql_query_actions, $counted_inserts, $arr;

	if($action == 'InitGame:') return;

	$auid = 0;
	$team = 0;

	if($action == "AA")
	{
		$auid = $players->getId(checkSumPlayer($line[3]));
		
		$muid = $gameActions->getId($line[2]);
		$team = ($line[5] == 'axis') ? 0 : 1;
		
		$sql_query_actions.= "('', '$rid', '$time', '$action', '$auid', '', '', '', '', '', '', '', '', '', '$muid', '', '', ''),";
        $counted_inserts++;

		//FM  No Exit 	> abbruch Runde selbe gametype und map
		//LM  Exit		> abbruch Spiel meist anderer gametype und map
		//RM  Exit		> abbruch Spiel meist andere map aber gleicher gametype
		
		//bei FM hier prüfen ob ein Teamspiel vorliegt
		if($line[2] == "FM" && $arr['teamplay'] == 0)
		{
			$exitReached = true;
		}
		
	}
	else if($action == "A")
	{
		$muid = $gameActions->getId($line[6]);
		$auid = $players->getId(checkSumPlayer($line[2]));
		
		$sql_query_actions.= "('', '$rid', '$time', '$action', '$auid', '', '', '', '', '', '', '', '', '', '$muid', '', '', ''),";
        $counted_inserts++;
	}
	else if($action == "ExitLevel: executed")
	{
		$muid = $gameActions->getId($action);
		$sql_query_actions.= "('', '$rid', '$time', '$action', '', '', '', '', '', '', '', '', '', '', '$muid', '', '', ''),";
	}
	else
	{
		$auid = $players->getId(checkSumPlayer($line[2]));
		$team = ($line[3] == 'axis') ? 0 : 1;
		
		if(count($line) > 12)
		{
	    	$mod = $mods->getId($line[12]);
	    }
	    else
	    {
			$mod = NULL;    
	    }
		$muid = $gameActions->getId($action);
		
		$x = $y = $z = 0;
		
		if(isset($line[5]))
		{
			$pos = explode(" ", $line[5]);
			$x = $pos[0];
			$y = $pos[1];
			$z = $pos[2];
				
		}

		$sql_query_actions.= "('', '$rid', '$time', '$action', '$auid', '', '', '', '', '', '', '', '$mod', '', '$muid', '$x', '$y', '$z'),";
		
		$counted_inserts++;
	}
}

/*
  349:39 BP;58418a2336c205b453b2061d8da80454;2;Drake
	2:53 ST;73c514a8d5f508b3d636d39d05e07602;Bloody;crouch
	2:54 ST;58418a2336c205b453b2061d8da80454;Drake;crouch
	
  3:44 ST;58418a2336c205b453b2061d8da80454;Drake;stand
  3:47 SP;58418a2336c205b453b2061d8da80454;Drake;axis;1513.5;18.8;4.125
*/
function processPlayerMoveActions($action, $time, &$line, &$db, $rid)
{
	global $players, $mods, $spawnPoints, $gameActions, $sql_query_actions, $counted_inserts, $arr;
	
	$auid = $players->getId($line[2]);
	$mod = NULL;
	$muid = NULL;
	
	if($action == 'ST')
	{
		$mod = $mods->getId($line[4]);
    }
    else
    {
		//SpawnPoints werden nun separat ermittelt.
    }
    
    $x = $y = $z = 0;
    
    $x = (isset($line[5])) ? $line[5] : 0.0;
    $y = (isset($line[6])) ? $line[6] : 0.0;
    $z = (isset($line[7])) ? $line[7] : 0.0;
    
    
    $sql_query_actions.= "('', '$rid', '$time', '$action', '$auid', '', '', '', '', '', '', '', '$mod', '', '$muid', '$x', '$y', '$z'),";
	$counted_inserts++;
}


function getUnclosedLastRounds()
{
    global $roundBuffer, $db, $types;
    //oki hier letzte Karte und Type ermitteln
    //Karte und Type müssen in die Parserschleife übergeben werden

    //$roundBuffer muss durch die letzten Runden befüllt werden.

    //TODO:
    // Achtung was ist wenn z.Bsp: Sab zweimal auf der selben Map gespielt wird?
    // Eventuell auch in die games_full schauen

    $db->query_db("SELECT * FROM rounds_full ORDER BY id DESC");

    $l_type = -1;
    $l_map = -1;

    while($r = mysqli_fetch_array($db->result))
    {
	    $team = $types->getAditionalContentFromField('multiround', $r['type']);
	    if($team == 0) return;
	    
        if($l_type == -1 && $l_map == -1)
        {
            $l_type = $r['type'];
            $l_map = $r['map'];

            array_push($roundBuffer, array('map' => $l_map, 'type' => $l_type, 'duration' => $r['duration'], 'id' => $r['id'], 'time' => $r['time']));
        }
        else if($r['type'] == $l_type && $r['map'] == $l_map)
        {
            array_push($roundBuffer, array('map' => $l_map, 'type' => $l_type, 'duration' => $r['duration'], 'id' => $r['id'], 'time' => $r['time']));
        }
        else
        {
            break;
        }
    }

    //so nun array umdrehen, da wir ja mit Flag DESC gezogen haben
    $roundBuffer = array_reverse($roundBuffer);
}

function computeWeaponGroup($weap, $line5)
{
	global $weapons, $db;

	//gruppieren der Waffen;
	//1. selbe Waffe schon im Array ?
	$weaps = $weapons->data;
	$c = true;
	$groupName = extractWeaponGroupName($line5);
	$group = 0;
	
	foreach($weaps as $weapon)
	{
		if($weapon['name_log'] != $line5)
		{
			$c = true;

			if(extractWeaponGroupName($weapon['name_log']) == $groupName)
			{
				$grid = $weapon['id'];
				$group = $weapon['weapon_grouped_id'];
				$c = false;
				break;
			}
			
		}
	}	

	if($c == true)
	{
		$tmp = $db->query_assoc("SELECT weapon_grouped_id FROM weapons ORDER BY weapon_grouped_id DESC LIMIT 1");
		$lid = $tmp[0]['weapon_grouped_id'];
		if($lid == NULL) $lid = 0;
		$lid = $lid + 1;
		$group = $lid;		
	}
	
	if($group >= 0)
	{
		$weapons->addAditionalContentToField($group, "weapon_grouped_id", $weap);
	}
}


function parseFile($fileName="")
{
    global $extrem_fast_log, $exitReached, $global_logtime;
	global $weapons, $maps, $types, $hits, $players, $gameActions, $sql_query_actions, $sql_query_rounds, $sql_query_teams, $arr, $db;
	global $last_game_time, $server_start_time, $counted_inserts, $counted_rounds, $last_log_init_time, $sql_query_games, $game_index;

    $handle = fopen($fileName, "r");
	if(!$handle) return;

	$buffer;
	$needupdate         = true;
	$last_game_time     = 0;
	$logtime            = 0;
	$exitReached        = false;
    $last_exit          = false;
	$last_insert_amount = $counted_inserts; //damit wir wissen ob wir gespeichert haben.
	$fileSize           = filesize($fileName);
	$info               = getLastLoggesInfos();
	$last_game_time     = $info['gametime'];
	$old_fileSize       = $info['size'];
	$counted_inserts    = $info['linecount'];
    $exitReached        = $info['game_finished'];
	$start_game_id 		= 0;
	


	if($old_fileSize == $fileSize)
	{
		/*
		echo $old_fileSize. "kb vs. ".$fileSize." kb<br />";
		echo "no update needed<br />Logfile has no changes<br />";
		echo memory_get_peak_usage()." peak Mem | ".memory_get_usage()." norm Mem usage (bytes)<br />";
		*/
		return;
	}
	

	$game_index = getLastGameId();	

    if($exitReached == 0)
    {
        getUnclosedLastRounds();
        
        if($counted_rounds > 0)
        {
	        $game_index--;
        }
    }

    
    $start_game_id = $game_index;

	$counted_rounds = getLastRoundId();

    $round;
    $run = false;
	
	$db->debug = true;

	$sts = microtime_float();

	
    
    while(($buffer = fgets($handle)) !== false)
    {
	    //Comments are not interested
        if(processUnusableLines($buffer) != false)
        {
	     
            if(strpos($buffer, 'InitGame:') !== false)
            {
                $arr = processGameInit($buffer);
				$l = splitLogLine($buffer);
                $t = $l[0];
	            $logtime = processLogTime($t);
	 
	            //echo "chT: ".$last_game_time." ".($server_start_time + $logtime)." ll: ".$t."<br />";

				//Oki $last:game_time nur neu setzen wenn vorher ExitLevel getriggert wurde
				//dann können wir von vorne starten
				// Achtung Runden ohne exitlevel nicht speichern

                if($last_game_time >= ($server_start_time + $logtime))
                {
	                $needupdate = false;
	                unset($arr);
                }
                else
                {
	                $needupdate = true;
	                $round = array();
					$run = true;

                }
            }

			//only when new content is available
			if($needupdate == true)
			{
				//a game is running on
                if($run == true)
                {
                    $round[] = $buffer;
                }
				
				
				if(strpos($buffer, 'AA') !== false)
				{
					$run == false;
				}
				
				


                if(strpos($buffer, 'ShutdownGame:') !== false)
                {
                    if($run == true && count($round) > 2)
                    {
	                    //we need to test whether there is a right round
	                    $parseLines = false;
	                    $rc = count($round);
	                    $lrc = $rc - 1;
						$aaFired = false;

						//first check if AA is fired
						//so we still parse nothing
						if($aaFired == false)
						{
	                        for($i=2; $i<$rc; $i++)
	                        {
		                        $line = splitLogLine($round[$i]);
	
								if(trim($line[1]) == "K")
								{
									$parseLines = true;
									$i = $rc;
	                                break;
								}
		                    } 
						}	
						
						//echo date('m/d/Y H:i:s', ($server_start_time + $logtime))." ll: ".$t."<br />";
						//echo "prsl: ".(($parseLines==false)?0:1)." ROUNDCOUNT: ".$counted_rounds." rc': ".$rc."<br /><br />";

	                    if($parseLines != false && $rc > 6)
						{
							$last_log_init_time = $logtime;

	                        $atime = $arr['time'] + $logtime;

                            $sql_query_actions.= "('', '$counted_rounds', '$atime', 'InitGame:', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),";
		                    $counted_inserts++;
	                        $startTime = -1;

	                        for($i=2; $i<$rc; $i++)
	                        {
		                        $line = splitLogLine($round[$i]);
		                        
								$time = $line[0];
								$action = trim($line[1]);

                                //hier eventuell
                               // $action_id = $gameActions->getId($action);
	                            
	                            $logtime = processLogTime($time);

	                            if($startTime == -1)
								{
									$startTime = $logtime;
								}
								$ltime = $logtime - $startTime;

	                            if($action == 'JT' || $action == 'J' || $action == 'Q' || $action == 'L')
	                            {
		                            processPlayerTeamActions($action, $ltime, $line, $db, $counted_rounds);
	                            }
	                            else if($action == 'K' || $action == 'D')
	                            {
	                            	processKillDeathActions($action, $ltime, $line, $db, $counted_rounds);
	                            }
	                            else if($action == 'Weapon')
								{
		                            processWeaponLog($action, $ltime, $line, $db, $counted_rounds);
	                            }
	                            else if($action == 'say' || $action == 'sayteam')
	                            {
		                            processInGameChat($action, $ltime, $line, $db, $counted_rounds);
	                            }
	                            else if($action == 'MRCS')
	                            {
		                            // aviable maps rotations
	                            }
	                            else if($action == 'MVS' || $action == 'OWFYI')
	                            {
		                            // wtf? > ne mapvoting aber was ist mit OWFYI?
	                            }
	                            else if($action == "ShutdownGame:")
	                            {

	                            	processGameShutdown($ltime, $buffer, $line, $counted_rounds, $logtime, $action);
	                            	
	                              	if($exitReached == true)
	                               	{
		                            	processGameExit($action, $ltime, $line, $db, $counted_rounds);
		                            	$exitReached = false;
		                            	$last_exit = true;
		                            	//$start_game_id++;
	                            	}
	                            	else
	                            	{
		                            	$last_exit = false;
	                            	}
	                            }
	                            else if($action == 'ExitLevel: executed')
	                            {	
			                        processOtherGameActions($action, $ltime, $line, $db, $counted_rounds);
		                           	$exitReached = true;
	                            }
	                            else if($action == 'ST' || $action == 'SP')
	                            {
		                            processPlayerMoveActions($action, $ltime, $line, $db, $counted_rounds);
	                            }
	                            else
	                            {
		                            processOtherGameActions($action, $ltime, $line, $db, $counted_rounds);
	                            }
	                        }


							//save possibly entire line of the file in the DB here
							//remove last comma in querystring
                            $counted_rounds++;

                            if($counted_rounds > 0 && $extrem_fast_log == false)
                            {
                                saveQueryStrings();
                                $needupdate = true;

        						$sql_query_actions  = "INSERT INTO actions_full VALUES ";
                                $sql_query_teams    = "INSERT INTO teams VALUES ";
                                $sql_query_games    = "INSERT INTO games_full VALUES ";
                            }
                        }
                    }
					$run = false;
                }
            }
        }
		//nicht entfernen. Das muss noch behandelt werden.
        //   eventuell ausgeben ob permissions nicht gesetzt sind oder datei nicht gefunden.. (?)
	    if(feof($handle))
	    {
		    echo "Fertig nun abschliessen<br />";
	    }
	}

    fclose($handle);

	if($needupdate == true && ($counted_inserts > $last_insert_amount))
	{
		echo "Log Speichern ".$last_game_time." ".$counted_inserts."<br />";

        if($extrem_fast_log == true)
        {
            saveQueryStrings();

            unset($sql_query_actions);
            unset($sql_query_teams);
        }
        
        
        $sql_query_games = substr($sql_query_games, 0, strlen($sql_query_games)-1);
	    $db->unsave_query($sql_query_games);
	    
		//speichern der letzten insert_id in die LogTabelle
		$time = $last_game_time;

        $sql = "SELECT time FROM rounds_full ORDER BY id DESC LIMIT 1";
        $db->unsave_query($sql);
        $rnd = mysqli_fetch_array($db->result);

        //$time+= $rnd['duration'];
        $logtime = time();
        
		$sql = "INSERT INTO logupdate VALUES ('', '$counted_inserts', '$fileSize', '0', '$time', '$last_exit', '$logtime')";
		$db->unsave_query($sql);

    	//So nun runden speichern und aufräumenf
		$sql_query_rounds = substr($sql_query_rounds, 0, strlen($sql_query_rounds)-1);
        $db->unsave_query($sql_query_rounds);

		echo "gebrauchte Zeit (Sek.) ".(microtime_float() - $sts)." Sekunden bei ".$counted_inserts." Inserts bei ".$counted_rounds." Runden<br />";
	}
	else
	{
		echo "gebrauchte Zeit (Sek.) ".(microtime_float() - $sts)." Sekunden bei test auf update<br />";
	}
	

    echo memory_get_peak_usage()." peak Mem | ".memory_get_usage()." norm Mem usage (bytes)<br />";
    
    return $start_game_id;
}


	
?>