<?php

function buildPlayerStatsArray($uid)
{
	$arr = array('K' => 0, 			// Kill Anzahl
				'D' => 0,			// Deaths Total
				'T' => 0,			// Teamkills Total
				'S' => 0,			// Suizide Total
				'TD' => 0,			// Team 1 oder 0
				'id' => $uid,		// id des Spielers
				'DA' => 0,			// Damage Anzahl
				'DS' => 0,			// Damage summe ausgeteilt
				'sBD' => 0,	 		// Summe Bomb Defuse
				'sBP' => 0,			// Summe Bomb Plant
				'sBPP' => 0,		// Summe Bomb Plant Process
				'sBT' => 0,			// Summe Bomb Taken
				'sBDP' => 0,		// Summe Bomb Defuse Process
				'sBL' => 0,			// Summe Bomb Loose
				'sBDA' => 0,		// Summe Bomb Defuse Abort
				'sBPA' => 0,		// Summe Bomb Plant Abort
				'sKC' => 0,			// Summe Kill Confirm
				'sKD' => 0,			// Summe Kill Denied
				'sGPS' => 0, 		// Summe Gridirion
				'sKV' => 0,			// Summe Abschüsse VIP (ass)
				'sFT' => 0,			// Summe Flag Taken
				'sFR' => 0,			// Summe Flag Return
				'sFC' => 0,			// Summe Flag Confirmed
				'Flagger' => 0,		// Hat der Spieler gerade die Flagge?
				'skFlagger' => 0,	// Summe Kills erreicht mit Flagge
				'sKBomber' => 0,	// Summe Kills mit Bombe
				'sMBPA' => 0,		// Summe Kills Messer Plant Process	(den Bombenleger)
				'sMBDA' => 0,		// Summe Kills Messer Defuse Process (den Entschärfenden)
				'Bomber' => 0,		// Hat der Spieler gerade die Bombe?
				'skBDA' => 0,		// Summe Kills (den Entschärfenden)
				'skBPA' => 0,		// Summe Kills (den Bombenleger)
				'isDeath' => 0,		// Flag um zu erkennen ob jemand versucht ne Killstreak zu bekommen
				'cToKillStreak' => 0,		// Anzahl der erlangten Kills zum Killstreak
                'cToDeathStreak' => 0,      // Anzahl der erlangten Deaths zur Deathstreak
				'W' => 0,			// hat gewonnen
				'sSTc' => 0,		// Haltungsänderungen hocken
				'sSTs' => 0,		// Haltungsänderungen stehen
				'sSTp' => 0,		// Haltungsänderungen liegen
				'lST' => '',		// letzte Haltung
                'lSTstamp' => 0,    // Timestamp letzte Änderung der Körperhaltung
				'sSTKc' => 0,		// Summe Abschüsse in der Hocke
				'sSTKs' => 0,		// Summe Abschüsse stehend
				'sSTKp' => 0,		// Summe Abschüsse liegend
				'timestamp' => 0,   // timestamp der letzten aktion
				'isDS' => true,		// true (default) wird false sobald Schaden (nicht Kill) verursacht wurde
				'sRC' => 0,			// koth bedeutung unbekannt
				'sRD' => 0,
				'sKillStreak' => 0,         // Summe Killstreaks
				'sDeathStreak' => 0,		// Summe Deathstreaks
                'killstreaks' => array(
                    '3er' => 0,
                    '6er' => 0,
                    '9er' => 0,
                    '12er' => 0
                ),
                'deathstreaks' => array(
                    '3er' => 0,
                    '4er' => 0,
                    '5er' => 0,
                    '6er' => 0
                ),
                'distance' => 0
            );

	return $arr;
}

function buildWeaponsStatsArray(){

    $arr = array('w1' => 0,
                'w2' => 0,
                'w3' => 0,
                'w4' => 0,
                'w5' => 0,
                'w6' => 0,
                'w7' => 0,
                'w8' => 0,
                'w9' => 0,
                'w10' => 0,
                'w11' => 0,
                'w12' => 0,
                'w13' => 0,
                'w14' => 0,
                'w15' => 0,
                'w16' => 0,
                'w17' => 0,
                'w18' => 0,
                'w19' => 0,
                'w20' => 0,
                'w21' => 0,
                'w22' => 0,
                'w23' => 0,
                'w24' => 0,
                'w25' => 0,
                'w26' => 0,
                'w27' => 0,
                'w28' => 0,
                'w29' => 0,
                'w30' => 0,
                'w31' => 0,
                'w32' => 0,
                'w33' => 0,
                'w34' => 0,
                'w35' => 0,
                'w36' => 0,
                'w37' => 0,
                'w38' => 0,
                'w39' => 0,
                'w40' => 0,
                'w41' => 0,
                'w42' => 0,
                'w43' => 0,
                'w44' => 0,
                'w45' => 0,
                'w46' => 0,
                'w47' => 0,
                'w48' => 0,
                'w49' => 0,
                'w50' => 0,
                'w51' => 0,
                'w52' => 0,
                'w53' => 0,
                'w54' => 0,
                'w55' => 0,
                'w56' => 0,
                'w57' => 0,
                'w58' => 0,
                'w59' => 0,
                'w60' => 0,
                'w61' => 0,
                'w62' => 0,
                'w63' => 0,
                'w64' => 0,
                'w65' => 0,
                'w66' => 0
            );

    return $arr;
}

?>

