<?php

function buildGametypeSelector($type_id)
{
	$db = new mbdb();
	$db->query_db("SELECT * FROM gametypes ORDER BY name_log ASC");
	
	$select = '<select name="types" id="type_id" onchange="this.form.submit()">';
	$select.= '<option value="-1" '.$sel.'>-</option>';
	
	while($arr = mysqli_fetch_array($db->result))
	{
		$sel = ($type_id == $arr['id']) ? 'selected' : '';
		$select.= '<option value="'.$arr['id'].'" '.$sel.'>'.$arr['name_log'].'</option>';
	}
	
	$select.= '</select>';

	return $select;
}

function getTotalGametypeCount()
{
	$db = new mbdb();
	
	$sql = "SELECT COUNT(id) FROM gametypes";
	
	return $db->query_assoc($sql);
}


// Liste der Gametypes nach Anzahl der Spiele
function getGameTypeCountList()
{
	$db = new mbdb();
	
	$sql = "SELECT COUNT(type) as type_sum, type, gametypes.name_log FROM games_full, gametypes WHERE gametypes.id = games_full.type GROUP BY type ORDER BY type_sum DESC";
	
	return $db->query_assoc($sql);
}

function getMapGametypeCount($type_id)
{
	$db = new mbdb();
	
	$sql = "SELECT COUNT(map) as map_sum, map, maps.name FROM games_full, maps WHERE maps.id = games_full.map AND games_full.type='$type_id' GROUP BY map ORDER BY map_sum DESC";
	
	return $db->query_assoc($sql);
}

function getActionRatioPerGametype($type_id)
{
	$db = new mbdb();
	$db1 = new mbdb();
	
	$sql = "SELECT duration, id FROM rounds_full WHERE type = '$type_id'";
	
	$db->query_db($sql);
	
	$duration = 0;
	$actions = 0;
	
	while($r = mysqli_fetch_array($db->result))
	{
		$duration+= $r['duration'];
		$id = $r['id'];
		
		$arr = $db1->query_assoc("SELECT COUNT(action) AS cactions FROM actions_full WHERE roundid = '$id'");
		$actions+= $arr[0]['cactions'];	
	}
	
	return "Alle ".round(($duration / $actions), 4)." Sekunden eine Aktion";
}



	
?>