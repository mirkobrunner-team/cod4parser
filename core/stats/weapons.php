<?php


function buildWeaponSelector($type_id)
{
	$db = new mbdb();
	$db->query_db("SELECT * FROM weapons ORDER BY name_log ASC");
	
	$select = '<select name="weapons" id="weapon_id" onchange="this.form.submit()">';
	$select.= '<option value="-1" '.$sel.'>-</option>';
	
	while($arr = mysqli_fetch_array($db->result))
	{
		$sel = ($type_id == $arr['id']) ? 'selected' : '';
		$select.= '<option value="'.$arr['id'].'" '.$sel.'>'.$arr['name'].'</option>';
	}
	
	$select.= '</select>';

	return $select;
}

function buildWeaponTypeSelector($type_id)
{
	$db = new mbdb();
	$db->query_db("SELECT * FROM weapons GROUP BY weapons.weapon_grouped_id ORDER BY name_log ASC");
	
	$select = '<select name="weapons_type" id="weapon_type" onchange="this.form.submit()">';
	$select.= '<option value="-1" '.$sel.'>-</option>';
	
	while($arr = mysqli_fetch_array($db->result))
	{
		$sel = ($type_id == $arr['weapons.weapon_grouped_id']) ? 'selected' : '';
		$select.= '<option value="'.$arr['weapons.weapon_grouped_id'].'" '.$sel.'>'.$arr['name'].'</option>';
	}
	
	$select.= '</select>';

	return $select;
}

function getCountedListHitsPerWeapon()
{
	$db = new mbdb();
	
	$sql = "SELECT COUNT(actions_full.weapon) AS w_sum, weapons.name FROM actions_full, weapons WHERE (actions_full.action = 'K' OR actions_full.action = 'D') AND weapons.id = actions_full.weapon GROUP BY actions_full.weapon ORDER BY w_sum DESC";
	
	return $db->query_assoc($sql);
}

function getCountedListKillsPerWeapon()
{
	$db = new mbdb();
	
	$sql = "SELECT COUNT(actions_full.weapon) AS w_sum, SUM(actions_full.health) as d_sum, (COUNT(actions_full.weapon) / SUM(actions_full.health)) as wd_ration, (SUM(actions_full.health) / COUNT(actions_full.weapon)) as dw_ration, weapons.name FROM actions_full, weapons WHERE actions_full.action = 'K' AND weapons.id = actions_full.weapon GROUP BY actions_full.weapon ORDER BY w_sum DESC";
	
	
	return $db->query_assoc($sql);
}


function getCountedListDamagePerWeapon()
{
	$db = new mbdb();
	
	$sql = "SELECT COUNT(actions_full.weapon) AS w_sum, SUM(actions_full.health) as d_sum, weapons.name FROM actions_full, weapons WHERE actions_full.action = 'D' AND weapons.id = actions_full.weapon GROUP BY actions_full.weapon ORDER BY w_sum DESC";
	
	return $db->query_assoc($sql);
}

function getCountedListHitsPerWeaponByType()
{
	// Oki alle genutze Waffen geordnet nach genutzter Anzahl
	$db = new mbdb();
	
	$sql = "SELECT COUNT(actions_full.weapon) AS w_sum, weapons.name FROM actions_full, weapons WHERE (actions_full.action = 'K' OR actions_full.action = 'D') AND weapons.id = actions_full.weapon GROUP BY weapons.weapon_grouped_id ORDER BY w_sum DESC";
	
	return $db->query_assoc($sql);
}

function getCountedListKillsPerWeaponByType()
{
	$db = new mbdb();
	
	$sql = "SELECT COUNT(actions_full.weapon) AS w_sum, weapons.name FROM actions_full, weapons WHERE actions_full.action = 'K' AND weapons.id = actions_full.weapon GROUP BY weapons.weapon_grouped_id ORDER BY w_sum DESC";
	
	return $db->query_assoc($sql);
}

function getCountedListDamagePerWeaponByType()
{
	$db = new mbdb();

	$sql = "SELECT COUNT(actions_full.weapon) AS w_sum, SUM(actions_full.health) as d_sum, weapons.name FROM actions_full, weapons WHERE actions_full.action = 'D' AND weapons.id = actions_full.weapon GROUP BY weapons.weapon_grouped_id ORDER BY w_sum DESC";
	
	return $db->query_assoc($sql);
}

function getKillDamageRationPerWeapon()
{
	$kills = getCountedListKillsPerWeapon();
	$damages = getCountedListDamagePerWeapon();
	$effi = array();
	
	foreach($kills as $kill)
	{
		
		foreach($damages as $damage)
		{
			if($kill['name'] == $damage['name'])
			{
				$effi[] = array('effi' => ($kill['w_sum'] / $damage['w_sum']), 'damage_needed_per_kill' => ($damage['d_sum'] / $kill['w_sum']), 'name' => $kill['name']);
				break;
			}
		}
	}
	
	return $effi;
}

function getKillDamageRationPerWeaponByType()
{
	$kills = getCountedListKillsPerWeaponByType();
	$damages = getCountedListDamagePerWeaponByType();
	$effi = array();
	
	foreach($kills as $kill)
	{
		
		foreach($damages as $damage)
		{
			if($kill['name'] == $damage['name'])
			{
				$effi[] = array('effi' => ($kill['w_sum'] / $damage['w_sum']), 'damage_needed_per_kill' => ($damage['d_sum'] / $kill['w_sum']), 'name' => $kill['name']);
				break;
			}
		}
	}
	
	return $effi;
}


?>