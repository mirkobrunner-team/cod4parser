<?php

date_default_timezone_set('Europe/Berlin');
if(!isset($db)) {
    $db = new mbdb();
}
if(!isset($types)) {
    $types = new ExtendedArray();
    $types->fill('gametypes');
}
if(!isset($maps)) {
    $maps = new ExtendedArray();
    $maps->fill('gametypes');
}
if(!isset($players)) {
    $players = new ExtendedArray();
    $players->fill('gametypes');
}

function buildGameSelector($pid, $noSubmitter = false)
{
	global $db, $maps, $types;
	if(!isset($db)) $db = new mbdb();

	$db->query_db("SELECT * FROM games_full ORDER BY time ASC");
	
	if($noSubmitter == false)
	{
		$cont = "<select name='selmap' id='selmap' onchange='this.form.submit()'>";
	}
	else
	{
		$cont = "<select name='selmap' id='selmap'>";
	}
		
	$cont.= "<option value='0'>-</option>";

	while($r = mysqli_fetch_array($db->result))
	{
		$time = date('m/d/Y H:i:s', $r['time']);
        $sel = ($pid == $r['id']) ? "selected" : "";
		$cont.= '<option value="'.$r['id'].'" '.$sel.'>'.$time." ".$types->getLogName($r['type'])." - ".$maps->getAditionalContentFromField('name', $r['map']).'</option>';
	}

	$cont.= "</select>";
	return $cont;
}

function buildGamesList()
{
	global $db, $maps, $types;
	
	$sql = "SELECT games_full.id as gid, games_full.time, map, type, duration, rounds, COUNT(DISTINCT(teams.playerid)) as num_players FROM games_full, teams WHERE games_full.id = teams.gameid GROUP BY games_full.id ORDER BY time ASC";
	
	$arr = $db->query_assoc($sql);

	$cont = "";

	$c = count($arr);
	
	foreach($arr as $r)
	{
		$time = date('m/d/Y H:i:s', $r['time']);
		$nplayer = $r['num_players'];
		$mapName = $maps->getAditionalContentFromField('name', $r['map']);
		$typeName = $types->getLogName($r['type']);
		$numRounds = substr_count($r['rounds'], ",");
		$numRounds++;
		
		$durtaion = secToTime($r['duration']);
		
		$cont.= $time." ".$mapName." ".$typeName." ".$numRounds." ".$durtaion."<br />";	
	}
	
	return $cont;
}

function buildGamesTable()
{
	global $db, $maps, $types;
	
	$sql = "SELECT games_full.id as gid, games_full.time, map, type, duration, rounds, COUNT(DISTINCT(teams.playerid)) as num_players FROM games_full, teams WHERE games_full.id = teams.gameid GROUP BY games_full.id ORDER BY time ASC";
	
	$arr = $db->query_assoc($sql);

	$content = '<table class="tablesorter" id="games_table" border=1>';
	$content.= '<thead title="Zum Sortieren klicken">';
	$content.= '<tr class="table_row" align="left"><th>Zeit</th>';
	$content.= '<th align="center">Art</th>';
	$content.= '<th align="center"Karte</th>';
	$content.= '<th align="center">Runden</th>';
	$content.= '<th align="center">Dauer (Min)</th>';
	$content.= '</tr>';
	$content.= '</thead>';

	$content.= '<tbody>';

	foreach($arr as $r)
	{
		$time = date('m/d/Y H:i:s', $r['time']);
		$nplayer = $r['num_players'];
		$mapName = $maps->getAditionalContentFromField('name', $r['map']);
		$typeName = $types->getLogName($r['type']);
		$numRounds = substr_count($r['rounds'], ",");
		$numRounds++;
		
		$duration = secToTime($r['duration']);
		
		$content.= '<tr><td>'.$time.'</td><td>'.$typeName.'</td><td>'.$mapName.'</td><td>'.$numRounds.'</td><td>'.$duration.'</td></tr>';
	}
	
	$content.= '</tbody>';
	$content.= '</table>';
	
	$content.= '<script type="text/javascript">$(document).ready(function(){$("#games_table").tablesorter();});</script>';
	
	return $content;
}



function countGames()
{
	global $db;
	if(!isset($db)) $db = new mbdb();
	
	return $db->query_assoc("SELECT COUNT(id) FROM games_full");
}

function countRounds()
{
	global $db;
	if(!isset($db)) $db = new mbdb();
	
	return $db->query_assoc("SELECT COUNT(id) FROM rounds_full");
}

function countActions()
{
	global $db;
	if(!isset($db)) $db = new mbdb();
	
	return $db->query_assoc("SELECT COUNT(id) FROM actions_full");
}

function totalPlayTime()
{
	global $db;
	if(!isset($db)) $db = new mbdb();
	
	return $db->query_assoc("SELECT DATE_FORMAT(FROM_UNIXTIME(SUM(games_full.duration)), '%d Tage %H:%i:%S') as mduration FROM games_full");
}

function totalKillSum()
{
	global $db;
	if(!isset($db)) $db = new mbdb();
	
	return $db->query_assoc("SELECT SUM(IF(action = 'K', 1,0)) killsum FROM actions_full");
}

function totalHitsSum()
{
	global $db;
	if(!isset($db)) $db = new mbdb();
	
	return $db->query_assoc("SELECT (SUM(IF(action = 'K', 1,0)) + SUM(IF(action = 'K', 1,0))) as h_sum FROM actions_full");
}

function countMaps()
{
	global $db;
	if(!isset($db)) $db = new mbdb();
	
	return $db->query_assoc("SELECT COUNT(id) FROM maps");
}


function countGametypes()
{
	global $db;
	if(!isset($db)) $db = new mbdb();
	
	return $db->query_assoc("SELECT COUNT(id) FROM gametypes");
}

function countPlayers()
{
	global $db;
	if(!isset($db)) $db = new mbdb();
	
	return $db->query_assoc("SELECT COUNT(id) FROM aliases");
}

function countSpawnPoints()
{
	global $db;
	if(!isset($db)) $db = new mbdb();
	
	return $db->query_assoc("SELECT COUNT(id) FROM spawnpoints");
}

function getMostPlayedMap()
{
	global $db;
	if(!isset($db)) $db = new mbdb();
	
	return $db->query_assoc("SELECT COUNT(games_full.map) as map_sum, maps.name FROM games_full, maps WHERE maps.id = games_full.map GROUP BY map ORDER BY map_sum DESC LIMIT 1");
}

function getMostPlayedGametype()
{
	global $db;
	if(!isset($db)) $db = new mbdb();

	return $db->query_assoc("SELECT COUNT(games_full.type) as type_sum, gametypes.name_log FROM games_full, gametypes WHERE gametypes.id = games_full.type GROUP BY type ORDER BY type_sum DESC LIMIT 1");
}

function getLongestPlayedGame()
{
	global $db;
	if(!isset($db)) $db = new mbdb();

	$arr = $db->query_assoc("SELECT SEC_TO_TIME(MAX(games_full.duration)) as mduration, maps.name AS mapname, gametypes.name_log FROM games_full, maps, gametypes WHERE maps.id = games_full.map AND gametypes.id = games_full.type");
}

function getMostUsedWeapon()
{
	global $db;
	if(!isset($db)) $db = new mbdb();

	return $db->query_assoc("SELECT COUNT(actions_full.weapon) as cweapon, weapons.name FROM actions_full, weapons WHERE weapons.id = actions_full.weapon GROUP BY weapon ORDER BY cweapon DESC LIMIT 1");
}



function getRealDurationForRound($round_id)
{
	global $db;
	if(!isset($db)) $db = new mbdb();
	
	$sql = "SELECT time FROM actions_full WHERE id = ((SELECT id FROM actions_full WHERE action = 'ShutdownGame' AND roundid = '$round_id' ) - 1) ORDER BY id ASC";
	
	$db->query_assoc($sql);
}



function countTeamKills()
{
	global $db;
	if(!isset($db)) $db = new mbdb();
	
	$sql = "SELECT COUNT(action) as teamkills FROM actions_full, gametypes, rounds_full WHERE rounds_full.id = actions_full.roundid AN action = K AND eteam = team AND gametypes.id = rounds_full.type";
	
	$db->query_assoc($sql);
}


function countSuizides()
{
	global $db;
	if(!isset($db)) $db = new mbdb();
	
	$sql = "SELECT COUNT(action) as teamkills FROM actions_full WHERE action = K AND auid = euid";
	
	$db->query_assoc($sql);
}


// supportet intervals: h = hour, d = day, w = week, m = month
function getPlayersCountByTimeInterval($interval = 'd')
{
	global $db;
	if(!isset($db)) $db = new mbdb();
	
	$sqlInter = "DAY";

	if($interval == 'h')
	{
		$sql = "SELECT COUNT(DISTINCT(teams.playerid)) as player_sum DATE_FORMAT(FROM_UNIXTIME(rounds_full.time),'%d.%m.%Y - %h') AS datum FROM rounds_full, teams WHERE teams.roundid = rounds_full.id GROUP BY datum ORDER BY rounds_full.time ASC";
	}
	else if($interval == 'd')
	{
		$sql = "SELECT COUNT(DISTINCT(teams.playerid)) as player_sum, DATE_FORMAT(FROM_UNIXTIME(rounds_full.time),'%d.%m.%Y') AS datum FROM rounds_full, teams WHERE teams.roundid = rounds_full.id GROUP BY DAY(FROM_UNIXTIME(rounds_full.time)) ORDER BY rounds_full.time ASC";
	}
	else if($interval == 'w')
	{
		//$sql = "SELECT COUNT(DISTINCT(teams.playerid)) as player_sum, DATE_FORMAT(FROM_UNIXTIME(rounds_full.time),'%d.%m.%Y') AS datum FROM rounds_full, teams WHERE teams.roundid = rounds_full.id GROUP BY WEEK(FROM_UNIXTIME(rounds_full.time)) ORDER BY rounds_full.time ASC";
		
		$sql = "SELECT COUNT(DISTINCT(teams.playerid)) as player_sum, WEEK(FROM_UNIXTIME(rounds_full.time), 1) datum FROM rounds_full, teams WHERE teams.roundid = rounds_full.id GROUP BY WEEK(FROM_UNIXTIME(rounds_full.time), 1) ORDER BY rounds_full.time ASC";

	}
	else if($interval == 'm')
	{
		$sqlInter = "MONTH";
		
		$sql = "SELECT COUNT(DISTINCT(teams.playerid)) as player_sum, DATE_FORMAT(FROM_UNIXTIME(rounds_full.time),'%d.%m.%Y') AS datum FROM rounds_full, teams WHERE teams.roundid = rounds_full.id GROUP BY MONTH(FROM_UNIXTIME(rounds_full.time)) ORDER BY rounds_full.time ASC";
	}
	else
	{
		return false;
	}

	if($sql == "") return false;
	
	return $db->query_assoc($sql);
}

function getNewPlayersCountByTimeInterval($interval = 'd')
{
	global $db;
	if(!isset($db)) $db = new mbdb();
	
	$sqlInter = "DAY";

	if($interval == 'h')
	{
		$sql = "SELECT COUNT(DISTINCT(teams.playerid)) as player_sum DATE_FORMAT(FROM_UNIXTIME(rounds_full.time),'%d.%m.%Y - %h') AS datum FROM rounds_full, teams WHERE teams.roundid = rounds_full.id GROUP BY datum ORDER BY rounds_full.time ASC";
	}
	else if($interval == 'd')
	{
		$sql = "SELECT COUNT(DISTINCT(teams.playerid)) as player_sum, DATE_FORMAT(FROM_UNIXTIME(rounds_full.time),'%d.%m.%Y') AS datum FROM rounds_full, teams WHERE teams.roundid = rounds_full.id GROUP BY DAY(FROM_UNIXTIME(rounds_full.time)) ORDER BY rounds_full.time ASC";
	}
	else if($interval == 'w')
	{
		//$sql = "SELECT COUNT(DISTINCT(teams.playerid)) as player_sum as new_player_sum, WEEK(FROM_UNIXTIME(rounds_full.time), 1) datum FROM rounds_full, teams WHERE teams.roundid = rounds_full.id GROUP BY WEEK(FROM_UNIXTIME(rounds_full.time), 1) ORDER BY rounds_full.time ASC";
		
		$sql = "SELECT COUNT(DISTINCT(teams.playerid)) as player_sum, WEEK(FROM_UNIXTIME(rounds_full.time), 1) datum FROM rounds_full, teams, aliases WHERE teams.roundid = rounds_full.id AND aliases.id = teams.playerid AND WEEK(FROM_UNIXTIME(aliases.added), 1) = WEEK(FROM_UNIXTIME(rounds_full.time), 1) GROUP BY WEEK(FROM_UNIXTIME(rounds_full.time), 1) ORDER BY rounds_full.time ASC";

	}
	else if($interval == 'm')
	{
		$sqlInter = "MONTH";
		
		$sql = "SELECT COUNT(DISTINCT(teams.playerid)) as player_sum, DATE_FORMAT(FROM_UNIXTIME(rounds_full.time),'%d.%m.%Y') AS datum FROM rounds_full, teams WHERE teams.roundid = rounds_full.id GROUP BY MONTH(FROM_UNIXTIME(rounds_full.time)) ORDER BY rounds_full.time ASC";
	}
	else
	{
		return false;
	}

	if($sql == "") return false;

	return $db->query_assoc($sql);
}

function getGamesCountByTimeInterval($interval = 'd')
{
	global $db;
	if(!isset($db)) $db = new mbdb();
	
	if($interval == 'h')
	{
		$sql = "SELECT COUNT(id) as games_sum, DATE_FORMAT(FROM_UNIXTIME(games_full.time),'%d.%m.%Y - %h') AS datum FROM games_full GROUP BY datum ORDER BY games_full.time ASC";
	}
	else if($interval == 'd')
	{
		$sql = "SELECT COUNT(id) as games_sum, DATE_FORMAT(FROM_UNIXTIME(games_full.time),'%d.%m.%Y') AS datum FROM games_full GROUP BY DAY(FROM_UNIXTIME(games_full.time)) ORDER BY games_full.time ASC";
	}
	else if($interval == 'w')
	{
		//$sql = "SELECT COUNT(id) as games_sum, DATE_FORMAT(FROM_UNIXTIME(games_full.time),'%d.%m.%Y') AS datum FROM games_full GROUP BY WEEK(FROM_UNIXTIME(games_full.time)) ORDER BY games_full.time ASC";
		$sql = "SELECT COUNT(id) as games_sum, WEEK(FROM_UNIXTIME(games_full.time), 1) AS datum FROM games_full GROUP BY WEEK(FROM_UNIXTIME(games_full.time), 1) ORDER BY games_full.time ASC";
	}
	else if($interval == 'm')
	{
		$sql = "SELECT COUNT(id) as games_sum, DATE_FORMAT(FROM_UNIXTIME(games_full.time),'%d.%m.%Y') AS datum FROM games_full GROUP BY MONTH(FROM_UNIXTIME(games_full.time)) ORDER BY games_full.time ASC";
	}
	else
	{
		return false;
	}
	
	
	
	if($sql == "") return false;

	
	return $db->query_assoc($sql);
}


function getAllPlayerStatsByTimeInterval($interval = 'w')
{
	global $db;
	if(!isset($db)) $db = new mbdb();
	
	if($interval == 'w')
	{
		$sql = "SELECT SUM(points) as points_sum, SUM(kills) as kills_sum, SUM(deaths) as deaths_sum, SUM(teamkills) as teamkills_sum, WEEK(FROM_UNIXTIME(games_full.time), 1) AS datum FROM stats_rounds_players, games_full WHERE games_full.id = gameid GROUP BY WEEK(FROM_UNIXTIME(games_full.time), 1) ORDER BY games_full.time ASC";
	}
	if($sql == "") return false;

	
	return $db->query_assoc($sql);
}



function getGameActionsByTimeInterval($gameid, $interval = 'i')
{/*
	global $db;
	if(!isset($db)) $db = new mbdb();
	
	$sql = "SELECT DATE_FORMAT(FROM_UNIXTIME(actions_full.time),'%i'), SUM(IF(action = 'K', 1,0)) AS k_sum, SUM(IF(action = 'D', 1,0)) AS d_sum, SUM(IF(action = 'ST', 1,0)) AS st_sum FROM actions_full WHERE actions_full.action != 'InitGame:' AND roundid IN(329,330,331,332) GROUP BY DATE_FORMAT(FROM_UNIXTIME(actions_full.time),'%i') ORDER BY actions_full.time ASC";
	if($sql == "") return false;

	
	return $db->query_assoc($sql);
*/
}




function getActionCountList()
{
    global $db;
	if(!isset($db)) $db = new mbdb();

    $sql = "SELECT action, count(action) AS count, gameactions.name FROM actions_full, gameactions WHERE gameactions.name_log = actions_full.action GROUP BY action ORDER BY count DESC";

    return $db->query_assoc($sql);
}

function getDistanceSumForGame($gameid) {
	$db = new mbdb();

	$sql = "SELECT SUM(distance) AS sum_distance FROM stats_rounds_players WHERE stats_rounds_players.gameid = '$mapid'";

	return $db->query_assoc($sql);
}

function getDistanceSumForPlayersInGame($gameid) {
	$db = new mbdb();

	$sql = "SELECT SUM(distance) AS sum_distance, playerid FROM stats_rounds_players WHERE gameid = $gameid GROUP BY playerid ORDER BY sum_distance DESC";

	return $db->query_assoc($sql);
}

function getDistanceSumForPlayerInGame($gameid, $playerid) {
	$db = new mbdb();

	$sql = "SELECT SUM(distance) AS sum_distance FROM stats_rounds_players WHERE gameid = $gameid AND playerid = $playerid";

	return $db->query_assoc($sql);
}

function getActionDounatByGame($gameid, $playerid = -1) {
    $db = new mbdb();

    $result = $db->query_assoc("SELECT SUM(IF(actoin == 'D') THEN 1 ELSE 0 END IF) as c FROM actions_full WHERE gameid = '".$gameid."'");
}


?>
