<?php
/*
require_once '../misc/serv_db.inc.php';

function d($e) {
    echo '<pre>';
    var_dump($e);
    echo '</pre>';
}
*/
function buildPlayerSelector($player_id, $all = false)
{
	$db = new mbdb();

    if($all == true)
	    $db->query_db("SELECT * FROM aliases ORDER BY hash ASC");
    else
        $db->quer_db("SELECT DISTINCT(playerid) AS id, `hash` FROM stats_rounds_players, aliases WHERE aliases.id = playerid");

	$select = '<select name="player" id="player" onchange="this.form.submit()">';

	while($arr = mysqli_fetch_array($db->result))
	{
		$sel = ($player_id == $arr['id']) ? 'selected' : '';
		$select.= '<option value="'.$arr['id'].'" '.$sel.'>'.$arr['hash'].'</option>';
	}

	$select.= '</select>';

	return $select;
}


function getBasicPlayerKillStats($player_id)
{
	$db = new mbdb();

	$stats = array();
	
	//Kills
	$sql1 = "SELECT COUNT(action) AS k_sum FROM actions_full WHERE action = 'K' AND euid = '$player_id' AND puid != euid";

	$tmp = $db->query_assoc($sql1);
	$stats['Kills'] = $tmp[0]['k_sum'];
	
	//Nun die Kills
	
	//teamkill
	$sql2 = "SELECT COUNT(action) AS k_sum, actions_full.puid AS player_id FROM actions_full, rounds_full, gametypes WHERE euid = '$player_id' AND actions_full.action = 'K' AND actions_full.team = actions_full.eteam AND puid != euid AND rounds_full.id = roundid AND gametypes.id = rounds_full.type AND gametypes.team = 1";

	$tmp = $db->query_assoc($sql2);
	$stats['TeamKills'] = $tmp[0]['k_sum'];

	
	//suizide
	$sql3 = "SELECT COUNT(actions_full.action) AS k_sum, actions_full.puid AS player_id FROM actions_full WHERE puid = '$player_id' AND actions_full.action = 'K' AND puid = euid";
	
	$tmp = $db->query_assoc($sql3);
	$stats['Suizide'] = $tmp[0]['k_sum'];

	return $stats;
}


function getBasicPlayerDeathStats($player_id)
{
	$db = new mbdb();
	
	$stats = array();
	
	//Kills
	$sql1 = "SELECT COUNT(action) AS k_sum FROM actions_full WHERE puid = '$player_id' AND action = 'K' AND puid != euid";
	
	$tmp = $db->query_assoc($sql1);
	$stats['Kills'] = $tmp[0]['k_sum'];


	//teamkill
	$sql2 = "SELECT COUNT(action) AS k_sum FROM actions_full, rounds_full, gametypes WHERE puid = '$player_id' AND actions_full.action = 'K' AND actions_full.team = actions_full.eteam AND puid != euid AND rounds_full.id = roundid AND gametypes.id = rounds_full.type AND gametypes.team = 1";
	
	$tmp = $db->query_assoc($sql2);
	$stats['TeamKills'] = $tmp[0]['k_sum'];

	
	//suizide
	$sql3 = "SELECT SUM(IF(action = 'K', 1,0)) AS k_sum, actions_full.puid AS player_id FROM actions_full, rounds_full WHERE puid = '$player_id' AND puid = euid AND rounds_full.id = roundid AND rounds_full.type > 2";
	
	
	$tmp = $db->query_assoc($sql3);
	$stats['Suizide'] = $tmp[0]['k_sum'];

	return $stats;
}

function getKDRatio($kill, $death)
{
	if($kill == 0) return 0;
	$death = ($death == 0) ? 1 : $death;
	return $kill / $death;
}

function getWeaponKillCount($player_id)
{
	// Oki alle genutze Waffen geordnet nach genutzter Anzahl
	$db = new mbdb();
	
	$sql = "SELECT COUNT(actions_full.weapon) AS w_sum, weapons.name FROM actions_full, weapons WHERE actions_full.action = 'K' AND actions_full.euid = '$player_id' AND weapons.id = actions_full.weapon GROUP BY actions_full.weapon ORDER BY w_sum DESC";
	
	return $db->query_assoc($sql);
}

function getWeaponDeathsCount($player_id)
{
	// Oki alle genutze Waffen geordnet nach genutzter Anzahl
	$db = new mbdb();
	
	$sql = "SELECT COUNT(actions_full.weapon) AS w_sum, weapons.name FROM actions_full, weapons WHERE actions_full.action = 'K' AND actions_full.puid = '$player_id' AND  weapons.id = actions_full.weapon GROUP BY actions_full.weapon ORDER BY w_sum DESC";
	
	return $db->query_assoc($sql);
}

function getWeaponKillCountByType($player_id)
{
	// Oki alle genutze Waffen geordnet nach genutzter Anzahl
	$db = new mbdb();
	
	$sql = "SELECT COUNT(actions_full.weapon) AS w_sum, weapons.weapon_grouped_id, weapons.name FROM actions_full, weapons WHERE actions_full.action = 'K' AND actions_full.euid = '$player_id' AND  weapons.id = actions_full.weapon GROUP BY weapons.weapon_grouped_id ORDER BY w_sum DESC";
	
	return $db->query_assoc($sql);
}

function getWeaponDeathsCountByType($player_id)
{
	// Oki alle genutze Waffen geordnet nach genutzter Anzahl
	$db = new mbdb();

	$sql = "SELECT COUNT(actions_full.weapon) AS w_sum, weapons.weapon_grouped_id, weapons.name FROM actions_full, weapons WHERE actions_full.action = 'K' AND actions_full.puid = '$player_id' AND  weapons.id = actions_full.weapon GROUP BY weapons.weapon_grouped_id ORDER BY w_sum DESC";
	
	return $db->query_assoc($sql);
}

function countKillsPlayerList($player_id)
{
	$db = new mbdb();

	$sql = "SELECT SUM(IF(action = 'K', 1,0)) AS k_sum, euid, actions_full.puid AS player_id, aliases.hash FROM actions_full, rounds_full, aliases WHERE euid = '$player_id' AND puid != euid AND rounds_full.id = roundid AND aliases.id = euid GROUP BY euid ORDER BY k_sum DESC";

	 return $db->query_assoc($sql);
}

function countTeamKillsPlayerList($player_id)
{
	$db = new mbdb();

	$sql = "SELECT SUM(IF(action = 'K', 1,0)) AS k_sum, puid, aliases.hash FROM actions_full, rounds_full, aliases, gametypes WHERE action = 'K' AND euid = '$player_id' AND actions_full.team = actions_full.eteam AND puid != euid AND rounds_full.id = roundid AND aliases.id = puid AND (gametypes.id = rounds_full.type AND gametypes.team = 1) GROUP BY puid ORDER BY k_sum DESC";

	 return $db->query_assoc($sql);
}

function countDeathsByEnemyList($player_id)
{
	$db = new mbdb();

	$sql = "SELECT SUM(IF(action = 'K', 1,0)) AS k_sum, euid, aliases.hash FROM actions_full, rounds_full, aliases, gametypes WHERE action = 'K' AND puid = '$player_id' AND actions_full.team = actions_full.eteam AND puid != euid AND rounds_full.id = roundid AND aliases.id = euid AND (gametypes.id = rounds_full.type AND gametypes.team = 1) GROUP BY euid ORDER BY k_sum DESC";

	 return $db->query_assoc($sql);
}

function countTeamDeathsByEnemyList($player_id)
{
	$db = new mbdb();

	$sql = "SELECT SUM(IF(action = 'K', 1,0)) AS k_sum, euid, aliases.hash FROM actions_full, rounds_full, aliases, gametypes WHERE puid = '$player_id' AND actions_full.team = actions_full.eteam AND puid != euid AND rounds_full.id = roundid AND aliases.id = euid AND gametypes.id = rounds_full.type AND gametypes.team = 1 GROUP BY euid ORDER BY k_sum DESC";

	 return $db->query_assoc($sql);
}

function countAction($action, $grouped = '')
{
    $group = $grouped;
    $endGroup = ($group != '') ? "GROUP BY ".$group : "";

    $sql = "SELECT SUM(IF(action = '$action', 1,0)), ".$group." AS k_sum FROM actions_full, rounds_full WHERE rounds_full.id = roundid ".$endGroup;

    $db = new mbdb($sql);
    return $db->query_assoc($sql);
}	



function getPlayerMapsCountList($player_id)
{
	$db = new mbdb();
	
	//get all games
	$sql = "SELECT COUNT(games_full.map) as map_sum, maps.name FROM teams, games_full, maps WHERE games_full.id = gameid AND teams.playerid = '$player_id' AND maps.id = games_full.map GROUP BY games_full.map ORDER BY map_sum DESC";

	return $db->query_assoc($sql);
}

function getPlayerGametypeCountList($player_id)
{
	$db = new mbdb();

	//get all games
	$sql = "SELECT gameid, COUNT(games_full.type) as type_sum, gametypes.name_log as name FROM teams, games_full, gametypes WHERE games_full.id = gameid AND teams.playerid = '$player_id' AND gametypes.id = games_full.type GROUP BY games_full.type ORDER BY type_sum DESC";

	return $db->query_assoc($sql);
}

function getPlayerHitLocationsByKillsTotal($palyer_id)
{
	$db = new mbdb();
	
	$sql = "SELECT COUNT(actions_full.hitlocation) as hit_sum, SUM(actions_full.health) as dam_sum, hitlocations.name FROM actions_full, hitlocations WHERE actions_full.action = 'K' AND actions_full.euid = '$player_id' AND hitlocations.id = actions_full.hitlocation GROUP BY hitlocations.id ORDER BY dam_sum DESC";

	return $db->query_assoc($sql);
}

function getPlayerHitLocationsByDeathsTotal($player_id)
{
	$db = new mbdb();
	
	$sql = "SELECT COUNT(actions_full.hitlocation) as hit_sum, SUM(actions_full.health) as dam_sum, hitlocations.name FROM actions_full, hitlocations WHERE actions_full.action = 'D' AND actions_full.euid = '$player_id' AND hitlocations.id = actions_full.hitlocation GROUP BY hitlocations.id ORDER BY dam_sum DESC";

	return $db->query_assoc($sql);
}

function getPlayerHitLocationsInflictedTotal($player_id)
{
	$db = new mbdb();
	
	$sql = "SELECT COUNT(actions_full.hitlocation) as hit_sum, SUM(actions_full.health) as dam_sum, hitlocations.name FROM actions_full, hitlocations WHERE actions_full.euid = '$player_id' AND hitlocations.id = actions_full.hitlocation GROUP BY hitlocations.id ORDER BY dam_sum DESC";

	return $db->query_assoc($sql);
}

function getPlayerHitLocationsRecievedTotal($player_id)
{
	$db = new mbdb();
	
	$sql = "SELECT COUNT(actions_full.hitlocation) as hit_sum, SUM(actions_full.health) as dam_sum, hitlocations.name FROM actions_full, hitlocations WHERE actions_full.puid = '$player_id' AND hitlocations.id = actions_full.hitlocation GROUP BY hitlocations.id ORDER BY dam_sum DESC";

	return $db->query_assoc($sql);
}

function sumDamageInflictedTotalbyHitlocation($player_id)
{
	$db = new mbdb();
	
	$sql = "SELECT SUM(actions_full.health) as d_sum, hitlocations.name as hitname FROM actions_full, hitlocations WHERE euid = '$player_id' AND hitlocations.id = actions_full.hitlocation AND hitlocation > 0 GROUP BY actions_full.hitlocation ORDER BY d_sum DESC";
	
	return $db->query_assoc($sql);
}

function sumDamageRecievedTotalHitlocation($player_id)
{
	$db = new mbdb();
	
	$sql = "SELECT SUM(actions_full.health) as d_sum, hitlocations.name as hitname FROM actions_full, hitlocations WHERE puid = '$player_id' AND hitlocations.id = actions_full.hitlocation AND hitlocation > 0 GROUP BY actions_full.hitlocation ORDER BY d_sum DESC";
	
	return $db->query_assoc($sql);
}

function sumDamageInflictedTotal($player_id)
{
	$db = new mbdb();
	
	$sql = "SELECT SUM(actions_full.health) as d_sum FROM actions_full WHERE euid = '$player_id'";
	
	return $db->query_assoc($sql);
}

function sumDamageRecievedTotal($player_id)
{
	$db = new mbdb();
	
	$sql = "SELECT SUM(actions_full.health) as d_sum FROM actions_full WHERE puid = '$player_id'";

	return $db->query_assoc($sql);
}


function getPlayersActionCountList($player_id)
{
    $db = new mbdb();

    $sql = "SELECT action, count(action) AS count, gameactions.name FROM actions_full, gameactions WHERE puid = '$player_id' AND gameactions.name_log = actions_full.action GROUP BY action ORDER BY count DESC";

    return $db->query_assoc($sql);
}

function getKillsByPlayerList($player_id)
{
	$db = new mbdb();
	
	$sql = "SELECT count(actions_full.puid) as p_sum, aliases.hash as name FROM actions_full, aliases WHERE actions_full.action = 'K' AND actions_full.euid = '$player_id' AND actions_full.puid != '$player_id' AND aliases.id = actions_full.puid GROUP BY actions_full.puid ORDER BY p_sum DESC";
	
	return $db->query_assoc($sql);
}

function getDeathsByPlayerList($player_id)
{
	$db = new mbdb();

	$sql = "SELECT count(actions_full.euid) as p_sum, aliases.hash as name FROM actions_full, aliases WHERE actions_full.action = 'K' AND actions_full.puid = '$player_id' AND actions_full.euid != '$player_id' AND aliases.id = actions_full.euid GROUP BY actions_full.euid ORDER BY p_sum DESC";

	return $db->query_assoc($sql);
}


function buildBody($arr)
{	
	$c = count($arr);
	$s = 0;
	
	for($i=0;$i<$c;$i++)
	{
		if($arr[$i]['name'] != 'none')
		{
			$s+= $arr[$i]['dam_sum'];
		}
	}
	
	$p = 100 / $s;
	$colors = array();
	
	for($i=0;$i<$c;$i++)
	{
		if($arr[$i]['name'] != 'none')
		{
			$t+=($arr[$i]['dam_sum'] / 100) * $p;
			$colors[trim($arr[$i]['name'])] = 1-$t;
		}
	} 
	
	$content.= '<div class="body">';
	$content.= '<div class="head" style="opacity:'.$colors['head'].';"></div>';
	$content.= '<div class="neck" style="opacity:'.$colors['neck'].';"></div>';
	$content.= '<div class="left_arm">';
	$content.= '<div class="left_arm_upper" style="opacity:'.$colors["Left Arm Upper"].';"></div>';
	$content.= '<div class="left_arm_lower" style="opacity:'.$colors["Left Arm Lower"].'"></di';
	$content.= '<div class="left_hand" style="opacity:'.$colors["Left Hand"].';"></div>';
	$content.= '</div>';
	$content.= '<div class="torso">';
	$content.= '<div class="torso_upper" style="opacity:'.$colors["Torso Upper"].';"></div>';
	$content.= '<div class="torso_lower" style="opacity:'.$colors["Torso Lower"].';"></div>';
	$content.= '</div>';
	$content.= '<div class="right_arm">';
	$content.= '<div class="right_arm_upper" style="opacity:'.$colors["Right Arm Upper"].';"></div>';
	$content.= '<div class="right_arm_lower" style="opacity:'.$colors["Right Arm Upper"].';"></div';
	$content.= '<div class="right_hand" style="opacity:'.$colors["Right Hand"].';"></div>';
	$content.= '</div>';
	$content.= '<div class="clearfix"></div>';
	$content.= '<div class="left_leg">';
	$content.= '<div class="left_leg_upper" style="opacity:'.$colors["Left Leg Upper"].';"></div>';
	$content.= '<div class="left_leg_lower" style="opacity:'.$colors["Left Leg Lower"].';"></div>';
	$content.= '<div class="left_foot" style="opacity:'.$colors["Left Foot"].';"></div>';
	$content.= '</div>';
	$content.= '<div class="right_leg">';
	$content.= '<div class="right_leg_upper" style="opacity:'.$colors["Right Leg Upper"].';"></div>';
	$content.= '<div class="right_leg_lower" style="opacity:'.$colors["Right Leg Lower"].'"></div>';
	$content.= '<div class="right_foot" style="opacity:'.$colors["Right Foot"].';"></div>';
	$content.= '</div>';
	$content.= '<div class="clearfix"></div>';
	$content.= '</div>';

	return $content;	
}	



// supportet intervals: h = hour, d = day, w = week, m = month
function getPlayersCountByTimeInterval($interval = 'd')
{
	$sqlInter = "DAY";

	if($interval == 'h')
	{
		$sql = "SELECT COUNT(DISTINCT(teams.playerid)) as player_sum, DATE_FORMAT(FROM_UNIXTIME(rounds_full.time),'%d.%m.%Y - %h') AS order_interval, DATE_FORMAT(FROM_UNIXTIME(rounds_full.time),'%d.%m.%Y - %h') AS datum FROM rounds_full, teams WHERE teams.roundid = rounds_full.id GROUP BY order_interval ORDER BY rounds_full.time ASC";
	}
	else if($interval == 'd')
	{
		$sql = "SELECT COUNT(DISTINCT(teams.playerid)) as player_sum, DAY(FROM_UNIXTIME(rounds_full.time)) AS order_interval, DATE_FORMAT(FROM_UNIXTIME(rounds_full.time),'%d.%m.%Y') AS datum FROM rounds_full, teams WHERE teams.roundid = rounds_full.id GROUP BY order_interval ORDER BY rounds_full.time ASC";
	}
	else if($interval == 'w')
	{
		$sql = "SELECT COUNT(DISTINCT(teams.playerid)) as player_sum, WEEK(FROM_UNIXTIME(rounds_full.time)) AS order_interval, DATE_FORMAT(FROM_UNIXTIME(rounds_full.time),'%d.%m.%Y') AS datum FROM rounds_full, teams WHERE teams.roundid = rounds_full.id GROUP BY order_interval ORDER BY rounds_full.time ASC";

	}
	else if($interval == 'm')
	{
		$sqlInter = "MONTH";
		
		$sql = "SELECT COUNT(DISTINCT(teams.playerid)) as player_sum, MONTH(FROM_UNIXTIME(rounds_full.time)) AS order_interval, DATE_FORMAT(FROM_UNIXTIME(rounds_full.time),'%d.%m.%Y') AS datum FROM rounds_full, teams WHERE teams.roundid = rounds_full.id GROUP BY order_interval ORDER BY rounds_full.time ASC";
	}
	else
	{
		return false;
	}
	
	$db = new mbdb();
	
	if($sql == "") return false;
	
	return $db->query_assoc($sql);
}

function getPlayerPoints($playerid)
{
	$db = new mbdb();
	
	$sql = "SELECT SUM(points) as points FROM stats_rounds_players WHERE palyerid = '$playerid'";
	
	return $db->query_assoc($sql);
}

function getPlayerHeadshoots($playerid)
{
	$db = new mbdb();
	
	$sql = "SELECT COUNT(`action`) as kills FROM actions_full WHERE `mod` = 1 AND `action` = 'K' AND euid = '$playerid'";
	
	return $db->query_assoc($sql);
}

function getPlayerStatsRoundsRow($playerid)
{
	$db = new mbdb();
	
	$sql = "SELECT SUM(points) as points, SUM(kills) as kills, SUM(deaths) as deatha, SUM(teamkills) as teamkills, SUM(suizide) as suizide, SUM(sum_damage_inflicted) as damage_inflicted, SUM(sum_damage_recieved) as damage_recieved, SUM(won) as won, SUM(sum_kills_stand) as kills_stand, SUM(sum_kills_crouch) as kills_crouch, SUM(sum_kills_prone) as kills_prone FROM stats_rounds_players WHERE playerid = '$playerid'";
	
	return $db->query_assoc($sql);
}

function getPlayersTotalPointsListed()
{
	$db = new mbdb();
	$sql = "SELECT SUM(points) as Punkte, aliases.hash as Spieler FROM stats_rounds_players, aliases WHERE aliases.id = stats_rounds_players.playerid GROUP BY playerid ORDER BY Punkte DESC";
	return $db->query_assoc($sql);
}

function getPlayersTotalKillsListed()
{
	$db = new mbdb();
	$sql = "SELECT SUM(kills) as pkt, aliases.hash as player FROM stats_rounds_players, aliases WHERE aliases.id = stats_rounds_players.playerid GROUP BY playerid ORDER BY pkt DESC";
	return $db->query_assoc($sql);
}


function getPlayersTotalDeathsListed()
{
	$db = new mbdb();
	$sql = "SELECT SUM(deaths) as pkt, aliases.hash as player FROM stats_rounds_players, aliases WHERE aliases.id = stats_rounds_players.playerid GROUP BY playerid ORDER BY pkt DESC";
	return $db->query_assoc($sql);
}


function getPlayersTotalTeamKillsListed()
{
	$db = new mbdb();
	$sql = "SELECT SUM(teamkills) as pkt, aliases.hash as player FROM stats_rounds_players, aliases WHERE aliases.id = stats_rounds_players.playerid GROUP BY playerid ORDER BY pkt DESC";
	return $db->query_assoc($sql);
}


function getPlayersPointsGameline($playerid)
{
	$db = new mbdb();
	
	$sql = "SELECT SUM(points) as Punkte, DATE_FORMAT(FROM_UNIXTIME(games_full.time),'%d.%m.%Y') as datum FROM stats_rounds_players, games_full WHERE playerid = '$playerid' AND games_full.id = gameid GROUP BY gameid ORDER BY games_full.time ASC";
	
	$arr = $db->query_assoc($sql);
	
	$ret = array();
	$total = 0;
	$i = 0;
	
	foreach($arr as $r)
	{
		$total+= $r['Punkte'];
		
		$ret[$i]['total'] = $total;
		$ret[$i]['punkte'] = $r['Punkte'];
		$ret[$i]['datum'] = $r['datum'];
		$i++;
	}

	return $ret;
}
/*
function getPlayersKillsFullGameline($playerid)
{
	$db = new mbdb();
	
	$sql = "SELECT SUM(kills) as sum_kills, SUM(deaths) as sum_death, teamkills, suizide, kdr, DATE_FORMAT(FROM_UNIXTIME(games_full.time),'%d.%m.%Y - %H:%i') as datum, games_full.time FROM stats_rounds_players, games_full WHERE playerid = '$playerid' AND games_full.id = gameid GROUP BY gameid ORDER BY games_full.time ASC";
	
	
	return $db->query_assoc($sql);
}
*/
function getPlayersHighscoresForTables($playerid = NULL)
{
	$db = new mbdb();
	
	if(!isset($playerid))
	{
		$sql = "SELECT SUM(kills) as sum_kills, SUM(deaths) as sum_death, SUM(teamkills) as sum_teamkills, SUM(suizide) as sum_suizide, SUM(points) as sum_points, aliases.hash as name, COUNT(DISTINCT(gameid)) as sum_games, playerid FROM stats_rounds_players, aliases WHERE aliases.id = playerid AND aliases.hash != '' GROUP BY playerid ORDER BY sum_points DESC";
	}
	else
	{
		$sql = "SELECT SUM(kills) as sum_kills, SUM(deaths) as sum_death, SUM(teamkills) as sum_teamkills, SUM(suizide) as sum_suizide, SUM(points) as sum_points, aliases.hash as name, COUNT(DISTINCT(gameid)) as sum_games, playerid FROM stats_rounds_players, aliases WHERE aliases.id = playerid AND aliases.hash != '' AND playerid = '$playerid'";
	}
	$ret = array();
	$i = 0;
	$arr = $db->query_assoc($sql);

	foreach($arr as $r)
	{
		$k = $r['sum_kills'];
		$d = ($r['sum_death']==0)?1:$r['sum_death'];
		
		$kdr = $k / $d;
		
		$arr[$i]['kdr'] = $kdr;
		
		$i++;
	}
	
	return $arr;
}

function getPlayersKillsFullGameline($playerid, $interval = "g") {
    $db = new mbdb();

    if($interval == "g") {
        $sql = "SELECT SUM(kills) as sum_kills, SUM(deaths) as sum_death, teamkills, suizide, kdr, DATE_FORMAT(FROM_UNIXTIME(games_full.time),'%d.%m.%Y - %H:%i') as datum, games_full.time FROM stats_rounds_players, games_full WHERE playerid = '$playerid' AND games_full.id = gameid GROUP BY gameid ORDER BY games_full.time ASC";

    }else if($interval == "gw") {
        // Fr-Sa

    }else if($interval == "d") {
        $sql = "SELECT SUM(kills) as sum_kills, SUM(deaths) as sum_death, teamkills, suizide, kdr, DATE_FORMAT(FROM_UNIXTIME(games_full.time),'%d.%m.%Y') as datum, games_full.time FROM stats_rounds_players, games_full WHERE playerid = '$playerid' AND games_full.id = gameid GROUP BY datum ORDER BY games_full.time ASC";

    }else if($interval == "w") {
        $sql = "SELECT SUM(kills) as sum_kills, SUM(deaths) as sum_death, teamkills, suizide, kdr, DATE_FORMAT(FROM_UNIXTIME(games_full.time),'%u') as datum, games_full.time FROM stats_rounds_players, games_full WHERE playerid = '$playerid' AND games_full.id = gameid GROUP BY DATE_FORMAT(FROM_UNIXTIME(games_full.time),'%u') ORDER BY games_full.time ASC";

    }else if($interval == "m") {
        $sql = "SELECT SUM(kills) as sum_kills, SUM(deaths) as sum_death, teamkills, suizide, kdr, DATE_FORMAT(FROM_UNIXTIME(games_full.time),'%m.%Y') as datum, games_full.time FROM stats_rounds_players, games_full WHERE playerid = '$playerid' AND games_full.id = gameid GROUP BY datum ORDER BY games_full.time ASC";
    }

    return $db->query_assoc($sql);
}

?>