<?php
/*
function d($e) {
    echo '<pre>';
    var_dump($e);
    echo '</preY';
};

include('../misc/serv_db.inc.php');
include('../rules/player_stat_array.php');
include('../rules/game_rules.php');
include('../misc/class.extendedArray.php');
*/


function processWeaponsPerRound($start_id=0)
{

    $weapons = new ExtendedArray();
    $mods = new ExtendedArray();

    $weapons->fill('weapons');
    $mods->fill('mods');

    $db1 = new mbdb();
    $ply = array();

    $start_id = intval($start_id);
    $lid = $start_id;

    $sql = "SELECT * FROM games_full WHERE id > $lid AND type != 16 ORDER BY id ASC";
    $games = $db1->query_assoc($sql);

    if(!is_array($games)) return;



    foreach($games as $game)
    {
        $rnds = $game['rounds'];
        $gameid = $game['id'];

        //$db1->query_db("SELECT * FROM actions_full WHERE roundid IN($rnds) ORDER BY id ASC");
        $arRound = explode(",", $rnds);
        $cRounds = 0;
        $gameid = 0;

        foreach($arRound as $rid)
        {
            $round = $db1->query_assoc("SELECT * FROM actions_full WHERE roundid = $rid ORDER BY id ASC");
            $ply = array();

            foreach($round as $r)
            {
                $action = $r['action'];
                $roudnid = $r['id'];

                if($action == 'ShutdownGame')
                {
                    $cRounds++;
                }
                else
                {

                }


                if($action == "K")
                {						// weapon_grouped_id
                    $g_id = $weapons->getAditionalContentFromField('weapon_group_id', $r['weapon']);
                    if(!isset($ply[$r['euid']][$g_id])) $ply[$r['euid']][$g_id] = 0;
                    $ply[$r['euid']][$g_id]+=1;

                    $w_id = $weapons->getAditionalContentFromField('weapon_grouped_id', $r['weapon']);
                    if(!isset($ply[$r['euid']]['w'.$w_id])) $ply[$r['euid']]['w'.$w_id] = 0;
                    $ply[$r['euid']]['w'.$w_id]+=1;

                }
            }

            $c = count($ply);
            $keys = array_keys($ply);

            $sql_weapons = "INSERT INTO stats_weapons_players VALUES ";
            $sql_weapons_all = "INSERT INTO stats_weapons_all_players VALUES ";

            for($i=0;$i<$c;$i++)
            {
                $gameid =   $game['id'];
                $playerid = $keys[$i];
                $riffles =  (!isset($ply[$keys[$i]][1])) ? 0 : $ply[$keys[$i]][1];
                $smg =      (!isset($ply[$keys[$i]][3])) ? 0 : $ply[$keys[$i]][3];
                $shotgun =  (!isset($ply[$keys[$i]][2])) ? 0 : $ply[$keys[$i]][2];
                $sniper =   (!isset($ply[$keys[$i]][4])) ? 0 : $ply[$keys[$i]][4];
                $pistols =  (!isset($ply[$keys[$i]][6])) ? 0 : $ply[$keys[$i]][6];
                $lmg =      (!isset($ply[$keys[$i]][5])) ? 0 : $ply[$keys[$i]][5];

                $sql_weapons.= "('NULL', '$gameid', '$playerid', '$riffles', '$smg', '$shotgun', '$sniper', '$pistols', '$lmg'),";

                $sql_one = "('', '$gameid', '$playerid',";
                for($j=1;$j<67;$j++) {
                    $v = (!isset($ply[$keys[$i]]['w'.$j])) ? 0 : $ply[$keys[$i]]['w'.$j];
                    $sql_one.= "'$v',";
                }

                $sql_one = substr($sql_one, 0, strlen($sql_one)-1);
                $sql_one.= "),";
                $sql_weapons_all.= $sql_one;
                //echo $sql_one;

            }



            //remove last komma
            $sql_weapons = substr($sql_weapons, 0, strlen($sql_weapons)-1);
            $sql_weapons_all = substr($sql_weapons_all, 0, strlen($sql_weapons_all)-1);

            $db1->query_db($sql_weapons);
            $db1->query_db($sql_weapons_all);

           //echo $sql_weapons_all;
           // echo "<br />";
        }
    }
}
//echo $sql_weapons_all;
//processWeaponsPerRound(0);
?>