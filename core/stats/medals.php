<?php

//require_once '../misc/serv_db.inc.php';

function d($e) {
    echo '<pre>';
    var_dump($e);
    echo '</pre>';
}

$coef = 55;
$coef_g = 25;
$m_colors = array(
    0 => array( 'color' => '',
                'from' => 0,
                'to' => 20),
    1 => array( 'color' => 'green',
                'from' => 20,
                'to' => 40),
    2 => array( 'color' => 'brown',
                'from' => 40,
                'to' => 60),
    3 => array( 'color' => 'red',
                'from' => 60,
                'to' => 80),
    4 => array( 'color' => 'blue',
                'from' => 80,
                'to' => 100),
    5 => array( 'color' => 'gold',
                'from' => 100,
                'to' => 999),
);

$groups = array('Riffles', 'SMG', 'Shotgun', 'Sniper', 'Pistols', 'LMG', 'Granate', 'RPG', 'Claymore', 'C4');
$counts = array(6, 5, 2, 4, 3, 4, 1, 1, 1, 1);

function getWeaponsMedalsForPlayer($player_id)
{
    global $m_colors, $coef, $coef_g, $groups;
    $db = new mbdb();
    $ret = array();
    $i = 0;

    $result = $db->query_rows("SELECT SUM(riffles) AS riffles, SUM(smg) AS smg, SUM(shotgun) AS shotgun, SUM(sniper) AS sniper, SUM(pistols) AS pistols, SUM(lmg) AS lmg, SUM(w7) AS granate, SUM(w11) AS rpg, SUM(w24) AS claymore, SUM(w51) AS c4 FROM stats_weapons_players, stats_weapons_all_players WHERE puid = '".$player_id."' AND playerid = puid AND gameid = game_id");

    foreach($result as $resk => $res) {
        $co = ($i < 6) ? $coef : $coef_g;
        $value = floor($res / $co);
        $value = ($value > $co) ? $co : $value;
        $perc = (100 / $co) * $value;

        foreach($m_colors as $col) {
            if(($col['from'] <= $perc) && ($perc < $col['to'])) {
                $ret[] = array('group' => $groups[$resk], 'lvl' => $value, 'sum' => $res*1, 'precent' => $perc, 'color' => $col['color']);
                continue;
            }
        }
        $i++;
    }

    return $ret;
}

function getGameMedalsForPlayer($player_id)
{
    global $m_colors;
    $db = new mbdb();
    $max_tatics = array(4063, 1301, 40, 27, 43, 19);
    $result = $db->query_rows("SELECT SUM(sKC) AS sKC, SUM(sKD) AS sKD, SUM(sFC) AS sFC, SUM(sFR) AS sFR, SUM(sBP) AS sBP, SUM(sBD) AS sBD FROM stats_tatics_players WHERE playerid = '".$player_id."'");
    $i = 0;
    $groups = array('sKC', 'sKD', 'sFC', 'sFR', 'sBP', 'sBD');

    foreach($result as $resk => $res) {
        $value = (100 / $max_tatics[$i]) * $res;
        $perc = $value;
        foreach($m_colors as $col) {
            if(($col['from'] <= $perc) && ($perc < $col['to'])) {
                $ret[] = array('group' => $groups[$resk], 'lvl' => $value, 'sum' => $res*1, 'precent' => $perc, 'color' => $col['color']);
                continue;
            }
        }
        $i++;
    }

    return $ret;
}

?>