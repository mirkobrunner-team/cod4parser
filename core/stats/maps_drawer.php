<?php
require_once '../misc/serv_db.inc.php';
$data = array();
$mapRect = array();

function printSpawnPoints($map_id, $name="")
{
	$sql = "SELECT * FROM spawnpoints WHERE mapid = '$map_id'";
	$db = new mbdb();
	
	$db->query_db($sql);
	if($db->nrows == 0) return;
	
	$imageWidth = 400;


	$vecs = array();
	$minX = 0;
	$maxX = 1;
	$maxZ = 0;
	$minZ = 0;
	$minY = 0;
	$maxY = 4;
	$modX = 0;
	$modY = 0;
	$min = 0;
	$max = 0;
	$mult = 0;
	$spotsize = 1;
	$image = ImageCreate($imageWidth, $imageWidth); 
	$white = imagecolorallocate($image, 255, 255, 255);
	$black = imagecolorallocate($image, 0, 0, 0);
	$red   = imagecolorallocate($image, 255, 0, 0);
	

	while($r = mysqli_fetch_row($db->result))
	{
		$tvc = explode(",", $r[1]);	
		
		$x = $tvc[1];
		$y = $tvc[2];
		
		if($minX > $x) $minX = $x;
		else if($maxX < $x) $maxX = $x;

		if($minY > $y) $minY = $y;
		else if($maxY < $y) $maxY = $y;
		
		//Z-Coordinate is not needed yet
		$vecs[] = array($x, $y, $tvc[3]);
	}

	$db->close_result();	

	$min = ($minX < $minY) ? $minX : $minY;
	$max = ($maxX > $maxY) ? $maxX : $maxY;

	if($min < 0) $min = $min * -1;
	if($max < 0) $max = $max * -1;
	
	$mult = ($max > $min) ? $max : $min;

	$norm = $imageWidth / $mult;
	
	imagerectangle($image, 0, 0, $imageWidth - 1, $imageWidth - 1, $black);

	//process real map size
	$xis = (($minX + $mult) * 0.5) * $norm;
	$yis = (($minY + $mult) * 0.5) * $norm;
	$xas = (($maxX + $mult) * 0.5) * $norm;
	$yas = (($maxY + $mult) * 0.5) * $norm;
	
	
	
	imagerectangle($image, $xis, $yis, $xas + $spotsize, $yas + $spotsize, $red);
	
	//rotate 90 degrees
	//$cx = $cy = $imageWidth * 1;
	//$ang = 3.1434892 * 2;
    $ang = M_PI * 0.5;

	foreach($vecs as $vector)
	{

       // rotatePoints(0, 0, $ang, $vector);

		$x = $vector[0];
		$y = $vector[1];
        $z = $vector[0];

		$ox = $x;
		$oy = $y;

		//rotate 90 degrees
		//$x = (($x - $cx) * cos($ang)) - (($cy - $y) * sin($ang));
		//$y = (($cy - $y) * cos($ang)) - (($x - $cx) * sin($ang));



		$x = (($x + $mult) * 0.5) * $norm;
		$y = (($y + $mult) * 0.5) * $norm;
    	$z = (($z + $mult) * 0.5) * $norm;

        $z = ($z < 0) ? 1 : $z;

        $r = $z * 0.5;
        $g = $z * 0.5;
        $b = $z * 0.5;

        $black = imagecolorallocate($image, $r, $g, $b);
		imagerectangle($image, $x, $y, $x+2, $y+2, $black);
	}

	$cont = "";


	//test with shipment
	if($map_id==31)
	{
		$cont = '<img id="spimage" src="../maps/testmap.jpg" width="400" height="400" />';
	}

	$filename = "/var/customers/webs/Mirko/verwackeltes/test/cod4parser/maps/dump".$map_id.".png";
	$b = ImagePNG($image, $filename);
	ImageDestroy($image);

	$cont.= '<img id="spimage" src="../maps/dump'.$map_id.'.png" />';
    $cont.= '<p>Schwarze Punkte = Spawnpunkte<br />Roter Rahmen = Kartengr&ouml;&szlig;e gesch&auml;tzt<br />';
    $cont.= 'Umso heller der Punkt umso höher liegt dieser.</p>';

	return $cont;
}

function printSpawnPoints_new_($rnds, $js = false, $flip = 1, $scal = 1, $newAngel = 0)
{
	global $data, $mapRect;

	$db = new mbdb();
	$db->query_db("SELECT * FROM actions_full WHERE roundid IN($rnds) AND (pos_x != 0 AND pos_y != 0 AND pos_z != 0) ORDER BY id ASC");
	if($db->nrows == 0) return;

	$imageWidth = 512;
	$imageCenter = $imageWidth * 0.5;

    // mult und norm für x und y separat berrechnen

	$vecs = array();
	$minX = 0;
	$maxX = 4;
	$maxZ = 0;
	$minZ = 0;
	$minY = 0;
	$maxY = 4;
	$modX = 0;
	$modY = 0;
	$min = 0;
	$max = 0;
	$mult = 0;
	$spotsize = 1;

	if($js == true)
	{
		$image = ImageCreate($imageWidth, $imageWidth); 
		$white = imagecolorallocate($image, 255, 255, 255);
		$black = imagecolorallocate($image, 0, 0, 0);
		$red   = imagecolorallocate($image, 255, 0, 0);
	}

	while($r = mysqli_fetch_row($db->result))
	{
		$x = $r[15];
		$y = $r[16];
		$z = $r[17];
		
		$x*= $flip;
		//$y*= $flip;
		//$z*= $flip;

		if($minX > $x) $minX = $x;
		if($maxX < $x) $maxX = $x;
        if($maxZ < $z) $maxZ = $z;

		if($minY > $y) $minY = $y;
		if($maxY < $y) $maxY = $y;
        if($minZ < $z) $minZ = $z;

		if($x < 0)
		{
			$t = ($x < 0) ? $x*-1 : $x;
			if($t > $modX) $modX = $t;
		}

		if($y < 0)
		{
			$t = ($y < 0) ? $y*-1 : $y;
			if($t > $modY) $modY = $t;
        }
		//Z-Coordinate is not needed yet
		$vecs[] = array($x, $y, $r[17], $r[3]);
		
		$data[] = $r;
	}
	
	$db->close_result();

	$spanx = $maxX - $minX;
	$spany = $maxY - $minY;
	$spanx = $imageWidth / $spanx;
	$spany = $imageWidth / $spany;
	$span = ($spanx < $spany) ? $spanx : $spany;

    $mindX = $minX;
    $mindY = $minY;

	$span*=0.9;
	
	$modX = $modX * -1;
	$modY = $modY * -1;
	
	if($min < 0) $min = $min * -1;
	if($max < 0) $max = $max * -1;

	$norm = $imageWidth / $mult;

	if($js == false)
		imagerectangle($image, 0, 0, $imageWidth - 1, $imageWidth - 1, $black);

	$_mx2 = ($maxX - $modX) * $span;
	$_my2 = ($maxY - $modY) * $span;
	//process real map size
	$mapRect['x1'] = 0;
	$mapRect['y1'] = 0;
	$mapRect['x2'] = $_mx2 * $scal;
	$mapRect['y2'] = $_my2 * $scal;

	if($js == false)
		imagerectangle($image, 0, 0,$mapRect['x2'] + $spotsize, $mapRect['y2'], $red);
	
	//rotate 90 degrees

	$cx = $cy = $imageWidth * 1;
	$ang = $newAngel;
	$_s = sin($ang);
	$_c = cos($ang);
	$_px = $mapRect['x2'] * 0.5;
	$_py = $mapRect['y2'] * 0.5;

	$i = 0;

    $maxY = $maxX = $minX = $minY = 0;
    $modX = $modY = 0;

	foreach($vecs as $vector)
	{

       // rotatePoints(0, 0, $ang, $vector);
		$x = $vector[0];
		$y = $vector[1];
        $z = $vector[2];
        $a = $vector[3];

		$x+= ($mindX < 0) ? $mindX * -1 : $mindX;
		$y+= ($mindY < 0) ? $mindY * -1 : $mindY;



		$x*= $span;
		$y*= $span;

		$x*= $scal;
		$y*= $scal;

        $p = rotatePoint($_px, $_py, $x, $y, $ang);

        $x = $p[0];
        $y = $p[1];

        $z = ($z < 0) ? 1 : $z;

        if($x < 0)
		{
			$t = ($x < 0) ? $x*-1 : $x;
			if($t > $modX) $modX = $t;
		}

		if($y < 0)
		{
			$t = ($y < 0) ? $y*-1 : $y;
			if($t > $modY) $modY = $t;
        }

        if($minX > $x) $minX = $x;
		if($maxX < $x) $maxX = $x;
        if($maxZ < $z) $maxZ = $z;

		if($minY > $y) $minY = $y;
		if($maxY < $y) $maxY = $y;
        if($minZ < $z) $minZ = $z;



		if($js == false)
		{
	        if($a == "SP")
	        {
	            $r = $z * 0.5;
	            $g = $z * 0.5;
	            $b = $z * 0.5;

	            $r = 255;
	            $g = $b = 0;
	        }
	        else if($a == "ST")
	        {
	            $r = $z * 0.5;
	            $g = $z * 0.5;
	            $b = $z * 0.5;
	        }
	        else
	        {
		        $r = $z * 0.5;
	            $g = 60;
	            $b = 255;
	        }

           //	$black = imagecolorallocate($image, $r, $g, $b);
        	//$black = getColorByPlayersId($r[4], 27, $z);
		 //	imagerectangle($image, $x, $y, $x+2, $y+2, $black);
		}

        $data[$i]['pos_x'] = $x;
		$data[$i]['pos_y'] = $y;
		$data[$i]['pos_z'] = $z;
		$i++;
	}

    $i = 0;

    foreach($vecs as $vector)
    {
        $x = $data[$i]['pos_x'];
        $y = $data[$i]['pos_y'];

        $x+= $modX;
        $y+= $modY;

        $data[$i]['pos_x'] = $x;
		$data[$i]['pos_y'] = $y;

        $i++;
    }

    $_mx2 = $maxX + $modX;
	$_my2 = $maxY + $modY;

    $mapRect['x2'] = $_mx2;
	$mapRect['y2'] = $_my2;

	$cont = "";

	if($js == true)
	{
		return "";
	}

	//test with shipment
	if($map_id==31)
	{
		$cont = '<img id="spimage" src="../maps/testmap.jpg" width="400" height="400" />';
	}

	$filename = "/var/customers/webs/Mirko/verwackeltes/test/cod4parser/maps/dump.png";
	$b = ImagePNG($image, $filename);
	ImageDestroy($image);

	$cont.= '<img id="spimage" src="../maps/dump.png" />';
    $cont.= '<p>Rote Punkte = Spawnpunkte<br />Schwarz Punkte = Spieler (Haltung ge&auml;ndert)<br />Blaue Punkte = Spieler getötet<br />Roter Rahmen = Kartengr&ouml;&szlig;e gesch&auml;tzt<br />';
    $cont.= 'Umso heller der Punkt umso höher liegt dieser.</p>';

	$cont.= " ".count($vecs)." Punkte gefunden";

	return $cont;
}

function printAllPoints($mapid, $js = false)
{
	global $data, $mapRect;

	$db = new mbdb();
	$db->query_db("SELECT * FROM actions_full, rounds_full WHERE rounds_full.map = $mapid AND (actions_full.pos_x != 0 AND actions_full.pos_y != 0 AND actions_full.pos_z != 0) AND actions_full.roundid = rounds_full.id");
	if($db->nrows == 0) return;
	
	$imageWidth = 512;
	$imageCenter = $imageWidth * 0.5;

    // mult und norm für x und y separat berrechnen

	$vecs = array();
	$minX = 0;
	$maxX = 1;
	$minY = 0;
	$maxY = 4;
	$modX = 0;
	$modY = 0;
	$spotsize = 1;
	

	$image = ImageCreate($imageWidth, $imageWidth);
	$white = imagecolorallocate($image, 255, 255, 255);
	$black = imagecolorallocate($image, 0, 0, 0);
	$red   = imagecolorallocate($image, 255, 0, 0);


	while($r = mysqli_fetch_row($db->result))
	{

		if($r[3] == "ST" || $r[3] == "SP" || $r[3] == "K" || $r[3] == "D")
		{

    		$x = $r[15];
    		$y = $r[16];
            $z = $r[17];

    		if($minX > $x) $minX = $x;
    		if($maxX < $x) $maxX = $x;
            if($maxZ < $z) $maxZ = $z;

    		if($minY > $y) $minY = $y;
    		if($maxY < $y) $maxY = $y;
            if($minZ < $z) $minZ = $z;

    		if($x < 0)
    		{
    			$t = ($x < 0) ? $x*-1 : $x;
    			if($t > $modX) $modX = $t;
    		}

    		if($y < 0)
    		{
    			$t = ($y < 0) ? $y*-1 : $y;
    			if($t > $modY) $modY = $t;
    		}



    		//Z-Coordinate is not needed yet
    		$vecs[] = array($x, $y, $r[17], $r[3]);

    		$data[] = $r;
		}
	}
	
	$db->close_result();

	$spanx = $maxX - $minX;
	$spany = $maxY - $minY;
	$spanx = $imageWidth / $spanx;
	$spany = $imageWidth / $spany;
	$span = ($spanx < $spany) ? $spanx : $spany;
	
	$modX = $modX * -1;
	$modY = $modY * -1;
	
	if($min < 0) $min = $min * -1;
	if($max < 0) $max = $max * -1;

	$norm = $imageWidth / $mult;


	imagerectangle($image, 0, 0, $imageWidth - 1, $imageWidth - 1, $black);

	//process real map size
	$mapRect['x1'] = 0;
	$mapRect['y1'] = 0;
	$mapRect['x2'] = ($maxX - $modX) * $span;
	$mapRect['y2'] = ($maxY - $modY) * $span;
	

	imagerectangle($image, 0, 0,$mapRect['x2'] + $spotsize, $mapRect['y2'], $red);
	
	//rotate 90 degrees
	//$cx = $cy = $imageWidth * 1;
	//$ang = 3.1434892 * 2;

	$i = 0;

	foreach($vecs as $vector)
	{

       // rotatePoints(0, 0, $ang, $vector);
		$x = $vector[0];
		$y = $vector[1];
        $z = $vector[2];
        $a = $vector[3];
        
		$x+= ($minX < 0) ? $minX * -1 : $minX;
		$y+= ($minY < 0) ? $minY * -1 : $minY;

		$x*= $span;
		$y*= $span;


        $z = ($z < 0) ? 1 : $z;

        if($a == "SP")
        {
            $r = $z * 0.5;
            $g = $z * 0.5;
            $b = $z * 0.5;

            $r = 255;
            $g = $b = 0;
        }
        else if($a == "ST")
        {
            $r = $z * 0.5;
            $g = $z * 0.5;
            $b = $z * 0.5;
        }
        else
        {
	        $r = 43;
            $g = 60;
            $b = 255;
        }

    	//$black = imagecolorallocate($image, $r, $g, $b);
    	//$black = getColorByPlayersId($r[4], 27, $z);
		imagerectangle($image, $x, $y, $x+1, $y+1, $black);

		$data[$i]['pos_x'] = $x;
		$data[$i]['pos_y'] = $y;
		$data[$i]['pos_z'] = $z;
		$i++;
	}

	$cont = "";


/*
	//test with shipment
	if($map_id==31)
	{
		$cont = '<img id="spimage" src="../maps/testmap.jpg" width="400" height="400" />';
	}
*/

	$filename = "/var/customers/webs/Mirko/verwackeltes/test/cod4parser/maps/dumpall".$mapid.".png";

	$b = ImagePNG($image, $filename);

	$b = file_exists($filename);

	var_dump($image);
	ImageDestroy($image);

	$cont.= '<img id="spimage" src="../maps/dumpall'.$mapid.'.png" />';
    $cont.= '<p>Rote Punkte = Spawnpunkte<br />Schwarz Punkte = Spieler (Haltung ge&auml;ndert)<br />Blaue Punkte = Spieler getötet<br />Roter Rahmen = Kartengr&ouml;&szlig;e gesch&auml;tzt<br />';
    $cont.= 'Umso heller der Punkt umso höher liegt dieser.</p>';

	$cont.= " ".count($vecs)." Punkte gefunden";

	return $cont;
}


function getMapPoints($mapid, $scal = 1, $flip = 1)
{
	$arr = array();

	$db = new mbdb();
	$db->query_db("SELECT * FROM actions_full, rounds_full WHERE rounds_full.map = $mapid AND (actions_full.pos_x != 0 AND actions_full.pos_y != 0 AND actions_full.pos_z != 0) AND actions_full.roundid = rounds_full.id");
	if($db->nrows == 0) return;
	

	$imageWidth = 512;
	$imageCenter = $imageWidth * 0.5;

    // mult und norm für x und y separat berrechnen



    $mirror_x = -1;//-1;


	$vecs = array();
	$minX = 0;
	$maxX = 4;
	$maxZ = 0;
	$minZ = 0;
	$minY = 0;
	$maxY = 4;
	$modX = 0;
	$modY = 0;
	$min = 0;
	$max = 0;
	$mult = 0;
	$spotsize = 1;

    if($scal <= 0)
    {
        $scal = 1;
    }

	while($r = mysqli_fetch_row($db->result))
	{
		$x = $r[15];
		$y = $r[16];
		$z = $r[17];

        $x*= $scal;
        $y*= $scal;
        $z*= $scal;

		if($minX > $x) $minX = $x;
		if($maxX < $x) $maxX = $x;
        if($maxZ < $z) $maxZ = $z;

		if($minY > $y) $minY = $y;
		if($maxY < $y) $maxY = $y;
        if($minZ < $z) $minZ = $z;

		if($x < 0)
		{
			$t = ($x < 0) ? $x*-1 : $x;
			if($t > $modX) $modX = $t;
		}

		if($y < 0)
		{
			$t = ($y < 0) ? $y*-1 : $y;
			if($t > $modY) $modY = $t;
        }
		//Z-Coordinate is not needed yet
		$vecs[] = array($x, $y, $z, $r[3]);
		#$arr[] = $r;
	}
	
	$db->close_result();

	$spanx = $maxX - $minX;
	$spany = $maxY - $minY;
	$spanx = $imageWidth / $spanx;
	$spany = $imageWidth / $spany;
	$span = ($spanx < $spany) ? $spanx : $spany;

	$span*=0.9;

	$modX = $modX * -1;
	$modY = $modY * -1;

	if($min < 0) $min = $min * -1;
	if($max < 0) $max = $max * -1;
	if($mult == 0) $mult = 1;
	
	$norm = $imageWidth / $mult;

	//process real map size
	$_mx2 = ($maxX - $modX) * $span;
	$_my2 = ($maxY - $modY) * $span;

	$mapRect['x1'] = 0;
	$mapRect['y1'] = 0;
	$mapRect['x2'] = $_mx2 * $scal;
	$mapRect['y2'] = $_my2 * $scal;
	
	$centerX = $mapRect['x2'] * .5;
	$centerY = $mapRect['y2'] * .5;

	$i = 0;

	foreach($vecs as $vector)
	{
       	rotatePoints($centerX, $centerY, M_PI * 1.5, $vector);
		#$x = mirrorPoints($mapRect['x2'], $vector[0]);
		$x = $vector[0];
		$y = $vector[1];
        $z = $vector[2];
        $a = $vector[3];

		$x+= ($minX < 0) ? $minX * -1 : $minX;
		$y+= ($minY < 0) ? $minY * -1 : $minY;

		$x*= $span;
		$y*= $span;
		
		$x = mirrorPoints($mapRect['x2'], $x);

        $z = ($z < 0) ? 1 : $z;

		$arr[$i]['pos_x'] = $x;
		$arr[$i]['pos_y'] = $y;
		$arr[$i]['pos_z'] = $z;
		$i++;
	}
	
	return $arr;
}

function computeHeatMap($mapid, $size = 5, $scal = 1, $flip)
{
	$dat = getMapPoints($mapid, $scal,  $flip);
    $quad = array();
    $new = array();

    $i=0;

    /*
    10px settup

    $roundfactor = 100;
    $r = 1 / $roundfactor;
    $w = 10;
    $rm = 1;
    */

    $roundfactor = $size * 10;
    $r = 1 / $roundfactor;
    $w = $size;
    $rm = 1;

    $h = 0;
	$v = 0;
    /*

    Hier eventeull z als durchnittswert mit einfliessen lassen

    */

    foreach($dat as $d)
    {
        $mx = round($d['pos_x'] * $r, $rm) * $roundfactor;
        $my = round($d['pos_y'] * $r, $rm) * $roundfactor;

        $key = $mx.".".$my;

        if(array_key_exists($key, $new))
        {
            $v = 0;
            $v = $new[$key];
            $v++;
            $new[$key] = $v;
            $quad[$i]['x'] = $mx;
            $quad[$i]['y'] = $my;
            $quad[$i]['value'] = $v;
        }
        else
        {
            $new[$key] = $v;
            $quad[$i]['x'] = $mx;
            $quad[$i]['y'] = $my;
            $quad[$i]['value'] = $v;
        }
        $i++;

        if($h < $v)
            $h = $v;
    }

    $retArray = array();
    $retArray['data'] = $quad;
    $retArray['max'] = $h;
    $retArray['count'] = $i;

    return $retArray;
}

function renderHeapMap($mapid, $size, $path = "")
{
    $dat = getMapPoints($mapid, 1);
    $quad = array();
    $new = array();
    $headMapQuadSize = 2;
    $i = 0;
    $r = 1;
    $h = 0;
    $v = 0;

    foreach($dat as $d)
    {
		$mx = round($d['pos_x'], 2);
		$my = round($d['pos_y'], 2);

        $key = $mx.".".$my;

        if(array_key_exists($key, $new))
        {
            $v = 0;
            $v = $new[$key];
            $v++;
            $new[$key] = $v;
            $quad[$key][0] = $mx;
            $quad[$key][1] = $my;
            $quad[$key][2] = $v;
        }
        else
        {
            $new[$key] = $v;
            $quad[$key][0] = $mx;
            $quad[$key][1] = $my;
            $quad[$key][2] = $v;
        }
        $i++;

        if($h < $v)
            $h = $v;
    }

    $cm = 255 / $h;

    $im = imagecreatetruecolor(524, 524);
    imagesavealpha($im, true);
	imagefilledrectangle($im, $x, $y, $size, $size, imagecolorallocatealpha($im, 255, 255, 255, 1));

    foreach($quad as $vec)
    {
        $x = $vec[0];
        $y = $vec[1];
        $c = $vec[2] * $cm;
	    $r = $c;
	    $g = 255 - $c;
	    $b = 0;
		
	    $col = imagecolorallocatealpha($im, $r, $g, $b, 100);
	    imagefilledrectangle($im, $x, $y, $x+$headMapQuadSize, $y+$headMapQuadSize, $col);
    }

	
    // Save the image
    imagepng($im, $path.'heap_map_'.$mapid.'.png');
    imagedestroy($im);

    return 'heap_map_'.$mapid.'.png';
}

function createColorFromValue(&$im, $m, $value)
{
    global $cm;

    $r = $value * $cm;
    $g = 255 - $value * $cm;
    $b = 0;

    $col = imagecolorallocatealpha($im, $r, $g, $b, 64);


    return $col;
}



function buildJSDataArray(&$array)
{
	echo json_encode($array);
}

function buildJSPlayerArray($game_id)
{
	$db = new mbdb();
	$db->query_db("SELECT teams.playerid as tid, aliases.hash as hash, teams.team FROM teams, aliases WHERE gameid = '$game_id' AND aliases.id = teams.playerid ORDER BY aliases.hash ASC");
	$arr;

	while($r = mysqli_fetch_assoc($db->result))
	{
		$arr[] = $r;
	}

	$db->close_result();

	echo json_encode($arr);

	return ;
}


function getColorByPlayersId($player_id, $maxid, $alpha)
{
    $n = 255 / $Maxid;

    $r = $player_id * $n;
    $g = $player_id - $player_id - $maxid * 5;
    $b = $player_id * $n;

    return imagecolorallocatealpha($image, $r, $g, $b, $alpha);
}


function normalizes($x, $y, $z)
{
	$a = sqrt(pow($x, 2) + pow($y, 2) + pow($z, 2));
	
	$x = $x / $a;
	$y = $y / $a;
	$z = $z / $a;
	
	return array($x, $y, $z);
}



function getMapPath($mapLogName)
{
	return 'http://k4f-in-berlin.de/fileadmin/images/Maps/'.$mapLogName."/compass_map_".$mapLogName.".jpg";
}




function pixelDistance($lat1, $lon1, $lat2, $lon2, $zoom)
{
    $x1 = $lon1*10000000;
    $y1 = $lat1*10000000;
    $x2 = $lon2*10000000;
    $y2 = $lat2*10000000;

    return sqrt(pow(($x1-$x2),2) + pow(($y1-$y2),2)) >> (21 - $zoom); //21 is the max zoom level
}


function rotatePoint($cx, $cy, $px, $py, $angle)
{
    $s = sin($angle);
    $c = cos($angle);

  // translate point back to origin:
    $px -= $cx;
    $py -= $cy;

  // rotate point
    $xnew = $px * $c - $py * $s;
    $ynew = $px * $s + $py * $c;

  // translate point back:
    $px = $xnew + $cx;
    $py = $ynew + $cy;

    return array($px, $py);
}

function rotatePoints($cx, $cy, $angle, &$vector)
{
    $s = sin($angle);
    $c = cos($angle);

    $x = $vector[0];
    $y = $vector[1];

    $x-= $cx;
    $y-= $cy;

    $_x = $x * $c - $y * $s;
    $_y = $x * $s + $y * $c;

    $vector[0] = $_x + $cs;
    $vector[1] = $_y + $cy;
}

function mirrorPoints($lineX, $x)
{
	//tauschen um $lineX 
	#$c = $lineX * .5;
	#$v = $x - $c;
	#$x+= $v; 
	
	return $x + ($lineX - $x) * 2;
	
	return $x;
	
	
	if($x < $c)
	{
		$v = $x - $c;
		$x+= $v; 	
	}
	else
	{
		$v = $c - $x;
		$x-= $v; 
	}
	return $x;
}



#renderHeapMap(29, 524, '/var/customers/webs/Mirko/verwackeltes/test/cod4parser/maps/');
#die(); 

$sql = "SELECT map, name FROM games_full, maps WHERE maps.id = map GROUP BY map;";
$db = new mbdb();
$db->query_db($sql);

while($r = mysqli_fetch_row($db->result))
{
	renderHeapMap($r[0], 524, '/var/customers/webs/Mirko/verwackeltes/test/cod4parser/maps/');
}

#$max = 255;
/*
for($i=0;$i<256;$i++) {
	
	$r = $i;
	$g = $max - $i;
	$b = 0;
	
	echo $r." ".$g." ".$b."<br />";
}
*/
?>