<?php
$rang = 50;

function processMasterShooter($playerid, $weaponType)
{
	$db = new mbdb();
	
	$sql = "SELECT COUNT(action) summe, weapons.name FROM actions_full, weapons WHERE actions_full.action = 'K' AND actions_full.euid = '$playerid' AND weapons.weapon_group_id = '$weaponType' AND weapons.id = actions_full.weapon GROUP BY weapons.weapon_grouped_id ORDER BY summe DESC";

	$res = $db->query_assoc($sql);
	$arr = array();
	$i = 0;

	foreach($res as $r)
	{
		$s = $r['summe'];
		
		$arr[$i] = $r;

        $stufe = $s / 50;

        $arr[$i]['mult'] = $stufe;
		$arr[$i]['rang'] = floor($stufe);
		
		$i++;
	}

	return $arr;
}

function processMasterHeadShooter($playerid, $weaponType)
{
	$db = new mbdb();
	
	$sql = "SELECT COUNT(action) summe, weapons.name FROM actions_full, weapons WHERE actions_full.action = 'K' AND actions_full.euid = '$playerid' AND `mod` = '1' AND weapons.weapon_group_id = '$weaponType' AND weapons.id = actions_full.weapon GROUP BY weapons.weapon_grouped_id ORDER BY summe DESC";

	$res = $db->query_assoc($sql);
	$arr = array();
	$i = 0;

	foreach($res as $r)
	{
		$s = $r['summe'];
		
		$arr[$i] = $r;

        $stufe = $s / 50;

        $arr[$i]['mult'] = $stufe;
		$arr[$i]['rang'] = floor($stufe);
		
		$i++;
	}

	return $arr;
}

function processMasterStandingShooter($playerid)
{
	$db = new mbdb();

	$sql = "SELECT SUM(sum_kills_stand) as kills_standing, SUM(sum_kills_crouch) as kills_crouching, SUM(sum_kills_prone) as kills_proning FROM stats_rounds_players WHERE playerid = '$playerid'";

	$res = $db->query_assoc($sql);

	$res = $res[0];

	$arr = array();

	$s = $res['kills_standing'];
    $stufe = $s / 30;
    $arr[0]['name'] = "Kills stehend";
    $arr[0]['mult'] = $stufe;
	$arr[0]['rang'] = floor($stufe);

	$s = $res['kills_crouching'];
    $stufe = $s / 30;
    $arr[1]['name'] = "Kills hockend";
    $arr[1]['mult'] = $stufe;
	$arr[1]['rang'] = floor($stufe);

	$s = $res['kills_proning'];
    $stufe = $s / 30;
    $arr[2]['name'] = "Kills liegend";
    $arr[2]['mult'] = $stufe;
	$arr[2]['rang'] = floor($stufe);	

	return $arr;
}

/*
Z�hlt wieviel eine Spielart gewonnen wurde
*/
function processWonCountByGametype($playerid, $gametyp)
{

}


/*
Anzahl stimmt irgendwie nicht mit der Realit�t �ber ein
*/
function processExtraWeaponCount($playerid, $weaponId, $rang)
{
	$db = new mbdb();
	
	$sql = "SELECT COUNT(action) as anzahl FROM actions_full WHERE euid = '$playerid' AND action = 'K' AND weapon = '$weaponId'";

	$res = $db->query_assoc($sql);
	$res = $res[0];
	
	$s = $res['anzahl'];
	$stufe = $s / $rang;
	$arr[0]['anzahl'] = $s;
    $arr[0]['mult'] = $stufe;
	$arr[0]['rang'] = floor($stufe);
	
	return $arr;
}


/*

*/
function processMultiGranateCount($playerid, $modId, $rang, $startid = 0)
{
	$db = new mbdb();
	
	$sql = "SELECT time, roundid FROM actions_full WHERE euid = '$playerid' AND action = 'K' AND `mod` = '$modId'";

	$res = $db->query_assoc($sql);
	
	$ltime = 0;
	$c = 0;
	
	foreach($res as $r)
	{
		if($ltime != $r['time'])
		{
			$ltime = $r['time'];
		}
		else 
		{
			$c++;
		}
	}	

	$res = $res[0];
	
	$s = $c;
	$stufe = $s / $rang;
	$arr[0]['anzahl'] = $s;
    $arr[0]['mult'] = $stufe;
	$arr[0]['rang'] = floor($stufe);
	
	return $arr;
}



function processMultiWeaponCount($playerid, $weaponId, $rang, $startid = 0)
{
	$db = new mbdb();
	
	$sql = "SELECT time, roundid FROM actions_full WHERE euid = '$playerid' AND action = 'K' AND weapon = '$weaponId'";
	
	$res = $db->query_assoc($sql);
	
	$ltime = 0;
	$c = 0;
	
	foreach($res as $r)
	{
		if($ltime != $r['time'])
		{
			$ltime = $r['time'];
		}
		else 
		{
			$c++;
		}
	}	

	$res = $res[0];

	$s = $c;
	$stufe = $s / $rang;
	$arr[0]['anzahl'] = $s;
    $arr[0]['mult'] = $stufe;
	$arr[0]['rang'] = floor($stufe);
	
	return $arr;
}


function processMultiAirstrikeCount($playerid, $weaponId, $rang, $startid = 0)
{
	$db = new mbdb();
	
	$sql = "SELECT time, roundid FROM actions_full WHERE euid = '$playerid' AND action = 'K' AND weapon = '$weaponId'";
	
	
	$res = $db->query_assoc($sql);
	
	$ltime = 0;
	$lround = 0;
	$c = 0;
	
	foreach($res as $r)
	{
		if($lround != $r['roundid'])
		{
			$lround = $r['roundid'];
			$c = 0;
		}
		else
		{

		}
		
		if($ltime < ($r['time']-30))
		{
			$ltime = $r['time'];
		}
		else 
		{
			$c++;
		}
	}

	$res = $res[0];
	
	$s = $c;
	$stufe = $s / $rang;
	$arr[0]['anzahl'] = $s;
    $arr[0]['mult'] = $stufe;
	$arr[0]['rang'] = floor($stufe);
	
	return $arr;
}

/*
Anzahl stimmt irgendwie nicht mit der Realit�t �ber ein
*/
function processExtraModCount($playerid, $modId, $rang)
{
	$db = new mbdb();
	
	$sql = "SELECT COUNT(action) as anzahl FROM actions_full WHERE euid = '$playerid' AND action = 'K' AND `mod` = '$modId'";
	
	$res = $db->query_assoc($sql);
	$res = $res[0];
	
	$s = $res['anzahl'];
	$stufe = $s / $rang;
	$arr[0]['anzahl'] = $s;
    $arr[0]['mult'] = $stufe;
	$arr[0]['rang'] = floor($stufe);
	
	return $arr;
}


//
function processSilencerKills($playerId, $rang)
{
	$ids = '42,67,69,75,81,94,104,105,110,111,114,117,134,158';
	
	$db = new mbdb();
	
	$sql = "SELECT COUNT(action) as anzahl FROM actions_full WHERE euid = '$playerId' AND action = 'K' AND weapon in ($ids)";

	$res = $db->query_assoc($sql);
	$res = $res[0];
	
	$s = $res['anzahl'];
	$stufe = $s / $rang;
	$arr[0]['anzahl'] = $s;
    $arr[0]['mult'] = $stufe;
	$arr[0]['rang'] = floor($stufe);
	
	return $arr;
}

function processGameWinner($playerId, $gametype, $rang)
{
	$db = new mbdb();

	
	$res = $db->query_assoc("SELECT COUNT(won) won FROM stats_rounds_players, games_full WHERE playerid = '$playerId' AND games_full.id = gameid AND games_full.type = '$gametype'");
	$res = $res[0];
	
	$s = $res['won'];
	$stufe = $s / $rang;
	$arr[0]['anzahl'] = $s;
    $arr[0]['mult'] = $stufe;
	$arr[0]['rang'] = floor($stufe);
	
	return $arr;
}

function processTeamGameWinner($playerId, $rang)
{
	$db = new mbdb();

	
	$res = $db->query_assoc("SELECT COUNT(won) won FROM stats_rounds_players, games_full, gametypes WHERE playerid = '$playerId' AND games_full.id = gameid AND gametypes.id = games_full.type AND gametypes.team = 1");
	$res = $res[0];
	
	$s = $res['won'];
	$stufe = $s / $rang;
	$arr[0]['anzahl'] = $s;
    $arr[0]['mult'] = $stufe;
	$arr[0]['rang'] = floor($stufe);

	return $arr;
}

function fast_distance($x1, $y1, $z1, $x2, $y2, $z2)
{
    $nx = $x2 - $x1;
    $ny = $y2 - $y1;
    $nz = $z2 - $z1;

    $nx*=$nx;
    $ny*=$ny;
    $nz*=$nz;

    $d = sqrt($nx + $ny + $nz);

    return $d;
}

function processWalkingDistanceTotal($playerid, $round = 0, $only = false)
{
    $db = new mbdb();
    if($only == false) {
        $sql = "SELECT actions_full.action, pos_x, pos_y, pos_z FROM `actions_full` WHERE (action = 'K' OR action LIKE 'D' OR action LIKE '%SP%' OR action = 'ST') AND puid = '$playerid' AND roundid > 0 AND (pos_x != 0.000000 AND pos_y != 0.000000 AND pos_z != 0.000000)";
    } else if($only == true) {
        $sql = "SELECT actions_full.action, pos_x, pos_y, pos_z FROM `actions_full` WHERE (action = 'K' OR action LIKE 'D' OR action LIKE '%SP%' OR action = 'ST') AND puid = '$playerid' AND roundid = '$round' AND (pos_x != 0.000000 AND pos_y != 0.000000 AND pos_z != 0.000000)";
    }


    $res = $db->query_db($sql);
    $dist = 0;
    $x = 0;
    $y = 0;
    $z = 0;
    $_x = 0;
    $_y = 0;
    $_z = 0;
    $d = 0;
	$i = 0;

	while($r = mysqli_fetch_row($db->result))
    {
        $_x = $r[1];
        $_y = $r[2];
        $_z = $r[3];

        if($r[0] == 'SP')
        {
            $dist+= $d;
        }
        else
        {
            $d+= fast_distance($x, $y, $z, $_x, $_y, $_z);
        }

        $x = $_x;
        $y = $_y;
        $z = $_z;
    }

    return ($d * 0.025) * 0.001;
}

//ACHTUNG erstmal nur gesch�tzt
function processWalkingDistanceWithBomb_sab($playerid, $round = 0)
{
	$db = new mbdb();
	
	//oki nur gametype sab
	//gez�hlt ab BT, BD
	//abbruch BL BP

	$gametype = 6;
	
	
	$sql = "SELECT actions_full.pos_x, actions_full.pos_y, actions_full.pos_z, actions_full.action FROM `actions_full`, `rounds_full` WHERE (action LIKE '%K%' OR action LIKE 'D' OR action LIKE '%SP%' OR action LIKE '%ST%' OR action LIKE '%BL%' OR action LIKE '%BD%' OR action LIKE '%BT%' OR action LIKE '%BP%') AND puid = $playerid AND roundid > $round AND rounds_full.id = roundid AND rounds_full.type = '$gametype' ORDER BY actions_full.id ASC";

	$res = $db->query_assoc($sql);
    $dist = 0;
    $x = 0;
    $y = 0;
    $z = 0;
    $_x = 0;
    $_y = 0;
    $_z = 0;
    $bomber = false;
    
    foreach($res as $r)
    {
	    if($r['action'] != 'BT' || $r['action'] != 'BD' || $r['action'] != 'BL' || $r['action'] != 'BP')
	    {
			$_x = $r['pos_x'];
        	$_y = $r['pos_y'];
        	$_z = $r['pos_z'];
        }
        
        if($r['action'] == 'BT' || $r['action'] == 'BD')
        {
	        $bomber = true;
	        $dist+= $d;

            if($i > 0)
            {
                $d = fast_distance($x, $y, $z, $_x, $_y, $_z);
            }
        }
        else if($r['action'] == 'BL' || $r['action'] == 'BP')
        {
	        $bomber = false;
        }
        
        if($bomber == true)
        {
        	$d+= fast_distance($x, $y, $z, $_x, $_y, $_z);
        }
        
        if($r['action'] != 'BT' || $r['action'] != 'BD' || $r['action'] != 'BL' || $r['action'] != 'BP')
	    {
			$x = $_x;
			$y = $_y;
			$z = $_z;
		}
    }
    
    return ($d * 0.025) * 0.001;
}


?>
