<?php

include('/var/customers/webs/Mirko/verwackeltes/test/cod4parser/core/rules/player_stat_array.php');
include('/var/customers/webs/Mirko/verwackeltes/test/cod4parser/core/rules/game_rules.php');
include('/var/customers/webs/Mirko/verwackeltes/test/cod4parser/core/stats/challenges.php');
/*
include('../rules/player_stat_array.php');
include('../rules/game_rules.php');
include('../misc/class.extendedArray.php');
include('../misc/serv_db.inc.php');
include('challenges.php');
*/
function checkIfThatKD(&$r, $teamPlay)
{
	if($r['puid'] == $r['euid'])
	{
		return 1;
	}
	else if($r['team'] == $r['eteam'] && $teamPlay == 1)
	{
		return 2;
	}
	else
	{
		return 3;
	}
}



function processPlayersPerRound($start_id)
{

	$types = new ExtendedArray();
	$gameActions = new ExtendedArray();
	$mods = new ExtendedArray();
	
	
	$types->fill('gametypes');
	$gameActions->fill('gameactions');
	$mods->fill('mods');
	
	$db1 = new mbdb();
	$ply = array();
	$isTeamPlay = false;

	$sab_melee = array('id' => 0, 'team' => 0, 'time' => 0, 'opfer' => 0);
	$sab_kill = array('id' => 0, 'team' => 0, 'time' => 0, 'opfer' => 0);

	$start_id = intval($start_id);
    $lid = $start_id;

	$sql = "SELECT * FROM games_full WHERE id > $lid AND type != 16 ORDER BY id ASC";
	$games = $db1->query_assoc($sql);

	if(!is_array($games)) return;
	

	foreach($games as $game)
	{
		$rnds = $game['rounds'];
		$isTeamPlay = $types->getAditionalContentFromField('team', $game['type']);
		$nameGameType = $types->getLogName($game['type']);
		$gameid = $game['id'];
		
		//$db1->query_db("SELECT * FROM actions_full WHERE roundid IN($rnds) ORDER BY id ASC");
		$arRound = explode(",", $rnds);

		$cRounds = 0;

		foreach($arRound as $rid)
		{
			$round = $db1->query_assoc("SELECT * FROM actions_full WHERE roundid = $rid ORDER BY id ASC");
			$ply = array();
			
			foreach($round as $r) {
				$action = $r['action'];
				$roudnid = $r['id'];

				if($action == 'ShutdownGame')
				{
					$cRounds++;
				}
				else
				{
					
				}
				
				if($action == 'J')
		        {
				    $ply[$r['puid']] = buildPlayerStatsArray($r['puid']);
		        }

                if(!isset($ply[$r['puid']]))
		        {
				    $ply[$r['puid']] = buildPlayerStatsArray($r['puid']);
		        }
		        
		        if($action == "D" || $action == "K")
				{
					$checkIfThatKD = checkIfThatKD($r, $isTeamPlay);
					
					if($action == "K")
					{
					    $ply[$r['puid']]['TD'] = $r['team'];
				        $ply[$r['euid']]['TD'] = $r['eteam'];
		
		                $h = $checkIfThatKD;
		
						switch($checkIfThatKD)
						{
						    case 1 :
							{
		                        $ply[$r['puid']]['D']+=1;
								$ply[$r['puid']]['S']+=1;
		                        $ply[$r['puid']]['cToDeathStreak'] = 0;
		                        $ply[$r['puid']]['cToKillStreak'] = 0;
								break;
							}
							case 2 :
							{
								$ply[$r['euid']]['T']+=1;
								$ply[$r['euid']]['isDeath'] = 0;
		                        $ply[$r['euid']]['cToDeathStreak'] = 0;
		                        $ply[$r['euid']]['cToKillStreak'] = 0;
		                        
								break;
							}
							case 3 :
							{
								$ply[$r['euid']]['K']+=1;
								$ply[$r['euid']]['isDeath'] = 0;
		                        //$ply[$r['euid']]['cToDeathStreak'] = 0;
								$ply[$r['puid']]['D']+=1;
								$ply[$r['puid']]['isDeath'] = 1;
		                        $ply[$r['puid']]['cToKillStreak'] = 0;
		                        $ply[$r['puid']]['cToDeathStreak']+=1;
		
								if(($ply[$r['puid']]['cToDeathStreak'] % 3) == 0)
								{
									$ply[$r['puid']]['sDeathStreak']+=1;
                                    if($ply[$r['euid']]['cToDeathStreak'] <= 3) {
                                        $death_streak_type = $ply[$r['euid']]['cToDeathStreak'] / 3;
                                    } else {
                                        $death_streak_type = $ply[$r['euid']]['cToDeathStreak'];
                                    }

                                    switch($death_streak_type) {
                                        case 1:
                                            $ply[$r['euid']]['deathstreaks']['3er']+=1;
                                            break;
                                        case 4:
                                            $ply[$r['euid']]['deathstreaks']['4er']+=1;
                                            break;
                                        case 5:
                                            $ply[$r['euid']]['deathstreaks']['5er']+=1;
                                            break;
                                        case 6:
                                            $ply[$r['euid']]['deathstreaks']['6er']+=1;
                                            break;
                                    }
								}

								if($ply[$r['puid']]['Bomber'] == 1)
								{
									$ply[$r['puid']]['sKBomber']+=1;
								}
		
								break;
							}
						}
						
		
		                if($ply[$r['euid']]['lST'] == 'prone')
		                {
		                    $ply[$r['euid']]['sSTKp']+=1;
		                }
		                else if($ply[$r['euid']]['lST'] == 'crouch')
		                {
		                    $ply[$r['euid']]['sSTKc']+=1;
		                }
		                else
		                {
		                    if($checkIfThatKD > 1)
		                    	$ply[$r['euid']]['sSTKs']+=1;
		                }
		                
		                if($checkIfThatKD == 2 || $checkIfThatKD == 3)
						{
		
							if($ply[$r['euid']]['isDeath'] == 0)
							{
								$ply[$r['euid']]['cToKillStreak']+=1;
							}
							
							if(($ply[$r['euid']]['cToKillStreak'] % 3) == 0)
							{
								$ply[$r['euid']]['sKillStreak']+=1;
                                $kill_streak_type = $ply[$r['euid']]['cToKillStreak'] / 3;

                                switch($kill_streak_type) {
                                    case 1:
                                        $ply[$r['euid']]['killstreaks']['3er']+=1;
                                        break;
                                    case 2:
                                        $ply[$r['euid']]['killstreaks']['6er']+=1;
                                        break;
                                    case 3:
                                        $ply[$r['euid']]['killstreaks']['9er']+=1;
                                        break;
                                    case 4:
                                        $ply[$r['euid']]['killstreaks']['12er']+=1;
                                        break;
                                }

							}
							else if($ply[$r['euid']]['cToKillStreak'] == 25)
							{
								$ply[$r['euid']]['sKillStreak']+=1;
							}
						}
		            }
		            else if($action == "D")
					{
						switch($checkIfThatKD)
						{
							case 1 :
							{
								$ply[$r['puid']]['DA']+=1;
								$ply[$r['puid']]['DS']+=$r['health'];
								break;
							}
							case 2 :
							{
								$ply[$r['euid']]['DA']+=1;
								$ply[$r['euid']]['DS']+=$r['health'];
								break;
							}
							case 3 :
							{
								$ply[$r['euid']]['DA']+=1;
								$ply[$r['euid']]['DS']+=$r['health'];
								break;
							}
						}
						
						if($checkIfThatKD > 1)
						{
							$ply[$r['euid']]['isDS'] = false;  
						}
					}
				}
				
				if($action == "ST")
				{
					$act = $mods->getLogName($r['mod']);
					$ply[$r['puid']]['lST'] = $act;
					
					if($act == 'stand')
					{
						$ply[$r['puid']]['sSTs']+=1;
					}
					else if($act == 'prone')
					{
						$ply[$r['puid']]['sSTp']+=1;
					}
					else if($act == 'couch')
					{
						$ply[$r['puid']]['sSTc']+=1;
					}
				}
				else if($action == 'BP')
				{
					$ply[$r['puid']]['sBP']+=1;
					$ply[$r['puid']]['Bomber'] = 0;
				}
				else if($action == 'BD')
				{
					$ply[$r['puid']]['sBD']+=1;
				}
				else if($action == 'BL')
				{
					$ply[$r['puid']]['sBL']+=1;
					$ply[$r['puid']]['Bomber'] = 0;
				}
				else if($action == 'BT')
				{
					$ply[$r['puid']]['sBT']+=1;
					$ply[$r['puid']]['Bomber'] = 1;
				}
				else if($action == 'BDA')
				{
					$ply[$r['puid']]['sBDA']+=1;
		
					if($sab_melee['id'] != 0)
					{
						if($sab_melee['opfer'] == $r['puid'])
						{
							$ply[$sab_melee['id']]['sMBDA']+=1;
						}
						$sab_melee['id'] = 0;
					}
		
					if($sab_kill['id'] != 0)
					{
						if($sab_kill['opfer'] == $r['puid'])
						{
							$ply[$sab_kill['id']]['skBDA']+=1;
						}
						$sab_kill['id'] = 0;
					}
		
				}
				else if($action == 'BPA')
				{
					$ply[$r['puid']]['sBPA']+=1;
					$ply[$r['puid']]['Bomber'] = 0;
		
					if($sab_melee['id'] != 0)
					{
						if($sab_melee['opfer'] == $r['puid'])
						{
							$ply[$sab_melee['id']]['sMBPA']+=1;
						}
						$sab_melee['id'] = 0;
					}
		
					if($sab_kill['id'] != 0)
					{
						if($sab_kill['opfer'] == $r['puid'])
						{
							$ply[$sab_kill['id']]['skBPA']+=1;
						}
						$sab_kill['id'] = 0;
					}
				}
				else if($action == 'BPP')
				{
					$ply[$r['puid']]['sBPP']+=1;
				}
				else if($action == 'BDP')
				{
					$ply[$r['puid']]['sBDP']+=1;
				}
				else if($action == 'KC')
				{
					$ply[$r['puid']]['sKC']+=1;
				}
				else if($action == 'KD')
				{
					$ply[$r['puid']]['sKD']+=1;
				}
				else if($action == 'FT')
				{
					$ply[$r['puid']]['sFT']+=1;
				}
				else if($action == 'FC')
				{
					$ply[$r['puid']]['sFC']+=1;
				}
				else if($action == 'FR')
				{
					$ply[$r['puid']]['sFR']+=1;
				}
				else if($action == 'W')
				{
					$ply[$r['puid']]['W']+=1;
				}
				else if($action == "GPS")
				{
					$ply[$r['puid']]['sGPS']+=1;
				}
				else if($action == "KV")
				{
					$ply[$r['puid']]['sKV']+=1;
				}
				else if($action == "RC")
				{
					$ply[$r['puid']]['sRC']+=1;
				}
				else if($action == "RD")
				{
					$ply[$r['puid']]['sRD']+=1;
				}

			//	$db1->query_db($sql);
			}
			
			$c = count($ply);
			$keys = array_keys($ply);

			$sql_rounds = "INSERT INTO stats_rounds_players VALUES ";
			$sql_tactics = "INSERT INTO stats_tatics_players VALUES ";
			$sql_amounts = "INSERT INTO stats_amounts_players VALUES ";

			for($i=0;$i<$c;$i++)
			{
				
				$playerid = $keys[$i];
				$pkt = 0;
				$kills = 	$ply[$keys[$i]]['K'];
				$deaths = 	$ply[$keys[$i]]['D'];
				$kdr = 		$kills / (($deaths == 0)?1:$deaths);
				$tk = 		$ply[$keys[$i]]['T'];
				$su = 		$ply[$keys[$i]]['S'];
				$sDi = 		$ply[$keys[$i]]['DS'];
				$w = 		$ply[$keys[$i]]['W'];
				$sKs = 		$ply[$keys[$i]]['sSTKs'];
				$sKc = 		$ply[$keys[$i]]['sSTKc'];
				$sKp = 		$ply[$keys[$i]]['sSTKp'];

				//oki Punkte ermitteln
				if($nameGameType == 'war' || $nameGameType == 'tdm' || $nameGameType == 'dm')
				{
					if($isTeamPlay == 1)
					{
						$pkt = computePoints_TDM($kills, $deaths, $w);
					}
					else
					{
						$pkt = computePoints_DM($kills, $deaths);
					}
				}
				else if($nameGameType == 'sab')
				{
					//$pkt = computePoints_sab2($ply[$keys[$i]]['K'], $ply[$keys[$i]]['D'], $ply[$keys[$i]]['sBD'], $ply[$keys[$i]]['sBP'], $ply[$keys[$i]]['sBDP'], $ply[$keys[$i]]['sBPP'], $ply[$keys[$i]]['skBDA'], $ply[$keys[$i]]['skBPA'], $ply[$keys[$i]]['sMBPA'], $ply[$keys[$i]]['sMBDA'], $ply[$keys[$i]]['sKBomber'], $ply[$keys[$i]], $w);

					$pkt = ($ply[$keys[$i]]['sBP'] + $ply[$keys[$i]]['sBD']) * 25;
					$pkt+= ($ply[$keys[$i]]['sBPP'] + $ply[$keys[$i]]['sBDP']) * 10;
					$pkt+= $ply[$r['puid']]['sKBomber'] * 7;
					
					
				}
				else if($nameGameType == 'kc')
				{
					//$pkt = computePoints_kc($kills, $deaths, $ply[$keys[$i]]['sKC'], $ply[$keys[$i]]['sKD'], $w);
					$pkt = ($ply[$keys[$i]]['sKC'] + $ply[$keys[$i]]['sKD']) * 5;
				}
				else
				{
					//$pkt = computePoints_DM($kills, $deaths);
				}
				
				$pkt+= $kills * 5;
				$pkt+= ($tk + $su) * -5;
				$pkt+= $w * 50;
                $sKS3 = $ply[$keys[$i]]['killstreaks']['3er'];
                $sKS6 = $ply[$keys[$i]]['killstreaks']['6er'];
                $sKS9 = $ply[$keys[$i]]['killstreaks']['9er'];
                $sKS12 = $ply[$keys[$i]]['killstreaks']['12er'];
                $sDS3 = $ply[$keys[$i]]['deathstreaks']['3er'];
                $sDS4 = $ply[$keys[$i]]['deathstreaks']['4er'];
                $sDS5 = $ply[$keys[$i]]['deathstreaks']['5er'];
                $sDS6 = $ply[$keys[$i]]['deathstreaks']['6er'];
                $distance = processWalkingDistanceTotal($ply[$keys[$i]]['id'], $rid, true);
				//$sql = "INSERT INTO stats_rounds_players VALUES ('', '$gameid', '$rid', '$playerid', '$pkt', '$kills', '$deaths', '$kdr', '$tk', '$su', '$sDi', '0', '$w', '$sKs', '$sKc', '$sKp')";
				$sql_rounds.= "('', '$gameid', '$rid', '$playerid', '$pkt', '$kills', '$deaths', '$kdr', '$tk', '$su', '$sDi', '0', '$w', '$sKs', '$sKc', '$sKp', '$sKS3', '$sKS6', '$sKS9', '$sKS12', '$sDS3', '$sDS4', '$sDS5', '$sDS6', '$distance'),";

				$sBP = $ply[$keys[$i]]['sBP'];
				$sBD = $ply[$keys[$i]]['sBD'];
				$sBT = $ply[$keys[$i]]['sBT'];
				$sBL = $ply[$keys[$i]]['sBL'];
				$sBPP = $ply[$keys[$i]]['sBPP'];
				$sBPA = $ply[$keys[$i]]['sBPA'];
				$sBDP = $ply[$keys[$i]]['sBDP'];
				$sBDA = $ply[$keys[$i]]['sBDA'];
				
				$sFT = $ply[$keys[$i]]['sFT'];
				$sFC = $ply[$keys[$i]]['sFC'];
				$sFR = $ply[$keys[$i]]['sFR'];
				
				$sKC = $ply[$keys[$i]]['sKC'];
				$sKD = $ply[$keys[$i]]['sKD'];

				$sRC = $ply[$keys[$i]]['sRC'];
				$sRD = $ply[$keys[$i]]['sRD'];
				
				$sGPS = $ply[$keys[$i]]['sGPS'];
				$sKV = $ply[$keys[$i]]['sKV'];
				
				//$sql2 = "INSERT INTO stats_tatics_players VALUES ('', '$gameid', '$rid', '$playerid', '$sBP', '$sBD', '$sBT', '$sBL', '$sBPP', '$sBPA', '$sBDP', '$sBDA', '$sFT', '$sFC', '$sFR', '$sKC', '$sKD', '$sRC', '$sRD', '$sGPS', '$sKV')";
				$sql_tactics.= "('', '$gameid', '$rid', '$playerid', '$sBP', '$sBD', '$sBT', '$sBL', '$sBPP', '$sBPA', '$sBDP', '$sBDA', '$sFT', '$sFC', '$sFR', '$sKC', '$sKD', '$sRC', '$sRD', '$sGPS', '$sKV'),";
				
				$sKiS = $ply[$keys[$i]]['sKillStreak'];
				$sDeS = $ply[$keys[$i]]['sDeathStreak'];
				
				
				//$sql3 = "INSERT INTO stats_amounts_players VALUES ('', '$gameid', '$rid', '$playerid', '$sKiS', '$sDeS')";

                /*
                wird nun direkt in EXT mb_ultraplayer abgearbertet

				$sql_amounts.= "('', '$gameid', '$rid', '$playerid', '$sKiS', '$sDeS'),";
				
				$lvl = 0;

				if($kdr < 1)
				{
					$lvl = 0;
				}
				else if($kdr >= 1 && $kdr < 2)
				{
					$lvl = 1;
				}
				else
				{
					$lvl = 2;
				}

				$sql = "UPDATE aliases SET league = '$kdr' WHERE id = '$playerid'";
				$db1->query_db($sql);

                */
			}

			//remove last komma
			$sql_rounds = substr($sql_rounds, 0, strlen($sql_rounds)-1);
			$sql_tactics = substr($sql_tactics, 0, strlen($sql_tactics)-1);
			$sql_amounts = substr($sql_amounts, 0, strlen($sql_amounts)-1);

			$db1->query_db($sql_rounds);
			$db1->query_db($sql_tactics);
			$db1->query_db($sql_amounts);
		}
	}	
}



function processMasterShooterAll($id, &$db)
{
	$sql = "SELECT COUNT(action) summe, weapons.name, weapons.weapon_grouped_id AS wid FROM actions_full, weapons WHERE actions_full.action = 'K' AND actions_full.euid = '$id' AND weapons.id = actions_full.weapon GROUP BY weapons.weapon_grouped_id ORDER BY summe DESC";
	
	$res = $db->query_assoc($sql);
	$arr = array();
	$i = 0;
	
	if(!is_array($res)) return null;
	
	$c25 = array(7 => '', 14 => '', 9 => '', 25 => '', 51 => '', 24 => '', 11 => '');

	foreach($res as $r)
	{
		$s = $r['summe'];
		
		$arr[$i] = $r;

		if(!array_key_exists($r['wid'], $c25))
		{
        	$stufe = $s / 50;
		}
		else
		{
			$stufe = $s / 25;
		}
	
		
        $arr[$i]['mult'] = $stufe;
		$arr[$i]['rang'] = floor($stufe);

		$i++;
	}

	return $arr;
}

function processMasterSlaughter($id, &$db)
{
	$sql = "SELECT COUNT(`action`) AS summe FROM `actions_full` WHERE `actions_full`.mod = 7 AND euid = $id";
	
	$res = $db->query_assoc($sql);
	
	$r[0] = array();
	
	if($res)
	{
		$s = $res[0]['summe'];
		$st = $s / 30;
		
		$r[0]['summe'] = $res[0]['summe'];
		$r[0]['nane'] = 'Knife';
		$r[0]['mult'] = $st;
		$r[0]['rang'] = floor($st);
		$r[0]['wid'] = 9999;
	}
	else
	{
		$r[0]['summe'] = 0;
		$r[0]['nane'] = 'Knife';
		$r[0]['mult'] = 0;
		$r[0]['rang'] = 0;
		$r[0]['wid'] = 9999;
	}
	
	return $r;
}

function getAllWeaponsOrdered()
{
	$arr[1] = array(4, 3, 19, 48, 54, 47, 53);
	$arr[2] = array(6, 32);						//Pump
	$arr[3] = array(37, 41, 49, 58, 59);		// mp7, ump45
	$arr[4] = array(60, 61, 62, 63);			//Sniper
	$arr[5] = array(46, 27, 31);				//MG
	$arr[6] = array(23, 21, 5, 35);				//pistol
	$arr[7] = array(7, 14, 9, 25);				//grenades
	$arr[8] = array(51, 24, 11, 9999);			//extra (messer ist extra 'weapon_knife.png')
	
	return $arr;
}


function processPlayersChallenges()
{
	$db1 = new mbdb();
	$db2 = new mbdb();
	
	$players = $db1->query_assoc("SELECT * FROM aliases ORDER BY id ASC");
	
	foreach($players as $p)
	{
		$id = $p['id'];
		
		$arr = processMasterShooterAll($id, $db2);
		
		if(is_array($arr))
		{
			$rs = 0;
			
			foreach($arr as $r)
			{
				$rs+= $r['rang'];
			}

			$sql = "UPDATE aliases SET rang = '$rs' WHERE id = '$id'";
			$db1->query_db($sql);
		}
	}
}


//processPlayersPerRound(0);
?>