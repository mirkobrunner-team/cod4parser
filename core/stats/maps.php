<?php


function buildMapSelector($map_id)
{
	$db = new mbdb();
	$db->query_db("SELECT * FROM maps ORDER BY name_log ASC");
	
	$select = '<select name="maps" id="map_id" onchange="this.form.submit()">';
	$select.= '<option value="-1" >-</option>';

	while($arr = mysqli_fetch_array($db->result))
	{
		$sel = ($map_id == $arr['id']) ? 'selected' : '';
		$select.= '<option value="'.$arr['id'].'" '.$sel.'>'.$arr['name'].'</option>';
	}
	
	$select.= '</select>';

	return $select;
}

function getTotalMapCount()
{
	$db = new mbdb();
	
	$sql = "SELECT COUNT(id) FROM maps";
	
	return $db->query_assoc($sql);
}

function getMapsCountList()
{
	$db = new mbdb();
	
	$sql = "SELECT COUNT(map) as map_sum, map, maps.name FROM games_full, maps WHERE maps.id = games_full.map GROUP BY map ORDER BY map_sum DESC";
	
	return $db->query_assoc($sql);
}

function getMapGameCount($map_id)
{
	$db = new mbdb();
	
	$sql = "SELECT COUNT(map) as map_sum, maps.name FROM games_full, maps WHERE games_full.map = '$map_id' AND maps.id = '$map_id'";

	return $db->query_assoc($sql);
}

function getGameTypeMapsCount($map_id)
{
	$db = new mbdb();
	
	$sql = "SELECT COUNT(type) as type_sum, map, gametypes.name_log FROM games_full, gametypes WHERE gametypes.id = games_full.type AND games_full.map = '$map_id' GROUP BY type ORDER BY type_sum DESC";
	
	return $db->query_assoc($sql);
}

function getActionCountPerMap($map_id)
{
	$db = new mbdb();
	$db1 = new mbdb();
	
	$sql = "SELECT duration, id FROM rounds_full WHERE map = '$map_id'";
	
	$db->query_db($sql);
	
	$duration = 0;
	$actions = 0;
	
	while($r = mysqli_fetch_array($db->result))
	{
		$duration+= $r['duration'];
		$id = $r['id'];
		
		$arr = $db1->query_assoc("SELECT COUNT(action) AS cactions FROM actions_full WHERE roundid = '$id'");
		$actions+= $arr[0]['cactions'];	
	}
	
	return "Alle ".round(($duration / $actions), 4)." Sekunden eine Aktion";
}

function getBestPlayersByPointsForMap($mapid, $max = 10) {
    $db = new mbdb();

    $sql = "SELECT SUM(stats_rounds_players.points) AS sum_points, stats_rounds_players.playerid, aliases.hash AS player_name FROM stats_rounds_players, games_full, aliases WHERE games_full.map = $mapid AND stats_rounds_players.gameid = games_full.id AND aliases.id = stats_rounds_players.playerid GROUP BY stats_rounds_players.playerid ORDER BY sum_points DESC LIMIT $max";

    return $db->query_assoc($sql);
}

function getBestPlayersByKillsForMap($mapid, $max = 10) {
    $db = new mbdb();

    $sql = "SELECT SUM(stats_rounds_players.kills) AS sum_kills, stats_rounds_players.playerid, aliases.hash AS player_name FROM stats_rounds_players, games_full, aliases WHERE games_full.map = $mapid AND stats_rounds_players.gameid = games_full.id AND aliases.id = stats_rounds_players.playerid GROUP BY stats_rounds_players.playerid ORDER BY sum_kills DESC LIMIT $max";

    return $db->query_assoc($sql);
}

function getBestPlayersByDeathsForMap($mapid, $max = 10) {
    $db = new mbdb();

    $sql = "SELECT SUM(stats_rounds_players.deaths) AS sum_deaths, stats_rounds_players.playerid, aliases.hash AS player_name FROM stats_rounds_players, games_full, aliases WHERE games_full.map = $mapid AND stats_rounds_players.gameid = games_full.id AND aliases.id = stats_rounds_players.playerid GROUP BY stats_rounds_players.playerid ORDER BY sum_deaths DESC LIMIT $max";

    return $db->query_assoc($sql);
}

function getBestPlayersByKdrForMap($mapid, $max = 10) {
    $db = new mbdb();

    $sql = "SELECT MAX(stats_rounds_players.kdr) AS max_kdr, stats_rounds_players.playerid, aliases.hash AS player_name FROM stats_rounds_players, games_full, aliases WHERE games_full.map = $mapid AND stats_rounds_players.gameid = games_full.id AND aliases.id = stats_rounds_players.playerid GROUP BY stats_rounds_players.playerid ORDER BY max_kdr DESC LIMIT $max";

    return $db->query_assoc($sql);
}

function getBestPlayersByTeamkillsForMap($mapid, $max = 10) {
    $db = new mbdb();

    $sql = "SELECT SUM(stats_rounds_players.teamkills) AS sum_teamkills, stats_rounds_players.playerid, aliases.hash AS player_name FROM stats_rounds_players, games_full, aliases WHERE games_full.map = $mapid AND stats_rounds_players.gameid = games_full.id AND aliases.id = stats_rounds_players.playerid GROUP BY stats_rounds_players.playerid ORDER BY sum_teamkills DESC LIMIT $max";

    return $db->query_assoc($sql);
}

function getBestPlayersBySuizideForMap($mapid, $max = 10) {
    $db = new mbdb();

    $sql = "SELECT SUM(stats_rounds_players.suizide) AS sum_suizides, stats_rounds_players.playerid, aliases.hash AS player_name FROM stats_rounds_players, games_full, aliases WHERE games_full.map = $mapid AND stats_rounds_players.gameid = games_full.id AND aliases.id = stats_rounds_players.playerid GROUP BY stats_rounds_players.playerid ORDER BY sum_suizides DESC LIMIT $max";

    return $db->query_assoc($sql);
}

function getBestPlayersByDistanceForMap($mapid, $max = 10) {
    $db = new mbdb();

    $sql = "SELECT SUM(stats_rounds_players.distance) AS sum_distance, stats_rounds_players.playerid, aliases.hash AS player_name FROM stats_rounds_players, games_full, aliases WHERE games_full.map = $mapid AND stats_rounds_players.gameid = games_full.id AND aliases.id = stats_rounds_players.playerid GROUP BY stats_rounds_players.playerid ORDER BY sum_distance DESC LIMIT $max";

    return $db->query_assoc($sql);
}

function getSumDistanceForMap($mapid) {
	$db = new mbdb();
	
	$sql = "SELECT SUM(distance) AS sum_distance FROM stats_rounds_players, games_full WHERE games_full.map = '$mapid' AND stats_rounds_players.gameid = games_full.id";
	
	return $db->query_assoc($sql);
}

function getSumPointsForMap($mapid) {
	$db = new mbdb();
	
	$sql = "SELECT SUM(points) AS sum_points FROM stats_rounds_players, games_full WHERE games_full.map = '$mapid' AND stats_rounds_players.gameid = games_full.id";
	
	return $db->query_assoc($sql);
}

function getSumKillsForMap($mapid) {
	$db = new mbdb();
	
	$sql = "SELECT SUM(kills) AS sum_kills FROM stats_rounds_players, games_full WHERE games_full.map = '$mapid' AND stats_rounds_players.gameid = games_full.id";
	
	return $db->query_assoc($sql);
}

function getSumDeathsForMap($mapid) {
	$db = new mbdb();
	
	$sql = "SELECT SUM(deaths) AS sum_deaths FROM stats_rounds_players, games_full WHERE games_full.map = '$mapid' AND stats_rounds_players.gameid = games_full.id";
	
	return $db->query_assoc($sql);
}

function getSumTeamkillsForMap($mapid) {
	$db = new mbdb();

	$sql = "SELECT SUM(teamkills) AS sum_teamkills FROM stats_rounds_players, games_full WHERE games_full.map = '$mapid' AND stats_rounds_players.gameid = games_full.id";
	
	return $db->query_assoc($sql);
}

function getSumSuizidesForMap($mapid) {
	$db = new mbdb();
	
	$sql = "SELECT SUM(suizide) AS sum_suizides FROM stats_rounds_players, games_full WHERE games_full.map = '$mapid' AND stats_rounds_players.gameid = games_full.id";
	
	return $db->query_assoc($sql);
}

function getAverageKdrForMap($mapid) {
	$db = new mbdb();
	
	$sql = "SELECT AVG(kdr) AS avg_kdr FROM stats_rounds_players, games_full WHERE games_full.map = '$mapid' AND stats_rounds_players.gameid = games_full.id";
	
	return $db->query_assoc($sql);
}

function getPlayTimeForMap($mapid) {
	$db = new mbdb();

	$sql = "SELECT SUM(duration) AS duration FROM games_full WHERE map = '$mapid'";

	return $db->query_assoc($sql);
}

function getOverallSumForMap($mapid) {
	$db = new mbdb();
	
	$sql = "SELECT SUM(points) AS sum_points, SUM(kills) AS sum_kills, SUM(deaths) AS sum_deaths, AVG(kdr) AS average_kdr, SUM(teamkills) AS sum_teamkills, SUM(suizide) AS sum_suizide, SUM(distance) AS sum_distance, SUM(games_full.duration) AS sum_duration, AVG(games_full.duration) AS avg_duration FROM stats_rounds_players, games_full WHERE games_full.map = '$mapid' AND stats_rounds_players.gameid = games_full.id";
	
	return $db->query_assoc($sql);	
}

function getGamesForMap($mapid) {
	$db = new mbdb();

	$sql = "SELECT games_full.id AS id, games_full.time AS time, games_full.rounds AS rounds, games_full.type AS typeid, games_full.duration AS duration, gametypes.name FROM games_full, gametypes WHERE map = '$mapid' AND gametypes.id = games_full.type ORDER BY games_full.time DESC";

	return $db->query_assoc($sql);
}

function getAverageKillsPerMap($mapid) {
	$db = new mbdb();

	$sql = "SELECT AVG(kills) AS avg_kills FROM stats_rounds_players, games_full WHERE games_full.map = '$mapid' AND stats_rounds_players.gameid = games_full.id";

	return $db->query_assoc($sql);
}

?>