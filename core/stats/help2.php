<?php

function all(){

    $f['api usage'] = array(
        'params' => array('params must be separated by ; in the same order as defined', 'all params must be set')
    );

    $f['games'] = array(
        'buildGameSelector' => array(
            'name' => 'buildgameselector',
            'params' => array('playerid', 'nosubmit'),
        ),
        'buildGamesList' => array(
            'name' => 'buildgameslist'
        ),
        'buildGamesTable' => array(
            'name' => 'buildgamestable'
        ),
        'countGames' => array(
            'name' => 'countgames'
        ),
        'countRounds' => array(
            'name' => 'countrounds'
        ),
        'countActions' => array(
            'name' => 'countactions'
        ),
        'totalPlayTime' => array(
            'name' => 'totalplaytime'
        ),
        'totalKillSum' => array(
            'name' => 'totalkillsum'
        ),
        'totalHitSum' => array(
            'name' => 'totalhitssum'
        ),
        'countMaps' => array(
            'name' => 'countmaps'
        ),
        'countGameTypes' => array(
            'name' => 'countgametypes'
        ),
        'countPlayers' => array(
            'name' => 'countplayers'
        ),
        'countSpawnPoints' => array(
            'warning' => 'Marked as deprecated but can be come back in the future!',
            'name' => 'countspawnpoints'
        ),
        'getMostPlayedMap' => array(
            'name' => 'getmostplayedmap'
        ),
        'getMostPlayedGameType' => array(
            'name' => 'getmostplayedgametype'
        ),
        'getLongestPlayedGame' => array(
            'name' => 'getlongestplayedgame'
        ),
        'getmMstUsedWeapon' => array(
            'name' => 'getmostusedweapon'
        ),
        'getRealDurationForRound' => array(
            'name' => 'getrealdurationforround',
            'params' => array('roundid')
        ),
        'countTeamKills' => array(
            'name' => 'countteamkills'
        ),
        'countSuizides' => array(
            'name' => 'countsuizides'
        ),
        'getPlayersCountByTimeInterval' => array(
            'name' => 'getplayerscountbytimeinterval',
            'params' => array(
                0 => array(
                    'name' => 'interval',
                    'type' => 'char',
                    'brief' => 'supported intervals: h = hour, d = day, w = week, m = month'
                )
            )
        ),
        'getNewPlayersCountByTimeInterval' => array(
            'name' => 'getNewplayerscountbytimeinterval',
            'params' => array(
                0 => array(
                    'name' => 'interval',
                    'type' => 'char',
                    'brief' => 'supported intervals: h = hour, d = day, w = week, m = month'
                )
            )
        ),
        'getGamesCountByTimeInterval' => array(
            'name' => 'getgamescountbytimeinterval',
            'params' => array(
                0 => array(
                    'name' => 'interval',
                    'type' => 'char',
                    'brief' => 'supported intervals: h = hour, d = day, w = week, m = month'
                )
            )
        ),
        'getAllPlayerStatsByTimeInterval' => array(
            'name' => 'getallplayerStatsbytimeinterval',
            'params' => array(
                0 => array(
                    'name' => 'interval',
                    'type' => 'char',
                    'brief' => 'supported intervals: h = hour, d = day, w = week, m = month'
                )
            )
        ),
        'getGameActionsByTimeInterval' => array(
            'name' => 'getgameactionsbytimeinterval',
            'params' => array(
                0 => array(
                    'name' => 'interval',
                    'type' => 'char',
                    'brief' => 'supported intervals: h = hour, d = day, w = week, m = month'
                )
            )
        ),
        'getActionCountlist' => array(
            'name' => 'getactioncountlist'
        ),
        'getDistanceSumForGame' => array(
            'name' => 'getdistancesumforgame',
            'params' => array('gameid')
        ),
        'getDistanceSumForPlayersInGame' => array(
            'name' => 'getdistancesumforplayersingame',
            'params' => array('gameid')
        ),
        'getdistancesumforplayeringame' => array(
            'name' => 'getdistancesumforplayersingame',
            'params' => array('gameid', 'playerid')
        )
    );

    $f['maps'] = array(
        'buildMapSelector' => array(
            'name' => 'buildmapselector',
            'params' => array(
                'mapid'
            )
        ),
        'getTotalMapCount' => array(
            'name' => 'gettotalmapcount'
        ),
        'getMapsCountList' => array(
            'name' => 'getmapscountlist'
        ),
        'getMapGameCount' => array(
            'name' => 'getmapgamecount',
            'params' => array('mapid')
        ),
        'getGameTypeMapsCount' => array(
            'name' => 'getgametypemapscount',
            'parans' => array('mapid')
        ),
        'getActionCountPerMap' => array(
            'name' => 'getactioncountpermap',
            'params' => array('mapid')
        ),
        'getBestPlayersByPointsForMap' => array(
            'name' => 'getbestplayersbypointsformap',
            'params' => array('mapid', 'max')
        ),
        'getbestplayersbykillsformap' => array(
            'name' => 'getbestplayersbykillsformap',
            'params' => array('mapid', 'max')
        ),
        'getBestPlayersByDeathsForMap' => array(
            'name' => 'getbestplayersbydeathsformap',
            'params' => array('mapid', 'max')
        ),
        'getBestPlayersByKdrForMap' => array(
            'name' => 'getbestplayersbykdrformap',
            'params' => array('mapid', 'max')
        ),
        'getBestPlayersByTeamKillsForMap' => array(
            'name' => 'getbestplayersbyteamkillsformap',
            'params' => array('mapid', 'max')
        ),
        'getBestPlayersBySuizideForMap' => array(
            'name' => 'getbestplayersbysuizideformap',
            'params' => array('mapid', 'max')
        ),
        'getBestPlayersByDistanceForMap' => array(
            'name' => 'getbestplayersbydistanceformap',
            'params' => array('mapid', 'max')
        ),
        'getSumDistanceForMap' => array(
            'name' => 'getsumdistanceformap',
            'params' => array('mapid')
        ),
        'getSumPointsForMap' => array(
            'name' => 'getsumpointsformap',
            'params' => array('mapid')
        ),
        'getSumKillsForMap' => array(
            'name' => 'getsumkillsformap',
            'params' => array('mapid')
        ),
        'getSumDeathsForMap' => array(
            'name' => 'getsumdeathsformap',
            'params' => array('mapid')
        ),
        'getSumTeamKillsForMap' => array(
            'name' => 'getsumteamkillsformap',
            'params' => array('mapid')
        ),
        'getSumSuizidesForMap' => array(
            'name' => 'getsumsuizidesformap',
            'params' => array('mapid')
        ),
        'getAverageKdrForMap' => array(
            'name' => 'getaveragekdrformap',
            'params' => array('mapid')
        ),
        'getPlayTimeForMap' => array(
            'name' => 'getplaytimeformap',
            'params' => array('mapid')
        ),
        'getOverallSumForMap' => array(
            'name' => 'getoverallsumformap',
            'params' => array('mapid')
        ),
        'getGamesForMap' => array(
            'name' => 'getgamesformap',
            'params' => array('mapid')
        ),
        'getaveragekillspermap' => array(
            'name' => 'getaveragekillspermap',
            'params' => array('mapid')
        )
    );

    $f['players'] = array(
        'buildPlayerSelector' => array(
            'name' => 'buildPlayerselector',
            'params' => array('playerid')
        ),
        'getBasicPlayerKillStats' => array(
            'name' => 'getbasicplayerkillstats',
            'params' => array('playerid')
        ),
        'getBasicPlayerDeathStats' => array(
            'name' => 'getbasicplayerdeathstats',
            'params' => array('playerid')
        ),
        'getWeaponKillCount' => array(
            'name' => 'getweaponkillcount',
            'params' => array('playerid')
        ),
        'getWeaponDeathsCount' => array(
            'name' => 'getweapondeathscount',
            'params' => array('playerid')
        ),
        'getWeaponKillCountByType' => array(
            'name' => 'getWeaponkillcountbytype',
            'params' => array('playerid')
        ),
        'getWeaponDeathsCountByType' => array(
            'name' => 'getweapondeathscountbytype',
            'params' => array('playerid')
        ),
        'countKillsPlayerList' => array(
            'name' => 'countkillsplayerlist',
            'params' => array('playerid')
        ),
        'countteamkillsplayerlist' => array('playerid'),
        'countdeathsbyenemylist' => array('playerid'),
        'countteamdeathsbyenemylist' => array('playerid'),
        'countaction' => array('playerid'),
        'getplayermapscountlist' => array('playerid'),
        'getplayergametypecountlist' => array('playerid'),
        'getplayerhitlocationsbykillstotal' => array('playerid'),
        'getplayerhitlocationsbydeathstotal' => array('playerid'),
        'getplayerhitlocationsinflictedtotal' => array('playerid'),
        'getplayerhitlocationsrecievedtotal' => array('playerid'),
        'sumdamageinflictedtotalbyhitlocation' => array('playerid'),
        'sumdamagerecievedtotalhitlocation' => array('playerid'),
        'sumdamageinflictedtotal' => array('playerid'),
        'sumdamagerecievedtotal' => array('playerid'),
        'getplayersactioncountlist' => array('playerid'),
        'getkillsbyplayerlist' => array('playerid'),
        'getdeathsbyplayerlist' => array('playerid'),
        'getplayerscountbytimeinterval' => array('playerid'),
        'getplayerpoints' => array('playerid'),
        'getplayerheadshoots' => array('playerid'),
        'getplayerstatsroundsrow' => array('playerid'),
        'getplayerstotalpointslisted' => array(),
        'getplayerstotalkillslisted' => array(),
        'getplayerstotaldeathslisted' => array(),
        'getplayerstotalteamkillslisted' => array(),
        'getplayerspointsgameline' => array('playerid'),
        'getplayerskillsfullgameline' => array('playerid'),
        'getplayershighscoresfortables' => array('playerid')
    );

    $f['gametypes'] = array(
        'buildgametypeselector' => array('typeid'),
        'gettotalgametypecount' => array(),
        'getgametypecountlist' => array(),
        'getmapgametypecount' => array('typeid'),
        'getactionratiopergametype' => array('typeid')
    );

    $f['weapons'] = array(
        'buildweaponselector' => array('typeid'),
        'buildweapontypeselector' => array('typeid'),
        'getcountedlisthitsperweapon' => array(),
        'getcountedlistkillsperweapon' => array(),
        'getcountedlistdamageperweapon' => array(),
        'getcountedlisthitsperweaponbytype' => array(),
        'getcountedlistkillsperweaponbytype' => array(),
        'getcountedlistdamageperweaponbytype' => array(),
        'getkilldamagerationperweaponbytype' => array()
    );

    $f['challenges'] = array(
        'processmastershooter' => array('playerid', 'weaponType'),
        'processmasterheadshooter' => array('playerid', 'weaponType'),
        'processmasterstandingshooter' => array('playerid'),
        'processwocountbygametype' => array('playerid', 'gametype'),
        'processextraweaponcount' => array('playerid', 'weaponId', 'rang'),
        'processmultigranatecount' => array('playerid', 'modId', 'rang'),
        'processmultiweaponcount' => array('playerid', 'weaponId', 'rang'),
        'processmultiairstrikecount' => array('playerid', 'weaponId', 'rang'),
        'processextramodcount' => array('playerid', 'modid'),
        'processsilencerkills' => array('playerid', 'rang'),
        'processgamewinner' => array('playerid', 'gametype'),
        'processteamgamewinner' => array('playerid', 'rang'),
        'processwalkingdistancetotal' => array('playerid'),
        'processwalkingdistancewithbomb_sab' => array('playerid', 'roundid')
    );

    $f['medals'] = array(
        'getweaponsmedalsforplayer' => array('playerid')
    );

    return $f;
}

?>