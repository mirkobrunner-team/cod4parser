<?php

function buildHTMLBody($arr, $playerid = 0)
{
	$c = count($arr);
	$s = 0;


	for($i=0;$i<$c;$i++)
	{
		if($arr[$i]['name'] != 'none')
		{
			$s+= $arr[$i]['dam_sum'];
		}
	}

	$keys = array('head', 'neck', 'Left Arm Upper', 'Left Arm Lower', 'Left Hand', 'Right Hand', 'Torso Upper', 'Right Arm Upper', 'Right Arm Lower', 'Torso Lower', 'Left Leg Upper', 'Right Leg Upper', 'Left Leg Lower', 'Right Leg Lower', 'Left Food', 'Right Food');

	$p = 100 / $s;
	$colors = array();
    $percent = array();

	for($i=0;$i<$c;$i++)
	{
		if($arr[$i]['name'] != 'none')
		{
		    $key = ($arr[$i]['name']);
		    
		    if($arr[$i]['dam_sum'] != 0)
		    {
				$t =($arr[$i]['dam_sum'] / 100) * $p;
			}
			else
			{
				$t = 0;
			}
			
			//$t+=($arr[$i]['dam_sum'] / 100) * $p;
			//$colors[trim($arr[$i]['name'])] = 1-$t;

			$colors[$key] = $t;
            $values[$key] = $arr[$i]['dam_sum'];
            $percent[$key] = round($t*100, 4);
		}
	}
	
	foreach($keys as $k)
	{
		if(!array_key_exists($k, $colors))
		{
			$colors[$k] = 0;
            $values[$k] = 0;
            $percent[$k] = 0;
		}
	}
	
	
	
	$content.= '<div class="player_body">';
	
	$content.= '<div class="head"></div>';
	
	$content.= '<div class="left_neck"></div>';
	$content.= '<div class="neck"></div>';
	$content.= '<div class="right_neck"></div>';
	$content.= '<div class="clearfix"></div>';
	
	$content.= '<div class="left_neck_down"></div>';
	$content.= '<div class="arm_left_lower"></div>';
	$content.= '<div class="arm_left_upper"></div>';
	$content.= '<div class="left_arm_down"></div>';
	$content.= '<div class="left_hand"></div>';
	
	$content.= '<div class="torso_upper"></div>';
	
	$content.= '<div class="right_arm_upper"></div>';
	$content.= '<div class="right_arm_lower"></div>';
	$content.= '<div class="right_neck_down"></div>';
	$content.= '<div class="right_arm_down"></div>';
	$content.= '<div class="right_hand"></div>';

	$content.= '<div class="torso_lower"></div>';
	
	$content.= '<div class="left_leg_upper"></div>';
	$content.= '<div class="right_leg_upper"></div>	';					

	$content.= '<div class="left_leg_lower"></div>';
	$content.= '<div class="right_leg_lower"></div>';

	$content.= '<div class="left_foot"></div>';
	$content.= '<div class="right_foot"></div>';


	


	$content.= '<div class="bodylayer ohead" style="opacity:'.$colors['head'].';" title="'.$values['head'].' EP '.$percent['head'].' %" '.buildDataAttributes($values, $percent, 'head').'></div>';
	$content.= '<div class="bodylayer oneck" style="opacity:'.$colors['neck'].';" title="'.$values['neck'].' EP" '.buildDataAttributes($values, $percent, 'neck').'></div>';

	$content.= '<div class="bodylayer oarm_left_lower" style="opacity:'.$colors["Left Arm Upper"].';" title="'.$values['Left Arm Upper'].' EP" '.buildDataAttributes($values, $percent, 'Left Arm Upper').'></div>';
	$content.= '<div class="bodylayer oarm_left_upper" style="opacity:'.$colors["Left Arm Lower"].'"  title="'.$values['Left Arm Lower'].' EP" '.buildDataAttributes($values, $percent, 'Left Arm Lower').'></div>';
	$content.= '<div class="bodylayer oleft_hand" style="opacity:'.$colors["Left Hand"].';"  title="'.$values['Left Hand'].' EP" '.buildDataAttributes($values, $percent, 'Left Hand').'></div>';
	
	$content.= '<div class="bodylayer otorso_upper" style="opacity:'.$colors["Torso Upper"].';" title="'.$values['Torso Upper'].' EP" '.buildDataAttributes($values, $percent, "Torso Upper").'></div>';
	
	$content.= '<div class="bodylayer oright_arm_upper" style="opacity:'.$colors["Right Arm Upper"].';" title="'.$values['Right Arm Upper'].' EP" '.buildDataAttributes($values, $percent, 'Right Arm Upper').'></div>';
	$content.= '<div class="bodylayer oright_arm_lower" style="opacity:'.$colors["Right Arm Lower"].';" title="'.$values['Right Arm Lower'].' EP" '.buildDataAttributes($values, $percent, 'Right Arm Lower').'></div>';
	$content.= '<div class="bodylayer oright_hand" style="opacity:'.$colors["Right Hand"].';" title="'.$values['Right Hand'].' EP" '.buildDataAttributes($values, $percent, 'Right Hand').'></div>';
						
	$content.= '<div class="bodylayer otorso_lower" style="opacity:'.$colors["Torso Lower"].';" title="'.$values['Torso Lower'].' EP '.$percent['Torso Lower'].' %" '.buildDataAttributes($values, $percent, 'Torso Lower').'></div>';
	
	$content.= '<div class="bodylayer oleft_leg_upper" style="opacity:'.$colors["Left Leg Upper"].';" title="'.$values['Left Leg Upper'].' EP" '.buildDataAttributes($values, $percent, 'Left Leg Upper').'></div>';
	$content.= '<div class="bodylayer oright_leg_upper" style="opacity:'.$colors["Right Leg Upper"].';" title="'.$values['Right Leg Upper'].' EP" '.buildDataAttributes($values, $percent, 'Right Leg Upper').'></div>	';
	
	$content.= '<div class="bodylayer oleft_leg_lower" style="opacity:'.$colors["Left Leg Lower"].';" title="'.$values['Left Leg Lower'].' EP" '.buildDataAttributes($values, $percent, 'Left Leg Lower').'></div>';
	$content.= '<div class="bodylayer oright_leg_lower" style="opacity:'.$colors["Right Leg Lower"].';" title="'.$values['Right Leg Lower'].' EP" '.buildDataAttributes($values, $percent, 'Right Leg Lower').'></div>';
	
	$content.= '<div class="bodylayer oleft_foot" style="opacity:'.$colors["Left Foot"].';" title="'.$values['Left Food'].'" '.buildDataAttributes($values, $percent, 'Left Food').'></div>';
	$content.= '<div class="bodylayer oright_foot" style="opacity:'.$colors["Right Foot"].';" title="'.$values['Right Food'].' EP" '.buildDataAttributes($values, $percent, 'Right Food').'></div>';
	

	$content.= '</div>';

   // $content.= '<div class="bodyoverlay"></div>';

	return $content;
}

function buildDataAttributes(&$values, &$percent, $key)
{
    return 'data-player-ep="'.$values[$key].'" data-player-percent="'.$percent[$key].'"';
}

?>