--
-- Tabellenstruktur für Tabelle `actions_full`
--

CREATE TABLE IF NOT EXISTS `actions_full` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roundid` int(11) NOT NULL,
  `time` int(11) NOT NULL,
  `action` char(32) COLLATE utf8_bin NOT NULL,
  `puid` int(11) DEFAULT NULL,
  `team` tinyint(2) NOT NULL,
  `pnam` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  `euid` int(11) DEFAULT NULL,
  `eteam` tinyint(4) DEFAULT NULL,
  `ename` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `weapon` int(11) DEFAULT NULL,
  `health` int(11) DEFAULT NULL,
  `mod` int(11) DEFAULT NULL,
  `hitlocation` int(11) DEFAULT NULL,
  `additional` int(11) DEFAULT NULL COMMENT 'various stuff',
  PRIMARY KEY (`id`),
  KEY `roundid` (`roundid`),
  KEY `action` (`action`),
  KEY `puid` (`puid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aliases`
--

CREATE TABLE IF NOT EXISTS `aliases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_log` varchar(64) COLLATE utf8_bin NOT NULL,
  `name` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `hash` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `hash` (`hash`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `chats`
--

CREATE TABLE IF NOT EXISTS `chats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roundid` int(11) NOT NULL,
  `puid` int(11) NOT NULL,
  `message` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `gameactions`
--

CREATE TABLE IF NOT EXISTS `gameactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_log` varchar(64) COLLATE utf8_bin NOT NULL,
  `name` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `points` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `games_full`
--

CREATE TABLE IF NOT EXISTS `games_full` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` int(11) NOT NULL,
  `rounds` varchar(255) COLLATE utf8_bin NOT NULL,
  `map` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `duration` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rounds` (`rounds`,`map`,`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `gametypes`
--

CREATE TABLE IF NOT EXISTS `gametypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_log` varchar(64) COLLATE utf8_bin NOT NULL,
  `name` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `team` int(11) DEFAULT '0',
  `max_play_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `hitlocations`
--

CREATE TABLE IF NOT EXISTS `hitlocations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_log` varchar(64) COLLATE utf8_bin NOT NULL,
  `name` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `points` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `logupdate`
--

CREATE TABLE IF NOT EXISTS `logupdate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `linecount` int(11) NOT NULL,
  `size` int(11) NOT NULL,
  `import` int(11) NOT NULL,
  `gametime` int(11) NOT NULL,
  `game_finished` tinyint(2) NOT NULL,
  `logtime` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `maps`
--

CREATE TABLE IF NOT EXISTS `maps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_log` varchar(64) COLLATE utf8_bin NOT NULL,
  `name` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `mods`
--

CREATE TABLE IF NOT EXISTS `mods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_log` varchar(64) COLLATE utf8_bin NOT NULL,
  `name` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `points` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `name_log` (`name_log`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `player_stats`
--

CREATE TABLE IF NOT EXISTS `player_stats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roundid` int(11) NOT NULL,
  `playerid` int(11) NOT NULL,
  `typeid` int(11) NOT NULL,
  `valuetypeid` int(11) NOT NULL,
  `value` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `playerid` (`playerid`,`typeid`,`valuetypeid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `rounds_full`
--

CREATE TABLE IF NOT EXISTS `rounds_full` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` int(11) NOT NULL,
  `duration` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `map` int(11) NOT NULL,
  `tmp_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `time` (`time`,`type`),
  KEY `map` (`map`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `servers_versions`
--

CREATE TABLE IF NOT EXISTS `servers_versions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `serverid` int(11) NOT NULL,
  `maps` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `modupdate` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `serverid` (`serverid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `spawnpoints`
--

CREATE TABLE IF NOT EXISTS `spawnpoints` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_log` varchar(64) COLLATE utf8_bin NOT NULL,
  `name` varchar(64) COLLATE utf8_bin NOT NULL,
  `mapid` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `mapid` (`mapid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `stats_types`
--

CREATE TABLE IF NOT EXISTS `stats_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `show_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `description` text COLLATE utf8_bin,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `stats_vars`
--

CREATE TABLE IF NOT EXISTS `stats_vars` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `typeid` int(11) NOT NULL,
  `show_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `description` text COLLATE utf8_bin,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `teams`
--

CREATE TABLE IF NOT EXISTS `teams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `playerid` int(11) NOT NULL,
  `roundid` int(11) NOT NULL,
  `gameid` int(11) NOT NULL,
  `time` int(11) NOT NULL,
  `team` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `team` (`team`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `var_store`
--

CREATE TABLE IF NOT EXISTS `var_store` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` char(32) COLLATE utf8_bin NOT NULL,
  `value` varchar(512) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `identifier` (`identifier`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `weapons`
--

CREATE TABLE IF NOT EXISTS `weapons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `weapon_group_id` int(11) DEFAULT NULL,
  `name_log` varchar(64) COLLATE utf8_bin NOT NULL,
  `name` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `killstreak` int(11) NOT NULL,
  `weapon_grouped_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Erstbefüllung von var_store
--

INSERT INTO `var_store` VALUES ('', 'last_round_id_games', '0');
