<?php
/*
	Oki sämtlich Daten aus Typo in die DB Pumpen
	
	- Maps, (US)
	- Spielarten, (US)
	- Waffen  (US)
	- HitLocations (US)
	- Spieler (US)
	- DamageTypes (US) > mods (MB)
	
*/

die();

require_once 'serv_db.inc.us.php';
require_once '../serv_db.inc.php';

$dbus = new serv_db_us();
$db = new mbdb();

$mapCount = 0;
$typeCount = 0;
//$playerCount = 0;
$weaponCount = 0;
$hitCount = 0;
$modCount = 0;


// Maps
$dbus->query_db("SELECT * FROM maps ORDER BY ID ASC");
$sql = "INSERT INTO maps VALUES ";

while($r = mysql_fetch_array($dbus->result))
{
	$name_log = $r['MAPNAME'];
	$name = $r['DisplayName'];
	$sql.= "('', '$name_log', '$name'),";
	
	$mapCount++;
}

$sql = removeLastChar($sql);
$db->query_db($sql);
$sql = "";



// Spielarten
$dbus->query_db("SELECT * FROM gametypes ORDER BY ID ASC");
$sql = "INSERT INTO gametypes VALUES ";

while($r = mysql_fetch_array($dbus->result))
{
	$name_log = $r['NAME'];
	$name = $r['DisplayName'];
	$sql.= "('', '$name_log', '$name'),";
	
	$typeCount++;
}

$sql = removeLastChar($sql);
$db->query_db($sql);
$sql = "";



// Waffen
$dbus->query_db("SELECT * FROM weapons ORDER BY ID ASC");
$sql = "INSERT INTO weapons VALUES ";

while($r = mysql_fetch_array($dbus->result))
{
	$type = $r['WeaponType'];
	$name_log = $r['INGAMENAME'];
	$name = $r['DisplayName'];
	$sql.= "('', '$type', '$name_log', '$name'),";
	
	$weaponCount++;
}

$sql = removeLastChar($sql);
$db->query_db($sql);
$sql = "";



// HitLocations
$dbus->query_db("SELECT * FROM hitlocations ORDER BY ID ASC");
$sql = "INSERT INTO hitlocations VALUES ";

while($r = mysql_fetch_array($dbus->result))
{
	$name_log = $r['BODYPART'];
	$name = $r['DisplayName'];
	$sql.= "('', '$name_log', '$name', ''),";
	
	$hitCount++;
}

$sql = removeLastChar($sql);
$db->query_db($sql);
$sql = "";



// Player
/*
$dbus->query_db("SELECT * FROM aliases ORDER BY ID ASC");
$sql = "INSERT INTO aliases VALUES ";

while($r = mysql_fetch_array($dbus->result))
{
	$name_log = $r['Alias'];
	$name = $r['Alias'];
	$sql.= "('', '$name_log', '$name', ''),";
	
	$playerCount++;
}

$sql = removeLastChar($sql);
$db->query_db($sql);
$sql = "";
*/




// DamageTypes > mods
$dbus->query_db("SELECT * FROM damagetypes ORDER BY ID ASC");
$sql = "INSERT INTO mods VALUES ";

while($r = mysql_fetch_array($dbus->result))
{
	$name_log = $r['DAMAGETYPE'];
	$name = $r['DisplayName'];
	$sql.= "('', '$name_log', '$name', ''),";
	
	$modCount++;
}

$sql = removeLastChar($sql);
$db->query_db($sql);
$sql = "";


echo $mapCount." Karten kopiert<br />";
echo $typeCount." Spielarten kopiert<br />";
echo $weaponCount." Waffen kopiert<br />";
echo $hitCount." HitLocations kopiert<br />";
echo $playerCount." Spieler kopiert<br />";
echo $modCount." Schadenstypen kopiert<br />";


?>