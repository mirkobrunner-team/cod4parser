<?php

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <title>Test</title>

	<meta http-equiv="content-language" content="de">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
</head>
<style>
.number {
    color: red;
}
.key {
    color: blue;
}

</style>
<body>

<form>

	<div>
		Categorie: <input id="w" type="text" value="help">
	</div>

	<div>
		Method: <input id="f" type="text" value="all">
	</div>

	<div>
		Param: <input id="p" type="text" value="">
	</div>
	<input type="button" id="rechne" value="Show" />
</form>

<div id="url"></div>

<pre id="ausgabe">

</pre>

<script>
    function syntaxHighlight(json) {
        if (typeof json != 'string') {
             json = JSON.stringify(json, undefined, 2);
        }
        json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
        return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
            var cls = 'number';
            if (/^"/.test(match)) {
                if (/:$/.test(match)) {
                    cls = 'key';
                } else {
                    cls = 'string';
                }
            } else if (/true|false/.test(match)) {
                cls = 'boolean';
            } else if (/null/.test(match)) {
                cls = 'null';
            }
            return '<span class="' + cls + '">' + match + '</span>';
        });
    }

    function fill(data) {
        var str = JSON.stringify(data, null, 4); // spacing level = 4
        $('#ausgabe').html(syntaxHighlight(str));
    }

	$('#rechne').click(function(){
	    var w = $('#w').val();
		var f = $('#f').val();
		var p = $('#p').val();
        $('#url').html('a.php?k=84urwofahnc843oqcrrn8&w='+w+'&f='+f+'&p='+p);
		$.ajax({
			url:'a.php?k=84urwofahnc843oqcrrn8&w='+w+'&f='+f+'&p='+p,
			type: 'GET',
			success: function(data) {
                fill(data);
			},
		});
	});

</script>

</body>
</html>