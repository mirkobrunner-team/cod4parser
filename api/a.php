<?php
$api_key = addslashes($_GET['k']);
$allowed_keys = array('84urwofahnc843oqcrrn8', '9w3rcnalz3brznxw8');
if(!in_array($api_key, $allowed_keys)) die();

$api_part = addslashes($_GET['w']);
$api_func = addslashes($_GET['f']);
$api_param = clean_array($_GET['p']);
$allowed = array('games', 'maps', 'players', 'weapons', 'gametypes', 'challenges', 'help', 'help2', 'medals', 'maps_drawer');

if(!in_array($api_part, $allowed)) die();


function clean_array($get) {
    if(strlen($get) == 0) return null;
    $t = explode(";", $get);
    $z;
    foreach($t AS $r) {
        $z[] = addslashes($r);
    }
    return $z;
}

function get_func_arg_names($func_name) {
    $f = new ReflectionFunction($func_name);
    $result = array();
    foreach($f->getParameters() as $param) {
        $result[] = $param->name;
    }
    return $result;
}

require_once '../core/misc/serv_db.inc.php';
if($api_part == 'games') {
    require_once '../core/misc/class.extendedArray.php';
    require_once '../core/misc/helpers.php';
}

require_once '../core/stats/'.$api_part.'.php';

$all_func = get_defined_functions();
$api_func = strtolower($api_func);
$return;

if(in_array($api_func, $all_func['user']) == true) {
	if(is_callable($api_func)) {
        $arg_list = get_func_arg_names($api_func);
        if(count($arg_list) == count($api_param)) {
            if(count($api_param) == 0) {
                $return = $api_func();
            } else {
                $return = call_user_func_array($api_func, $api_param);
            }

        } else {
            $return = array('error' => 'missing param');
        }
	} else {
	    $return = array('error' => 'unknown f param');
	}
} else {
    $return = array('error' => 'unknown f param');
}

header('Content-Type: application/json');
echo json_encode($return);
?>