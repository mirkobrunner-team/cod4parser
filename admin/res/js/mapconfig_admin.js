
Math.radians = function(degrees) {
  return degrees * (Math.PI / 180);
};

// Converts from radians to degrees.
Math.degrees = function(radians) {
  return radians * (180 / Math.PI);
};

var t = 'EFEF0934';
var timer1, timer2;

var isHeatMapOpen = function(){
    return false;
}



function checkForm()
{
    /*
    var newid = $('#selmap').val();

    if(mapid != newid)
    {
        //map s�ubern
        $('#zoom_factor').val(1);
        $('#quad_width').val(5);
        $('#flip_x').val(1);
        $('#newAngel').val(0);
        $('#pos_x').val(0);
        $('#pos_y').val(0);
        $('#rotation').val(0);
        $('#factor').val(0);
    }
    */
    return true;
}


function saveThisMap()
{
    var sizemod = $('#map_image').attr('xfactor');
    var classname = $('#map_image').attr('class');
    var top = parseInt($('#map').css('top'));
    var left = parseInt($('#map').css('left'));
	var zoom = $('#zoom_factor').val();
	var flip = $('#flip_x').val();
    var angle = $('#newAngel').val();

    console.log('will save map with id: ' + mapid);
    console.log(' size modifier: ' + sizemod);
    console.log(' position: ' + top + ' ' + left);
    console.log(' rotation-ident: ' + classname);
    console.log(' newAngle: ' + angle);
	console.log(' zoomFactor: ' + zoom);
	console.log(' flip_x:' + flip);
	

	var degrees = classname.substr(10);
	var f = 0;
	if(sizemod == 0) sizemod = 1;

	$.ajax({
  		url: '../api/index.php?p=' + t + '&m=' + mapid + '&x=' + top + '&y=' + left + '&r=' + degrees + '&fa=' + zoom + '&fm=' + sizemod + '&fx=' + flip + '&an=' + angle,
  		dataType: "json",

  		success: function(data)
  		{
      		if(data)
      		{	
				var test = data[0];
				alert('gespeichert' + ' ' + data[0]);
				console.log('gespeichter');
				console.log(data);
			}
  		},
  		fail: function(jqXHR, textStatus, errorThrown)
		{
			countResults = 0;
			console.log("fail " + errorThrown);
		},
		error: function(jqXHR, textStatus, errorThrown)
		{
			countResults = 0;
			console.log("error " + this.url);
		},
		done: function(data)
		{
			countResults = 0;
		}
	});
}

function rotateImage()
{
    var classname = $('#map_image').attr('class');
	var deg = 0;


    if(classname == 'map_image_0')
    {
    	$('#map_image').removeClass('map_image_0');
    	classname = 'map_image_90';
    	deg = 90;
    }
    else if(classname == 'map_image_90')
    {
    	$('#map_image').removeClass('map_image_90');
    	classname = 'map_image_180';
    	deg = 180;
    }
    else if(classname == 'map_image_180')
    {
    	$('#map_image').removeClass('map_image_180');
    	classname = 'map_image_270';
    	deg = 270;
    }
    else if(classname == 'map_image_270')
    {
    	$('#map_image').removeClass('map_image_270');
    	classname = 'map_image_0';
    	deg = 0;
    }

    console.log(classname);

    $('#map_image').addClass(classname);
    $('rotation').val(deg);
    $('#newAngel').val(Math.radians(deg));

}

function sizeImage(dir)
{
    var factor = $('#map_image').attr('xfactor')*1;
    var addFactor = 1;
    var w = $('#map_image').attr('width')*1;
    var h = $('#map_image').attr('height')*1;
    var top = parseInt($('#image').css('top'));
    var left = parseInt($('#image').css('left'));

    if(dir == '+')
    {
    	factor+= addFactor;
    	w+= addFactor;
    	h+= addFactor;

    	$('#map_image').css({'margin-top': top-addFactor+'px', 'margin-left' : left-addFactor+'px'});
        $('#image_map_wrap').css({'margin-top': top-addFactor+'px', 'margin-left' : left-addFactor+'px'});
    }
    else if(dir == '-')
    {
    	factor-= addFactor;
    	w-= addFactor;
    	h-= addFactor;

    	$('#map_image').css({'margin-top': top+addFactor+'px', 'margin-left' : left+addFactor+'px'});
        $('#image_map_wrap').css({'margin-top': top+addFactor+'px', 'margin-left' : left+addFactor+'px'});
    }

    $('#canvas_wrapper').css({'width': w+'px', 'heigth': h+'px'});

    $('#map_image').attr('xfactor', factor);

    $('#map_image').attr('width', w);
    $('#map_image').attr('height', h);
    $('#factor').val(factor);
    
    var f = (524 / w);
    
    $('#zoom_factor').val(f);
    $('#show_factor').html(f);
}

function transportFactor()
{
    $('#zoom_factor').val = $('#show_factor').html;
}








$(document).ready(function()
{
	var $dragging = null;

	$(document.body).on("mousemove", function(e) {
	   if($dragging)
	   {
	       $dragging.offset({
	           top: e.pageY,
	           left: e.pageX
	       });
	   }
	});
	
	$(document.body).on("mousedown", "div", function (event)
	{
	var obj = event.target.id;
	if(obj == 'map')
	{
	    $dragging = $(event.target);
	}
	else
	{
	    dragging = null;
	}
	
	});
	
	$(document.body).on("mouseup", function (e)
	{
	   $dragging = null;
	});
   
	
	$('#show_factor').click(function()
	{
		$('#zoom_factor').val(this.innerHTML);
	});
	
   
	$('#bigger').mousedown(function()
	{
		console.log('bla');
		timer1 = setInterval("sizeImage('+');", 50);
	});
	
	$('#bigger').mouseup(function()
	{
		clearInterval(timer1);
	});



	$('#smaler').mousedown(function()
	{
		console.log('alb');
		timer2 = setInterval("sizeImage('-');", 50);
	});
	
	$('#smaler').mouseup(function()
	{
		clearInterval(timer2);
	});	
});