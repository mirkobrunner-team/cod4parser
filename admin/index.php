<?php
require_once '../core/misc/serv_db.inc.php';
require_once '../core/misc/helpers.php';
require_once '../core/misc/class.extendedArray.php';

include_once '../core/stats/games.php';



$db = new serv_db();


?>
<html lang="de-DE">
	<head>
		<meta charset="utf-8">
		<title>CoD4 Log Parser - Server</title>

		<link rel="shortcut icon" href="http://k4f-in-berlin.de/fileadmin/images/Sontiges/faveicon.ico" type="image/x-icon; charset=binary">
		<link rel="icon" href="http://k4f-in-berlin.de/fileadmin/images/Sontiges/faveicon.ico" type="image/x-icon; charset=binary">

		<link rel="stylesheet" href="res/css/root.css">
		<link rel="stylesheet" href="res/css/nav.css">

		<script type="text/javascript" src="res/js/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="res/js/highcharts-4-custom.js"></script>

	</head>
	<body>

		<nav role="navigation">
			<div id="navcontent">
				<ul>
					<li><a href="mapfix.php">Map Konfig</a></li>
				</ul>
			</div>
		</nav>

		<div class="head_position"></div>

		<div id="page">

			<div id="main">


            </div>
        </div>
    </body>
</html>