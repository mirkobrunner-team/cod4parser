<?php

include '../core/misc/serv_db.inc.php';
include '../core/misc/class.extendedArray.php';
include '../core/misc/helpers.php';
include '../core/stats/maps_drawer.php';
include '../core/stats/games.php';


$weapons = new ExtendedArray();
$maps = new ExtendedArray();
$types = new ExtendedArray();
$hits = new ExtendedArray();
$players = new ExtendedArray();
$gameActions = new ExtendedArray();
$mods = new ExtendedArray();
$spawnPoints = new ExtendedArray();

$weapons->fill('weapons');
$maps->fill('maps');
$types->fill('gametypes');
$hits->fill('hitlocations');
$players->fill('aliases');
$gameActions->fill('gameactions');
$mods->fill('mods');
$spawnPoints->fill('spawnpoints');

function buildPlayerSelector($player_id, $game_id)
{
	if(isset($game_id))
	{
		$db = new mbdb();
		$db->query_db("SELECT DISTINCT(teams.playerid) as tid, aliases.hash as hash FROM teams, aliases WHERE gameid = '$game_id' AND aliases.id = teams.playerid ORDER BY aliases.hash ASC");
	
		$sel = ($player_id == -1) ? "selected" : "";
	
		$select = '<select name="player" id="player">';
		$select.= '<option value="-1" '.$sel.'>Alle</option>';
		
		while($arr = mysqli_fetch_array($db->result))
		{
			$sel = ($player_id == $arr['tid']) ? 'selected' : '';
			$select.= '<option value="'.$arr['tid'].'" '.$sel.'>'.$arr['hash'].'</option>';
		}
		
		$db->result->close();

		$select.= '</select>';	
	}
	else
	{
		$db = new mbdb();
		$db->query_db("SELECT * FROM aliases ORDER BY hash ASC");
	
		$select = '<select name="player" id="player">';
		
		$sel = ($player_id == -1) ? "selected" : "";
		
		$select.= '<option value="-1" '.$sel.'>Alle</option>';
		
		while($arr = mysqli_fetch_array($db->result))
		{
			$sel = ($player_id == $arr['id']) ? 'selected' : '';
			$select.= '<option value="'.$arr['id'].'" '.$sel.'>'.$arr['hash'].'</option>';
		}
		
		$db->result->close();

		$select.= '</select>';
	
	}
	
	return $select;
}


$t = array();

$content = "";
$map_id = -1;
$id = -1;
$player_id = -1;
$selmap  = null;
$showKills = 0;
$showTeam = -1;
$zoomFactor = 1;
$quadWidth = 5;
$flip_x = 1;
$pos_x = 0;
$pos_y = 0;
$rotation = 0;
$factor = 0;
$newAngel = 0;

//auswahl �ber spiel selektor sendet formular ab
//Daten aus der DB haben immer vorrang


if(isset($_POST['selmap']) && (is_numeric($_POST['selmap'])== true) && ($_POST['selmap']>0))
{
	//wir m�ssen schauen ob es f�r die Map schon ein eintrag gibt.
	$id = intVal($_POST['selmap']);

	$db = new serv_db();
	
	$db->query_db("SELECT * FROM games_full WHERE id =".$id);
	$rr = mysql_fetch_array($db->result);
	$rnds = $rr['rounds'];

    $map_id  = $rr['map'];
	$mapPath = getMapPath($maps->getLogName($rr['map']));
	
	$res = $db->query_assoc("SELECT * FROM maps_config WHERE mapid = '$map_id'");

    $loaded = false;

	if($res)
	{
        $loaded = true;
		$pos_x = $res[0]['pos_x'];
		$pos_y = $res[0]['pos_y'];
		$rotation = $res[0]['rotation'];
		$zoomFactor = $res[0]['factor'];
		$factor = $res[0]['map_fact'];
		$flip_x = $res[0]['flip_x'];
        $newAngel = $res[0]['angle'];
		
		if($factor <= 0) $factor = 1;
	}
	else
	{
		echo "take Data";

		$zoomFactor = $_POST['zoom_factor'];
		$quadWidth = $_POST['quad_width'];
		$flip_x = $_POST['flip_x'];
		$rotation = $_POST['rotation'];
		$factor = $_POST['factor'];
		$pos_x = $_POST['pos_x'];
		$pos_y = $_POST['pos_y'];
        $newAngel = $_POST['newAngel'];

		if($zoomFactor <= 0) $zoomFactor = 1;
		if($factor <= 0) $factor = 1;
	}
	
	if(isset($_POST['form_vals']))
	{
		echo "take Data from form";

		$zoomFactor = $_POST['zoom_factor'];
		$quadWidth = $_POST['quad_width'];
		$flip_x = $_POST['flip_x'];
		$rotation = $_POST['rotation'];
		$factor = $_POST['factor'];
		$pos_x = $_POST['pos_x'];
		$pos_y = $_POST['pos_y'];
		$newAngel = $_POST['newAngel'];

		if($zoomFactor <= 0) $zoomFactor = 1;
		if($factor <= 0) $factor = 1;
	}
	
    $player_id = $_POST['player'];
	$selmap = $_POST['selmap'];
    $showKills = $_POST['showkills'];
    $showTeam = $_POST['team'];
    

	
	
	if($rr['map'] == 75)
	{
		$mapPath = "../maps/compass_map_mp_k4f_test.jpg";
	}

	printSpawnPoints_new_($rnds, true, $flip_x, $zoomFactor, $newAngel);
}

?>
<html lang="de-DE">
	<head>
		<meta charset="iso-8859-1">
		<title>CoD4 Log Parser - Game</title>

			
		<link rel="stylesheet" href="../interface/res/css/interface.css" >
		<link rel="stylesheet" href="res/css/mapfix.css" >
		<link rel="stylesheet" href="../interface/res/css/maps.css" >

		
		<script type="text/javascript" src="../interface/res/js/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="../interface/res/js/heatmap.js"></script>
	</head>
	<body>


		<nav role="main">
			<ul>
				<li><a href="index.php">Start</a></li>
				<li><a href="mapfix.php">Map Konfig</a></li>
			</ul>
		</nav>
		
		<p>Map Fix</p>

		<form method="POST" action="" onsubmit="checkForm()">
			<?php
				echo buildGameSelector($id, true);
			?>
			<p>Multiplikator f&uuml;r Intervall (1000 == realtime oder 100 f&uuml;r zehn mal schneller)</p>
 			<input type="number" name="timer_mult" style="width: 60px;" value="<?php echo (isset($selmap)) ? $_POST['timer_mult'] : "10" ?>" id="time_mult">

 			<span>Zeige Pfade f&uuml;r Spieler</span>
                <?php
                    echo buildPlayerSelector($player_id, $selmap);
                ?>
 			<span>Zeige meine Absch&uuml;sse oder vom wem ich getroffen wurde</span>

 			<select name="showkills" id="showkills">
	 			<option value="0" <?php echo ($showKills==0) ? "selected" : ""; ?>>-</option>
	 			<option value="1" <?php echo ($showKills==1) ? "selected" : ""; ?>>Ich</option>
	 			<option value="2" <?php echo ($showKills==2) ? "selected" : ""; ?>>Andere</option>
 			</select>

            <span>Zeige Team</span>

            <select name="team" id="team">
                <option value="-1" <?php echo ($showTeam==-1) ? "selected" : ""; ?>>-</option>
                <option value="0" <?php echo ($showTeam==0) ? "selected" : ""; ?>>Alies</option>
	 			<option value="1" <?php echo ($showTeam==1) ? "selected" : ""; ?>>Axes</option>
            </select>

            <p>Admin</p>

            <input type="text" name="zoom_factor" style="width: 60px;" value="<?php echo $zoomFactor; ?>" id="zoom_factor">
            <span>Zoomfactor für das generiert Overlay. Standard ist 1. Bsp: zf = 1: (n * zf)</span>

            <br />
            <input type="number" name="quad_width" style="width: 60px;" value="<?php echo $quadWidth; ?>" id="quad_width">
            <span>Breite der quadratischen Grid in px. Standard ist 5. Beinflußt die Heatmap</span>
            <br />

            <input type="number" name="flip_x" style="width: 60px;" value="<?php echo $flip_x; ?>" id="flip_x">
            <span>Spiegelt die Koordinaten (1 = nein ; -1 = ja)</span>

            <br />
            <input type="text" name="newAngel" style="width: 60px;" value="<?php echo $newAngel; ?>" id="newAngel" />
            Drehung in rad
            <br />


            <input type="hidden" name="pos_x" id="pos_x" value="<?php echo $pos_x; ?>">
            <input type="hidden" name="pos_y" id="pos_y" value="<?php echo $pos_y; ?>">
            <input type="hidden" name="old_mapid" value="<?php echo (isset($selmap)); ?>" />
            
            <input type="hidden" name="rotation" id="rotation" value="<?php echo $rotation ; ?>">
			<input type="hidden" name="factor" id="factor" value="<?php echo $factor; ?>">
            <br />
            Nimm Formularwerte beim senden: 
            <input type="checkbox" name="form_vals" id="form_vals">
            <input type="submit" name="sender" value="neu senden" id="sender">
		</form>





		<div style="margin-top: 50px;margin-left: 150px;">

			<?php echo $content; ?>
			<p>Auf Punkt klicken f&uuml;r Info:</p>

			<div id="info"></div>
			
			<div id="canvas_wrapper">
				<div  id="image_map_wrap">
					<img src="<?php echo $mapPath; ?>" width="524" height="524" border="0" id="map_image" class="map_image_0" xfactor="0">
				</div>
				<canvas id="map" width="524" height="524" style="border:solid 1px black"></canvas>
				<div id="heatmap" style=""></div>
			</div>

			<div class="logger" id="logger"></div>

			<br />

            <div id="player"></div>

    			<canvas id="control" width="524" height="30" style="border:solid 1px black"></canvas>

    			<br />

    			<div class="startstop">
    				<span onclick="javascript:restart();">Restart</span><span onclick="javascript:pause();">Pause</span><span onclick="javascript:start();">Play</span>
    			</div>

    			<div class="startstop" id="other_controls">
    				<span onclick="javascript:switchElem('#heatmap');">Heatmap</span>
    			</div>

    			<div class="startstop" id="other_controls">
    				<span onclick="javascript:saveThisMap()" style="background-color: red; color:white;">Save</span>
    				<span onclick="javascript:rotateImage()">links</span>
    				<span id="bigger">Größe +</span>
    				<span id="smaler">Größe -</span>
    				<span style="margin-left: 1px;" id="show_factor" onclick="javascript:transportFactor();">.</span>
    			</div>

    			<div class="clearfix"></div>

    			<div id="actions"></div>

            </div>

			<ul>
				<li>Kreis: Spawnpunkt</li>
				<li>Dunkelgr&uuml;n: Haltung ge&auml;ndert</li>
				<li>Gr&uuml;n: Schaden verursacht (Linie Schusslinie)</li>
				<li>Blau: Kill verursacht (Lini Schusslinie)</li>
				<li>Grau: Bewegungspfad des gew&auml;ten Spielers</li>
			</ul>
			<ul>
				<li>Restart / Pause</li>
				<li>Bombenpl&auml;tze / Flaggen / HQÂ´s</li>
			</ul>

			<?php
				echo "<br /><br />".memory_get_peak_usage()." peak Mem | ".memory_get_usage()." norm Mem usage (bytes)<br />";

				//echo $content;
			?>
		</div>


		<script type="text/javascript">

			var mapid = <?php echo $map_id; ?>;
			var data = <?php ($selmap != null) ? buildJSDataArray($data).";" : 'new Array();'; ?>;
			var maprect = <?php ($selmap != null) ? buildJSDataArray($mapRect).";" : 'new Array();'; ?>;
			var playerNames = <?php ($selmap != null) ? buildJSPlayerArray($selmap).";" : "new Array();"; ?>;

			var count = data.length;
            var divWith = 525;


			//place the maprect (canvas) in the center above the img;
			tx = (divWith - maprect['x2']) * 0.5;
   			ty = (divWith - maprect['y2']) * 0.5;

            console.log($('#pos_x').val());

		    tx = ($('#pos_x').val() * 1);
			ty = ($('#pos_y').val() * 1);

			

			$('#map').attr('width', maprect['x2']);
			$('#map').attr('height', maprect['y2']);

			//$('#map').css({'top': (ty + divWith)+'px' , 'margin-left':tx+'px', 'width': maprect['x2' ] + 'px', 'height' : maprect['y2']+'px'});
			$('#map').css({'top': (tx + divWith)+'px' , 'left':ty + 'px', 'width': maprect['x2' ] + 'px', 'height' : maprect['y2'] + 'px'});


			var heatMap = <?php ($selmap != null) ? buildJSDataArray(computeHeatMap($map_id, $quadWidth, $zoomFactor, $flip_x)).";" : "new Array();"; ?>;

			$('#heatmap').attr('width', maprect['x2']);
			$('#heatmap').attr('height', maprect['y2']);
			$('#heatmap').css({'top': (ty) + 'px' , 'margin-left':tx + 'px', 'width': maprect['x2'] + 'px', 'height' : maprect['y2'] + 'px'});

			var config = {
			  container: document.getElementById('heatmap'),
			  radius: 10,
			  maxOpacity: .5,
			  minOpacity: 0,
			  blur: .75
			};

			var heatmap = h337.create(config);

			heatmap.setData(
				{
					min:0,
					max: heatMap['max'],
					data: heatMap['data'],
				});
			
			heatmap.repaint();

		</script>

        <script type="text/javascript" src="res/js/mapconfig_admin.js"></script>
		<script type="text/javascript" src="res/js/mapdrawer_admin.js"></script>
	</body>
</html>
