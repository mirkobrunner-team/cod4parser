<?php
date_default_timezone_set('Europe/Berlin');
require_once '../core/misc/serv_db.inc.php';
require_once '../core/misc/helpers.php';
require_once '../core/misc/class.extendedArray.php';

include_once '../core/stats/games.php';



$db = new mbdb();


?>
<html lang="de-DE">
	<head>
		<meta charset="utf-8">
		<title>CoD4 Log Parser - Server</title>

		<link rel="shortcut icon" href="http://k4f-in-berlin.de/fileadmin/images/Sontiges/faveicon.ico" type="image/x-icon; charset=binary">
		<link rel="icon" href="http://k4f-in-berlin.de/fileadmin/images/Sontiges/faveicon.ico" type="image/x-icon; charset=binary">

		<link rel="stylesheet" href="res/css/root.css">
		<link rel="stylesheet" href="res/css/nav.css">

		<script type="text/javascript" src="res/js/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="res/js/highcharts.js"></script>
		<script type="text/javascript" src="res/js/highcharts.codstats.theme.js"></script>

	</head>
	<body>


	    <nav role="navigation">
			<?php include 'nav.php'; ?>
		</nav>


		<div class="head_position"></div>

		<div id="page">

			<div id="main">



				
				<ul class="statboxes">
				
					<li class="statbox">
						
						<span class="fH2">
						<?php $arr = countGames(); echo $arr[0]['COUNT(id)']; ?>
						</span> 
						
						Spiele insgesamt gespielt
					</li>
					
					<li class="statbox">

						<span class="fH2">
						<?php $arr = countRounds(); echo $arr[0]['COUNT(id)']; ?>
						</span> 

						Runden insgesamt gespielt
					</li>
					
					<li class="statbox">
						<span class="fH2">
						<?php $arr = getMostPlayedMap(); $ms = $arr[0]['map_sum']; echo $arr[0]['name']; ?>
						</span>
						

						<?php echo $ms; ?> mal gespielt
					</li>
				</ul>
				
				<ul class="statboxes">
					
					<li class="statbox">
                    	<span class="fH2">
	    	            <?php $arr = getMostPlayedGametype(); $ms = $arr[0]['type_sum']; echo $arr[0]['name_log']; ?>
		                </span>

                        <?php echo $ms; ?> mal gespielt
					</li>
					
					<li class="statbox">
                        <span class="fH2">
                        <?php $arr = getLongestPlayedGame(); echo $arr[0]['mduration']; $ms = $arr[0]['name'] ?>
                        </span>

					   	<?php echo $ms; ?> lang gespielt
					</li>

					<li class="statbox">
                        <span class="fH2">
                        <?php $arr = totalPlayTime(); echo $arr[0]['mduration']; ?>
                        </span>

                        insgesamt gespielt.
					</li>
				</ul>	
				
				<ul class="statboxes">
					
					<li class="statbox">
                        <span class="fH2">
                        <?php $arr = totalKillSum(); echo $arr[0]['killsum']; ?>
                        </span>

						Abschüsse insgesamt geschafft
					</li>

					<li class="statbox">
						<span class="fH2">
						<?php $arr = getMostUsedWeapon(); echo $arr[0]['cweapon']; $ms = $arr[0]['name']; ?>
						</span>
						Abschüsse mit <?php echo $ms; ?>
					</li>
				
					<li class="statbox">
						<span class="fH2">
						<?php $arr = countMaps(); echo $arr[0]['COUNT(id)'];?>
						</span>
						fantastische Karten
					</li>
				
				</ul>

				<div class="clearfix"></div>

				<div id="server_visitors">

                    <div class="chart" id="visitors"></div>
                    
                    <br />
                    <div class="chart" id="games_chart"></div>
                  
                    
                    
                    <script type="text/javascript">
	                    
	                    var dat =  <?php echo json_encode(getPlayersCountByTimeInterval($interval = 'w')); ?>;
	                    var dat2 =  <?php echo json_encode(getGamesCountByTimeInterval('w')); ?>;
	                    var dat4 =  <?php echo json_encode(getNewPlayersCountByTimeInterval('w')); ?>;
	                    
	                    var c3 = dat4.length;
	                    var c = dat.length;
	                    
	                    
	                    var visitors = new Array;
	                    var new_visitors = new Array;
	                    var category = new Array;
	                    var games = new Array;
	                    
	                    for(i=0;i<c;i++)
	                    {
		                    var t = dat[i];
		                    var t2 = dat2[i];
		               
		                    visitors[i] = t['player_sum']*1;
		                    games[i]	= t2['games_sum']*1;
		                    category[i] = t['datum']*1;
		                    new_visitors[i] = 0;
		                    
		                    for(j=0;j<c3;j++)
		                    {
			                    var t3 = dat4[j];

			                    if(t['datum'] == t3['datum'])
			                    {
				                    new_visitors[i] = t3['player_sum']*1;
			                    }
		                    }
	                    }

	    
						Highcharts.setOptions(Highcharts.theme);
						
						$(function () {
						    $('#visitors').highcharts({
							    chart: {
									type: 'column'  
							    },
						        title: {
						            text: 'wöchentliche Spieler',
						            x: -20 //center
						        },
						        subtitle: {
						            text: 'Source: K4F HC Clan Server',
						            x: -20
						        },
						        xAxis: {
						            categories: category,
						            title: {
							            text: 'Woche'
						            	}
						            },
						        yAxis: {
						            title: {
						                text: 'Anzahl'
						            },
						            plotLines: [{
						                value: 0,
						                width: 1,
						                color: '#808080'
						            }]
						        },
						        tooltip: {
						            valueSuffix: '',
						            formatter: function () {
						                return '<b>' + this.x + '</b> Woche<br/>' +
						                    this.series.name + ': ' + this.y + '<br/>'
						            }
						        },

/*
						        legend: {
						            layout: 'vertical',
						            align: 'right',
						            verticalAlign: 'middle',
						            borderWidth: 0
						        },
*/

						        series: [{
						          
 						            name: 'Teilnehmer',
						            data: visitors
						        },
						        {
							        name: 'neue Teilnehmer',
							        data: new_visitors
						        },
						        {
							        name: 'Spiele',
							        data: games
						        }],
						    });
						    
						});
						        
						var dat3 =  <?php echo json_encode(getAllPlayerStatsByTimeInterval('w')); ?>;
	                    var c = dat3.length;
	                    
	                    var points = new Array;
	                    var kills = new Array;
	                    var deaths = new Array;
	                    var teamkills = new Array();
	                    var category = new Array;
						        
						for(i=0;i<c;i++)
	                    {
		                    var t = dat3[i];
		                    points[i] = t['points_sum']*1;
		                    kills[i] = t['kills_sum']*1;
		                    deaths[i] = t['deaths_sum']*1;
		                    teamkills[i] = t['teamkills_sum']*1;
		                    category[i] = t['datum'];
	                    }
	                    
	                    
	                    
	                    $(function () {
						    $('#games_chart').highcharts({
							    chart: {
									type: 'column'  
							    },
						        title: {
						            text: 'wöchentliche Spiele',
						            x: -20 //center
						        },
						        subtitle: {
						            text: 'Source: K4F HC Clan Server',
						            x: -20
						        },
						        xAxis: {
						            categories: category
						            },
						        yAxis: {
						            title: {
						                text: 'Anzahl'
						            },
						            plotLines: [{
						                value: 0,
						                width: 1,
						                color: '#808080'
						            }]
						        },
						        tooltip: {
						            valueSuffix: ''
						        },

						        series: [{
						          
 						            name: 'Punkte',
						            data: points
						        },
						        {
						          
 						            name: 'Kills',
						            data: kills
						        },
						        {
						          
 						            name: 'Deaths',
						            data: deaths
						        },
						        {
						          
 						            name: 'TeamKills',
						            data: teamkills
						        }],
						    });
						    
						});
						          
	                </script>
                    
                </div>
				

				
				
			</div>
		
		</div>
		
	</body>
</html>