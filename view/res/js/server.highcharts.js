function drawVisits()
{

    var chart = new Highcharts.Chart({
        chart: {
            renderTo: chartId,
            type: 'column',
            margin: 100,
            height: 500,
            spacingBottom: 10,
            marginTop: 10,
            marginBottom: 50,
            options3d: {
              enabled: true,
              alpha: 65,
              beta: 15,
              depth: 50,
              viewDistance: 100
            }
            },
            plotOptions: {
            column: {
              depth: 55,
              shadow: true,
            }
            },
            xAxis: {
            categories: names,
            labels: {
                      rotation: -71,
                      style: {
                          fontSize: '8px',
                          fontFamily: 'Verdana, sans-serif'
                      }
                  }
            },
            yAxis: {
                title: dataName
            },

            title: {
                text: chartTitle
            },
            subtitle: {
                text: chartSubTitle
            },
            series: [{
                name: dataName,
                data: nums,
                dataLabels: {
                    enabled: true,
                    rotation: -70,
                    color: '#000',
                    align: 'right',
                    format: '{point.y:.f}', // one decimal
                    y: -35, // 10 pixels down from the top
                    x: 9,
                    style: {
                        fontSize: '10px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                },
            }]
        });
}