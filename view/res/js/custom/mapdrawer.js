
var loggerObj = document.getElementById('logger');
var actionObj = document.getElementById('actions');
var selShowKills = document.getElementById('showkills');
var selShowTeam = document.getElementById('team');
var selTimer = document.getElementById('time_mult');
var selPlayer = document.getElementById('player');

var can = document.getElementById('map');
can.addEventListener("mousedown", doMouseDown, false);
var ctx = can.getContext("2d");
var player = 1;

var rect = can.getBoundingClientRect();
oldX = rect.left;
oldY = rect.top;

can.height = maprect['y2'] + 3;
loggerObj.height = can.height;
var control = document.getElementById('control');

var ctx2 = control.getContext("2d");
var rect = control.getBoundingClientRect();
contOldX = rect.left;
contOldY = rect.top;

var mod = rect.width / count;

//drawMap(player);
var rect = can.getBoundingClientRect();
oldX = rect.left;
oldY = rect.top;


var currentIndex = 0;
var nextIndex = 0;	
var nextSchedule = 0;
var oldSchedule = 0;
var dataCount = 0;
var drawCount = data.length;
var drawData = new Array();
var lastPosition = new Array();
var showMyKills = 1;
var showTeam = 1;
var fraction = -1;
var clearDurations = new Array(25, 35, 55);
var clearKDDurations = new Array(10, 15, 20);
var timer = null;
var accuracy = 4;


fillDataIntimesteps(data.length);

function resetAll()
{
	clearTimeout(timer);
	
	currentIndex = 0;
	nextIndex = 0;	
	nextSchedule = 0;
	oldSchedule = 0;
	dataCount = 0;
	drawCount = data.length;
	drawData = new Array();
	lastPosition = new Array();
	showMyKills = 1;
    showTeam = 1;
    fraction = -1;
	timer = null;
	
	ctx.clearRect(0, 0, can.width, can.height);

    while(logger.hasChildNodes())
    {
        logger.removeChild(logger.lastChild);
    }
}

function restart()
{
	resetAll();
	fillDataIntimesteps(data.length);
}

function pause()
{
	clearTimeout(timer);
}

function start()
{
	fillDataIntimesteps(data.length);
}

function getNextSchedule()
{
	oldSchedule = nextSchedule;
	nextSchedule = data[(currentIndex)][2];
    var c = data.length;

	for(i = currentIndex; i < c; i++)
	{
		if(data[(i)][2] != nextSchedule)
		{
			nextIndex = i;
			break;
		}						
	}
	currentIndex = nextIndex;
}

function fillDataIntimesteps(dataCount)
{
	drawMap(selPlayer.value);
	drawTimeLine();
	
	getNextSchedule();
	
	drawCount = nextIndex;
	
	var realTime = 	(nextSchedule -oldSchedule) * selTimer.value;
	var text = "";
	
	var t = data[i];
	
	text = nextSchedule+" Sek. "+t[3]+" " + getPlayerName(t[4]);
		
	if(t[3] == "K" || t[3] == "D")
	{
		text+= " von: " + getPlayerName(t[7]);
	}

	actionObj.innerHTML = text;

	
	var p = document.createElement("p");
	p.innerHTML = text;
	loggerObj.appendChild(p);
	
	loggerObj.scrollTop = loggerObj.scrollHeight;
	

	if(currentIndex < (dataCount - 1))
	{
		timer = setTimeout('fillDataIntimesteps(data.length)', realTime);	
	}
	else
	{
		clearTimeout(timer);
		actionObj.innerHTML = "Ende";
		text = "Ende";
	}

}

function drawMap(player)
{
	ctx.clearRect(0, 0, can.width, can.height);

	ctx.beginPath();
	ctx.strokeStyle = "#ff0000";
	ctx.rect(maprect['x1'], maprect['y1'], maprect['x2'] + 2, maprect['y2'] + 2);
	ctx.closePath();
	ctx.stroke();

	
	drawCount = nextIndex;//drawData.length;
	showMyKills = selShowKills.value;
    showTeam = selShowTeam.value;

    if(showMyKills > 0)
	{
		drawPlayerDamage(player, showMyKills);
    	drawPlayerKills(player, showMyKills);
	}


	if(player == -1)
	{
		drawUserPaths(player);
	}
	else
	{
		drawUserPathForPlayer(player);
	}
	
	ctx.beginPath()
	ctx.strokeStyle = "#000000";
	
	var fPI = Math.PI * 2;
	// alle Punkte plazieren
	for(i = 0; i < drawCount; i++)
	{
		var t = data[i];
		
		
		
		if(!lastPosition[t[4]]) lastPosition[t[4]] = new Array();

		lastPosition[t[4]]['x'] = t['pos_x'];
		lastPosition[t[4]]['y'] = t['pos_y'];
		lastPosition[t[4]]['z'] = t['pos_z'];
		
		

		if(t[3] == "SP")
		{
			ctx.beginPath();
			ctx.fillStyle = "#fffff";
			ctx.strokeStyle = "#ff0000";
			ctx.arc(t['pos_x'], t['pos_y'] , 4, 0, fPI, true);
			ctx.stroke();
			ctx.closePath();

		}
		else if(t[3] == "ST")
		{
			ctx.beginPath();
			ctx.strokeStyle = "#004500";
			ctx.fillStyle = "#004500";
			ctx.rect(t['pos_x'], t['pos_y'], 1, 1);
			ctx.stroke();
			ctx.closePath();
		}
		else if(t[3] == "D")
		{
			ctx.beginPath();
			ctx.strokeStyle = "#00ff00";
			ctx.fillStyle = "#004500";
			ctx.rect(t['pos_x'], t['pos_y'], 1, 1);
			ctx.stroke();
			ctx.closePath();
			
			if(t[10] == 5)
			{
				ctx.beginPath();
				ctx.strokeStyle = "#00FF00";
				ctx.arc(t['pos_x'], t['pos_y'] , 3, 0, fPI, true);
				ctx.stroke();
				ctx.closePath();
				
			}
		}
		else if(t[3] == "K")
		{
			ctx.beginPath();
			ctx.strokeStyle = "#0000FF";
			ctx.fillStyle = "#0000FF";
			ctx.rect(t['pos_x'], t['pos_y'], 1, 1);
			ctx.stroke();
			ctx.closePath();
			
			if(t[10] == 5)
			{
				ctx.beginPath();
				ctx.strokeStyle = "#0000FF";
				ctx.arc(t['pos_x'], t['pos_y'] , 3, 0, fPI, true);
				ctx.stroke();
				ctx.closePath();
			}
		}
		
	}

	
}




function drawPlayerDamage(player, showKills)
{
	for(i = 0; i < drawCount; i++)
	{
		var t = data[i];
		
		if(t[3] == "D")
		{

			if(!lastPosition[t[4]]) lastPosition[t[4]] = new Array();

			lastPosition[t[4]]['x'] = t['pos_x'];
			lastPosition[t[4]]['y'] = t['pos_y'];
			lastPosition[t[4]]['z'] = t['pos_z'];
			
			if(i < substractToZero(drawCount, clearKDDurations[3]))
			{
				ctx.strokeStyle = "#efe";
			}
			else if(i < substractToZero(drawCount, clearKDDurations[1]))
			{
				ctx.strokeStyle = "#afa";
			}
			else if(i < substractToZero(drawCount, clearKDDurations[0]))
			{
				ctx.strokeStyle = "#8f8";
			}
			else
			{
				ctx.strokeStyle = "#0f0";
			}
			
			if(player == -1)
			{
				ctx.beginPath();
				ctx.moveTo(lastPosition[t[7]]['x'], lastPosition[t[7]]['y']);
				ctx.lineTo(t['pos_x'], t['pos_y']);
				ctx.stroke();			
				ctx.closePath();
			}
			else if(player == t[4] && showMyKills == 2)
			{
				ctx.beginPath();
				ctx.moveTo(lastPosition[t[7]]['x'], lastPosition[t[7]]['y']);
				ctx.lineTo(t['pos_x'], t['pos_y']);
				ctx.stroke();			
				ctx.closePath();
			}
			else if(player == t[7] && showMyKills == 1)
			{
				ctx.beginPath();
				ctx.moveTo(lastPosition[t[7]]['x'], lastPosition[t[7]]['y']);
				ctx.lineTo(t['pos_x'], t['pos_y']);
				ctx.stroke();			
				ctx.closePath();
				
				
			}
							
		}
	}		
}


function drawPlayerKills(player, showKills)
{
    for(i = 0; i < drawCount; i++)
	{
		var t = data[i];
		
		if(t[3] == "K")
		{
            if(!lastPosition[t[4]]) lastPosition[t[4]] = new Array();

			lastPosition[t[4]]['x'] = t['pos_x'];
			lastPosition[t[4]]['y'] = t['pos_y'];
			lastPosition[t[4]]['z'] = t['pos_z'];
			
            if(i < substractToZero(drawCount, clearKDDurations[2]))
			{
				ctx.strokeStyle = "#eef";
			}
			else if(i < substractToZero(drawCount, clearKDDurations[1]))
			{
				ctx.strokeStyle = "#aaf";
			}
			else if(i < substractToZero(drawCount, clearKDDurations[0]))
			{
				ctx.strokeStyle = "#88f";
			}
			else
			{
				ctx.strokeStyle = "#00f";
			}


            if(player == -1)
			{
				ctx.beginPath();
				ctx.moveTo(lastPosition[t[7]]['x'], lastPosition[t[7]]['y']);
				ctx.lineTo(t['pos_x'], t['pos_y']);
				ctx.stroke();
				ctx.closePath();
			}
			else if(player == t[4] && showMyKills == 2)
			{
				ctx.beginPath();
				//ctx.strokeStyle = "#00f";
				ctx.moveTo(lastPosition[t[7]]['x'], lastPosition[t[7]]['y']);
				ctx.lineTo(t['pos_x'], t['pos_y']);
				ctx.stroke();
				ctx.closePath();
			}
			else if(player == t[7] && showMyKills == 1)
			{
				ctx.beginPath();
				//ctx.strokeStyle = "#00f";
				ctx.moveTo(lastPosition[t[7]]['x'], lastPosition[t[7]]['y']);
				ctx.lineTo(t['pos_x'], t['pos_y']);
				ctx.stroke();
				ctx.closePath();
			}
		}
    }
}

function drawUserPaths(player)
{
	var c = playerNames.length;
	var u = 0;
	
	for(u = 0; u < c; u++)
	{
		if(showTeam < 0)
		{
			drawUserPathForPlayer(playerNames[u]['tid'], false);
		}
		else if(showTeam >= 0)
		{
			if(playerNames[u]['team'] == showTeam)
			{
				drawUserPathForPlayer(playerNames[u]['tid'], true);
			}
            else
            {
                drawUserPathForPlayer(playerNames[u]['tid'], false);
            }
		}
	}
}

function drawUserPathForPlayer(playerid, drawBlack)
{
	for(i = 0; i < drawCount; i++)
	{
		var t = data[i];

        if(drawBlack == true)
        {
    		if(i < substractToZero(drawCount, clearDurations[2]))
    		{
    			ctx.strokeStyle = "#eee";
    		}
    		else if(i < substractToZero(drawCount, clearDurations[1]))
    		{
    			ctx.strokeStyle = "#aaa";
    		}
    		else if(i < substractToZero(drawCount, clearDurations[0]))
    		{
    			ctx.strokeStyle = "#777";
    		}
    		else
    		{
    			ctx.strokeStyle = "#000";
    		}
        }
        else
        {
           if(i < substractToZero(drawCount, clearDurations[2]))
    		{
    			ctx.strokeStyle = "#eee";
    		}
    		else if(i < substractToZero(drawCount, clearDurations[1]))
    		{
    			ctx.strokeStyle = "#ddd";
    		}
    		else if(i < substractToZero(drawCount, clearDurations[0]))
    		{
    			ctx.strokeStyle = "#ccc";
    		}
    		else
    		{
    			ctx.strokeStyle = "#aaa";
    		}
        }


		if(t[4] == playerid)
		{
			if(t[3] == "SP")
			{
				ctx.beginPath();
				ctx.strokeWidth = 1;
				ctx.moveTo(t['pos_x'], t['pos_y']);
			}
			else
			{
				ctx.lineTo(t['pos_x'], t['pos_y']);
				ctx.moveTo(t['pos_x'], t['pos_y']);
				ctx.stroke();
				ctx.closePath();
			}
		}
	}
    ctx.closePath();
}

function substractToZero(val, sub)
{
	val = val - sub;
	return (val < 0) ? 0 : val;
}


function getLastKnownPositionForUser(uid, index)
{
	for(i = index; i > 1; i--)
	{
		if(data[i][4] == uid)
		{
			return i;
		}
	}
	return -1;
}



function doMouseDown(e)
{
	
	getDataByCoords(e.pageX - oldX, e.pageY - oldY);
}

function getPlayerName(uid)
{
	var c = playerNames.length;
	
	for(i=0;i<c;i++)
	{
		if(playerNames[i]['tid'] == uid)
		{
			return playerNames[i]['hash'];
		}
	}
}

function getDataByCoords(x, y)
{
	var t = false;;
	for(i = 0;i < count; i++)
	{
		if(data[i]['pos_x'] >= x-accuracy && data[i]['pos_y'] >= y-accuracy)
		{
			if(data[i]['pos_x'] <= x+accuracy && data[i]['pos_y'] <= y+accuracy)
			{
				t = data[i];
				break;
			}
		}
	}
	
	if(t==false) return;
	
	var text = t[2]+" Sek. "+t[3]+" " + getPlayerName(t[4]);

	if(t[3] == "K" || t[3] == "D")
  	{
	  	text+= " von: " + getPlayerName(t[7]);
  	}
	document.getElementById('info').innerHTML = text;
					
}












function drawTimeLine()
{
	ctx2.clearRect(0, 0, control.width, control.height);

	ctx2.beginPath();
	ctx2.strokeStyle = "#ff0000";
    ctx2.fillStyle = '#ff0000';
	ctx2.rect(0, 5, nextIndex * mod, 15);
	ctx2.closePath();
	ctx2.stroke();
}


