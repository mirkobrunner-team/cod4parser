<?php
date_default_timezone_set('Europe/Berlin');

require_once '../core/misc/serv_db.inc.php';
require_once '../core/misc/helpers.php';
require_once '../core/misc/class.extendedArray.php';

include_once '../core/stats/players.php';
include_once '../core/stats/players_body.php';

$db = new mbdb();

$playerid = NULL;
$kills = 0;
$deaths = 0;
$ratio = 0;
$chart_ratio = array();
$content = "";

$playerObj;

function buildSelectedContent($playerid)
{
    global $chart_ratio, $from_highscore, $kills, $deaths, $ratio, $playerObj;

	$playerObj 	= getPlayerStatsRoundsRow($playerid);
	$kills 		= getBasicPlayerKillStats($playerid);
	$deaths 	= getBasicPlayerDeathStats($playerid);
	$ratio		= getKDRatio($kills['Kills'], $deaths['Kills']);
	$hea 		= getPlayerHeadshoots($playerid);
	$playerObj[0]['heads'] = $hea[0]['kills'];

	$arr1 = array();
	$arr2 = array();

    $a = $kills['Kills'];
	$b = $deaths['Kills'];

	$c = $a + $b;
	if($c==0) $c = 1;
	$d = ($b / $c) * 100;

	$arr1[] = 'Kills';
	$arr1[] = 100 - $d;

	$arr2[] = 'Deaths';
	$arr2[] = $d;

	$chart_ratio[] = $arr1;
	$chart_ratio[] = $arr2;
}

function buildPlayersHighscoreTable()
{
	$content = '<table class="tablesorter" id="highscore_table">';
	$content.= '<thead title="Zum Sortieren klicken">';
	$content.= '<tr class="table_row" align="left"><th style="width: 400px">Spieler</th>';
	$content.= '<th align="right" style="width: 50px;">Spiele</th>';
	$content.= '<th align="right" style="width: 50px;">Punkte</th>';
	$content.= '<th align="right" style="width: 50px;">Kills</th>';
	$content.= '<th align="right" style="width: 50px;">Deaths</th>';
	$content.= '<th align="right" style="width: 50px;">KDR</th>';
	$content.= '<th align="right" style="width: 50px;">Teamkills</th>';
	$content.= '<th align="right" style="width: 50px;">Suizide</th>';
	$content.= '</tr>';
	$content.= '</thead>';

	$content.= '<tbody>';

	$arr = getPlayersHighscoresForTables();
	
	
	foreach($arr as $r)
	{
		$content.= '<tr class="table_row">';
		
		
		$content.= '<td><form method="POST" action="" id="player_f_'.$r['playerid'].'"><input type="hidden" name="player" value="'.$r['playerid'].'"><span style="cursor:pointer;" onclick="document.getElementById(\'player_f_'.$r['playerid'].'\').submit();">'.$r['name'].'</span></form></td>';
		
		$content.= '<td align="right">'.$r['sum_games'].'</td>';
		
		$content.= '<td align="right">'.$r['sum_points'].'</td>';
		
		$content.= '<td align="right">'.$r['sum_kills'].'</td>';
		
		$content.= '<td align="right">'.$r['sum_death'].'</td>';
		
		$content.= '<td align="right">'.round($r['kdr'], 4).'</td>';
		
		$content.= '<td align="right">'.$r['sum_teamkills'].'</td>';
		
		$content.= '<td align="right">'.$r['sum_suizide'].'</td>';
		
		$content.= '</tr>';
	}

	$content.= '</tbody>';
	$content. '</table>';
	
	$content.= '<script type="text/javascript">$(document).ready(function(){$("#highscore_table").tablesorter();});</script>';
	
	return $content;
}




if(isset($_POST['player']))
{
	$playerid = $_POST['player'];

    buildSelectedContent($playerid);
}
else
{
	$content = buildPlayersHighscoreTable();
}

if($playerid != 0)
{
    $content2.= '<div class="col2">';
    $content2.= '<p class="hit_text">ausgeteilter Schaden nach Körperregion</p>';
    $content2.= buildHTMLBody(getPlayerHitLocationsInflictedTotal($playerid));
    $content2.= '</div><div class="col2">';
    $content2.= '<p class="hit_text">erhaltender Schaden nach Körperregion</p>';
    $content2.= buildHTMLBody(getPlayerHitLocationsRecievedTotal($playerid));
    $content2.= '</div>';
    $content2.= '<div class="clearfix"></div>';

    $content2.= '<script type="text/javascript" src="res/js/players_body.js"></script>';
}





?>
<!DOCTYPE HTML>
<html lang="de-DE">
	<head>
		<meta charset="utf-8">
		<title>CoD4 Log Parser - Players</title>

		<link rel="shortcut icon" href="http://k4f-in-berlin.de/fileadmin/images/Sontiges/faveicon.ico" type="image/x-icon; charset=binary">
		<link rel="icon" href="http://k4f-in-berlin.de/fileadmin/images/Sontiges/faveicon.ico" type="image/x-icon; charset=binary">

		<link rel="stylesheet" href="res/css/root.css" />
		<link rel="stylesheet" href="res/css/nav.css" />
        <link rel="stylesheet" href="res/css/body.css" />
		<link rel="stylesheet" href="res/css/players.css" />

		<script type="text/javascript" src="res/js/jquery-2.1.4.min.js"></script>
		<script type="text/javascript" src="res/js/jquery.tablesorter.js"></script>
		<script type="text/javascript" src="res/js/highcharts-4-custom.js"></script>
		<script type="text/javascript" src="res/js/highcharts.codstats.theme.js"></script>
		<script type="text/javascript" src="res/js/custom/players.js"></script>

	</head>
	<body>

		<nav>
			<?php include 'nav.php'; ?>
		</nav>

		<div class="head_position"></div>

		<div id="page">

			<div id="main">

				<form method="POST" action="<?php $_PHP['self']; ?>">
                    <input type="hidden" name="from_highscore" value="<?php echo $from_highscore; ?>" />
					<?php
						echo buildPlayerSelector($playerid);
					?>
				</form>
				
				<div class="table_wrapper">
					
					<?php echo $content; ?>
					
					<?php echo $player_info; ?>
					
					
				</div>	
				
				
				
				<?php if(isset($playerid)){ ?>
				
				<!--
					
					Punkte, Punkte pro Spiel, Kills, Deaths, Teamkills, TeamDeaths, Suizide, Abschüsse, Spiele teilgenommen, Spielzeit
					Teamplayer Faktor (Kills / TeamKills), Schaden ausgeteilt, Schaden eingesteckt, Zeit / Kill, Zeit / Damage
					
					Punkte
						Punkte pro Spiel
						Punkte pro Stunde(?)
						
					Kills
						
				-->
				
				<div id="player_info_wrap">
					
					<div id="player_info_list">
						
						<table border="0" cellpadding="0" cellspacing="0">
							<tbody>
								<tr onclick="showPlayerInfo('punkte');">
									<td class="name">Punkte</td>
									<td class="value"><?php echo $playerObj[0]['points']; ?></td>
								</tr>
								<tr onclick="showPlayerInfo('kills');">
									<td class="name">Kills</td>
									<td class="value"><?php echo $kills['Kills']; ?></td>
								</tr>
								<tr onclick="showPlayerInfo('deaths');">
									<td class="name">Deaths</td>
									<td class="value"><?php echo $deaths['Kills']; ?></td>
								</tr>
								<tr onclick="showPlayerInfo('kdr');">
									<td class="name">Kill Death Ratio (KDR)</td>
									<td class="value"><?php echo $ratio; ?></td>
								</tr>
								<tr onclick="showPlayerInfo('teamkills');">
									<td class="name">Teamkills</td>
									<td class="value"><?php echo $kills['TeamKills']; ?></td>
								</tr>
								<tr onclick="showPlayerInfo('teamdeaths');">
									<td class="name">TeamDeaths</td>
									<td class="value"><?php echo $deaths['TeamKills']; ?></td>
								</tr>
								<tr onclick="showPlayerInfo('suizide');">
									<td class="name">Suizide</td>
									<td class="value"><?php echo $playerObj[0]['suizide']; ?></td>
								</tr>
								<tr onclick="showPlayerInfo('treffer');">
									<td class="name">Kopfschüsse</td>
									<td class="value"><?php echo $playerObj[0]['heads']; ?></td>
								</tr>
							</tbody>
						</table>
						
					</div>
					
					<div id="player_info_description">
						<h3>Beschreibung</h3>
						<p></p>
					</div>
					
					<div class="clearfix"></div>
					
				</div>
				
				
				<div class="chart chart_half floated" id="punkte"></div>
				<div class="chart chart_half floated" id="kdr"></div>
				<br style="clear:both;" />
				<div class="chart" id="kills"></div>
				
				<?php } ?>
				
					<script type="text/javascript">
						
						var dat =  <?php echo json_encode(getPlayersPointsGameline($playerid)); ?>;
	                    var c = dat.length;
	                    
	                    var total = new Array;
	                    var punkte = new Array;
	                    var category = new Array;
	                    var punkte = new Array;
	                    
	                    for(i=0;i<c;i++)
	                    {
		                    var t = dat[i];
		                    total[i] = t['total']*1;
		                    punkte[i] = t['punkte']*1;
		                    category[i] = t['datum'];
		                    
	                    }
	    
						Highcharts.setOptions(Highcharts.theme);
						
						$(function () {
						    $('#punkte').highcharts({
							    chart: {
									type: 'line',
									zoomType: 'x',
							    },
						        title: {
						            text: 'erhaltene Punkte',
						            x: -20 //center
						        },
						        subtitle: {
						            text: 'Source: K4F HC Clan Server',
						            x: -20
						        },
						        xAxis: {
						            categories: category
						            },
						        yAxis: {
						            title: {
						                text: 'Punkte'
						            },
						            plotLines: [{
						                value: 0,
						                width: 1,
						                color: '#808080'
						            }],
						            min: 0
						        },
						        tooltip: {
						            valueSuffix: ''
						        },
						        series: [{
 						            name: 'Punkte kum.',
						            data: total
						        },
						        {
							        name: 'Punkte pro Spiel',
							        data: punkte
						        }
						        ],
						    });
						    
						});
						
						
						
						
						
						
						var dat1 =  <?php echo json_encode($chart_ratio); ?>;
	                    var c = dat.length;
						
						$(function () {
						    $('#kdr').highcharts({
						        chart: {
						            plotBackgroundColor: null,
						            plotBorderWidth: 0,
						            plotShadow: false,
						        },
						        title: {
						            text: 'Kill Death Ratio',
						            align: 'center',
						            verticalAlign: 'middle',
						            y: 40
						        },
						        tooltip: {
						            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
						        },
						        plotOptions: {
						            pie: {
						                dataLabels: {
						                    enabled: true,
						                    distance: -50,
						                    style: {
						                        fontWeight: 'bold',
						                        color: 'white',
						                        textShadow: '0px 1px 2px black'
						                    }
						                },
						                startAngle: -90,
						                endAngle: 90,
						                center: ['50%', '75%']
						            }
						        },
						        series: [{
						            type: 'pie',
						            name: 'Browser share',
						            innerSize: '50%',
						            data: dat1
						        }]
						    });
						});
						
						
						
						
						
						
						
						
						var dat2 =  <?php echo json_encode(getPlayersKillsFullGameline($playerid)); ?>;
	                    var c2 = dat2.length;
	                    
	                    var kills = new Array;
	                    var deaths = new Array;
	                    var teamkill = new Array;
	                    var suizid = new Array;
	                    var kdr = new Array;
	                    var cat2 = new Array;
	                    
	                    for(i=0;i<c2;i++)
	                    {
		                    var t = dat2[i];
		                    kills[i] = t['sum_kills']*1;
		                    deaths[i] = t['sum_death']*1;
		                    teamkill[i] = t['teamkills']*1;
		                    suizid[i]	= t['suizide']*1;
		                    kdr[i]		= t['kdr']*1;
		                    cat2[i] = t['datum'];
		                    
	                    }
	    
						Highcharts.setOptions(Highcharts.theme);
						
						$(function () {
						    $('#kills').highcharts({
							    chart: {
									type: 'line' ,
									zoomType: 'x',
							    },
						        title: {
						            text: 'Kills and Deaths',
						            x: -20 //center
						        },
						        subtitle: {
						            text: 'Source: K4F HC Clan Server',
						            x: -20
						        },
						        xAxis: {
						            categories: cat2
						            },
						        yAxis: {
						            title: {
						                text: 'Anzahl'
						            },
						            plotLines: [{
						                value: 0,
						                width: 1,
						                color: '#808080'
						            }],
						            min: 0,
						        },
						        tooltip: {
						            valueSuffix: ''
						        },
						        series: [{
 						            name: 'Kills',
						            data: kills
						        },
						        {
							        name: 'Deaths',
							        data: deaths
						        },
						        {
							        name: 'Teamkills',
							        data: teamkill
						        },
						        {
							        name: 'Suizide',
							        data: suizid
						        },
						        {
							        name: 'KDR',
							        data: kdr
						        }
						        ],
						    });
						    
						});
						
						
					</script>

			    <?php echo $content2; ?>

            </div>
        </div>
    </body>
</html>