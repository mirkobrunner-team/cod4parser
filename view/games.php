<?php

date_default_timezone_set('Europe/Berlin');

require_once '../core/misc/serv_db.inc.php';
require_once '../core/misc/helpers.php';
require_once '../core/misc/class.extendedArray.php';
require_once '../core/stats/maps_drawer.php';

include_once '../core/stats/games.php';

$db = new mbdb();
$maps = new ExtendedArray();
$types = new ExtendedArray();

$maps->fill('maps');
$types->fill('gametypes');

?>
<html lang="de-DE">
	<head>
		<meta charset="utf-8">
		<title>CoD4 Log Parser - Spiele</title>

		<link rel="shortcut icon" href="http://k4f-in-berlin.de/fileadmin/images/Sontiges/faveicon.ico" type="image/x-icon; charset=binary">
		<link rel="icon" href="http://k4f-in-berlin.de/fileadmin/images/Sontiges/faveicon.ico" type="image/x-icon; charset=binary">

		<link rel="stylesheet" href="res/css/root.css">
		<link rel="stylesheet" href="res/css/nav.css">
		<link rel="stylesheet" href="res/css/games.css">

		<script type="text/javascript" src="res/js/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="res/js/highcharts-4-custom.js"></script>
		<script type="text/javascript" src="res/js/jquery.tablesorter.js"></script>
		
	</head>
	<body>

		<nav role="navigation">
			<?php include 'nav.php'; ?>
		</nav>

		<div class="head_position"></div>

		<div id="page">

			<div id="main">

				<?php
					
					echo buildGamesTable();
				?>

            </div>
        </div>
    </body>
</html>