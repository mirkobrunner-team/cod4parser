<?php
date_default_timezone_set('Europe/Berlin');
include('/var/customers/webs/Mirko/verwackeltes/test/cod4parser/core/misc/serv_db.inc.php');
include('/var/customers/webs/Mirko/verwackeltes/test/cod4parser/core/misc/helpers.php');
include('../core/stats/maps.php');
include('../core/stats/maps_drawer.php');

$db = new mbdb();

$t = array();

$content = "";
$map_id = -1;


if(isset($_POST['maps']) && $_POST['maps'] > 0)
{
	$map_id = $_POST['maps'];

	
	$content = "Wie oft Spielart auf Karte gespielt<br />";
	$content.= $db->show_in_table(getGameTypeMapsCount($map_id));
	$content.= "<br /><br />";
	$content.= getActionCountPerMap($map_id);
	$content.= '<br /><br />Bekannte Startpunkte:<br />';
	$content.= printSpawnPoints($map_id);
	$content.= '<br /><br />Bekannte Nutzerposition:<br />';
	$content.= printAllPoints($map_id, false);
}
else
{
	$content = $db->show_in_table(getMapsCountList());
}

?>
<html lang="de-DE">
	<head>
		<meta charset="iso-8859-1">
		<title>CoD4 Log Parser - Gametypes</title>

		<link rel="stylesheet" href="res/css/interface.css" >

	</head>
	<body>
		<nav role="main">
			<a href="http://k4f-in-berlin.de">K4F Home</a>&nbsp;<a href="server.php">Server Stats</a>&nbsp;<a href="index.php">Runden Stats</a>&nbsp;<a href="player.php">Spieler Stats</a>&nbsp;<a href="gametypes.php">Spielarten Stats</a>&nbsp;<a href="maps.php">Maps Stats</a>&nbsp;<a href="weapons.php">Waffen Stats</a>&nbsp;<a href="game.php">Koord Tests</a>&nbsp;<a href="challenges.php">Herausforderungen</a>
		</nav>
		<p>Test Maps Statistiken</p>
		<form method="POST" action="">
			<?php
				echo buildMapSelector($map_id);
			?>
		</form>
		
		
		
		<div style="margin-top: 50px;">
			
			<?php echo $content; ?>
			
			<?php
			
				echo "<br /><br />".memory_get_peak_usage()." peak Mem | ".memory_get_usage()." norm Mem usage (bytes)<br />";
			?>
		</div>
		
	</body>
</html>
	
	