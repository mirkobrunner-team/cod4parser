<?php
	
error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);

require_once '../serv_db.inc.php';
require_once '../serv_db.inc.us.php';
require_once '../class.extendedArray.php';
require_once '../helpers.php';

$weapons = new ExtendedArray();
$maps = new ExtendedArray();
$types = new ExtendedArray();
$hits = new ExtendedArray();
$players = new ExtendedArray();
$gameActions = new ExtendedArray();
$mods = new ExtendedArray();

$weapons->fill('weapons');
$maps->fill('maps');
$types->fill('gametypes');
$hits->fill('hitlocations');
$players->fill('aliases');
$gameActions->fill('gameactions');
$mods->fill('mods');



function buildRoundSelecltor()
{
	global $maps, $types;
	
	$db = new mbdb();
	$db->query_db("SELECT * FROM games_full ORDER BY time ASC");

	$cont = "<select name='selmap' onchange='this.form.submit()'>";
	$cont.= "<option value='0'>-</option>";
	
	while($r = mysqli_fetch_array($db->result))
	{
		$time = date('m/d/Y H:i:s', $r['time']);
		
		$cont.= '<option value="'.$r['id'].'">'.$time." ".$types->getLogName($r['type'])." ".$maps->getLogName($r['map']).'</option>';
	}
	
	$cont.= "</select>";
	return $cont;
}

/*
	return:
		1 - suizide hit
		2 - team hit
		3 - hit
*/
function checkIfThatKD(&$r, $type)
{
	if($r['puid'] == $r['euid'])
	{
		return 1;
	}
	else if(($r['team'] == $r['eteam']) && ($type > 2))
	{
		return 2;
	}
	else
	{
		return 3;
	}
}

$ply = array();
$content = "";
$contB = "";
$feed = "";
$i = 0;

if(isset($_POST['selmap']))
{	
	$id = intVal($_POST['selmap']);
	$db = new mbdb();
	$db2 = new mbdb();
	
	
	
	$db2->query_db("SELECT * FROM games_full WHERE id = '$id'");
	$rr = mysqli_fetch_array($db2->result);
	
	$rnds = $rr['rounds'];
	$cRounds = explode(",", $rnds);
	
	$content.= date('m/d/Y H:i:s', $rr['time'])."<br />";
	$content.= $maps->getLogName($rr['map'])."<br />";
	$content.= $types->getLogName($rr['type'])."<br /><br />";
	
	$db->query_db("SELECT * FROM actions_full WHERE roundid IN($rnds) ORDER BY id ASC");
	
	
	
	while($r = mysqli_fetch_array($db->result))
	{
		$act = "";

        if($r['action'] == 'J')
        {
	        if($cRounds == 1)
           		$ply[$r['puid']] = array('K' => 0, 'D' => 0, 'T' => 0, 'S' => 0, 'TD' => 0, 'id' => $r['puid']);
        }

		if($r['action'] == "D" || $r['action'] == "K")
		{
			$a = ($r['action'] == "K") ? "get&ouml;tet" : "getroffen";
			$a.= (($rr['type'] <= 2) ? "" : (($r['team']==$r['eteam']) ? (($r['puid']==$r['euid']) ? " SUIZID " : " TEAMKILL ") : ""));

            $weap = $weapons->getLogName($r['weapon']);
            $weap = ($weap != 'none') ? $weap : $mods->getLogName($r['mods']);

            $hitstr = ($r['hitlocation'] > 1) ? $hits->getName($r['hitlocation']) : "Schock";
            $modstr = ($r['mod'] > 0) ? $mods->getName($r['mod']) : "";

			$act = $a." (".$hitstr.") von ".$players->getLogName($r['euid'])." ".$r['health']." Schaden mit Waffe ".$weap." ".$modstr;
			
			if($r['action'] == "K")
			{
				$h = checkIfThatKD($r, $rr['type']);
				
			    $ply[$r['puid']]['TD'] = $r['team'];
		        $ply[$r['euid']]['TD'] = $r['eteam'];
		
				switch($h)
				{
					case 1 :
					{
						$ply[$r['puid']]['S']+=1;
						break;
					}
					case 2 :
					{
						$ply[$r['euid']]['T']+=1;
						break;
					}
					case 3 :
					{
						$ply[$r['euid']]['K']+=1;
						$ply[$r['puid']]['D']+=1;
						break;
					}
				}
			}
		}
		else if($r['action'] == "Weapon")
		{
			$act = "wechselt zu ".$weapons->getLogName($r['weapon']);
		}
		else if($r['action'] == "J" || $r['action'] == "L" || $r['action'] == "JT")
		{
			$act = "";
		}
		else if($r['action'] == 'say' || $r['action'] == 'sayteam')
		{
			$ids = $r['additional'];
			$db2->query_db("SELECT * FROM chats WHERE id = '$ids'");
			$cs = mysql_fetch_array($db2->result);
			$act = $cs['message'];
		}	
		else
		{
			$act = $gameActions->getName($gameActions->getId($r['action']));
		}
		
		$time = secToTime($r['time']);
		
		if($r['action'] == 'InitGame:')
		{
			$time = "00:00";
		}
		
		
		$class = "";
		$icon = "";
		
		if($r['action'] == "J")
		{
			$class = "join";
			$icon = "pinch.png";
		}
		else if($r['action'] == "JT")
		{
			$class = "joinTeam";
			$icon = "team2.png";
		}
		else if($r['action'] == "L")
		{
			$class = "launch";
			$icon = "arrows7.png";		
		}
		else if($r['action'] == "Q")
		{
			$class = "launch";
			$icon = "door9.png";		
		}
		else if($r['action'] == "D")
		{
			$class = "damage";
			$icon = "damage.png";
		}
		else if($r['action'] == "K")
		{
			$class = "kill";
			$icon = "unicorn2.png";
		}
		else if($r['action'] == "Weapon")
		{
			$class = "Weapon";
			$icon = "gun9.png";
		}
		else if($r['action'] == "KC")
		{
			$icon = "mark9.png";
		}
		else if($r['action'] == "KD")
		{
			$icon = "cross-mark1.png";
		}
		else if($r['action'] == "BD" || $r['action'] == "BP" || $r['action'] == "BT"  || $r['action'] == "BL" || $r['action'] == "BPA")
		{
			$icon = "bomb13.png";
		}
		else if($r['action'] == "BL" || $r['action'] == "BDA" || $r['action'] == "BDP" || $r['action'] == "BPP")
		{
			$icon = "bomb13.png";
		}
		else if($r['action'] == "W")
		{
			$icon = "flag88.png";
		}
		else if($r['action'] == "InitGame:")
		{
			$icon = "transport305.png";
		}
		else if($r['action'] == "ExitLevel: executed")
		{
			$icon = "door9.png";
		}
		else if($r['action'] == "ShutdownGame")
		{
			$icon = "shut-down2.png";
		}
		else if($r['action'] == "FT")
		{
			$icon = "man22.png";		
		}
		else if($r['action'] == "FR")
		{
			$icon = "astronaut8.png";	
		}
		else if($r['action'] == "FC")
		{
			$icon = "castles.png";	
		}
		else if($r['action'] == "W")
		{
			$icon = "celebration.png";
		}
		else if($r['action'] == "say" || $r['action'] == "sayteam")
		{
			$icon = "audio11.png";
		}
		
		$feed.= '<div class="feed" id="feed'.$i.'">';
		$feed.= '<div><img class="icon" src="icons/'.$icon.'" /></div>';
		$feed.= '<div class="msg">'.$time.' - '.$r['action'].' - '.$players->getLogName($r['puid']).' > '.$act.'</div>';
		$feed.= '</div>';
		
		//$content.= "<tr><td>".$time."</td><td>".$r['action']."</td><td>".$r['team']."</td><td>".$players->getLogName($r['puid'])."</td><td>".$act."</td></tr>";
		
		$i++;
		
		
		
	}

	$feedAnimation = "showFeed(".$i.");";
}


?>
<html lang="de-DE">
	<head>
		<meta charset="iso-8859-1">
		<title>CoD4 Log Parser - Test</title>
		
		<link rel="stylesheet" href="interface.css" />
		<script type="text/javascript" src="jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="feed.js"></script>
		
	</head>
	
	<body>
		<p>Dient erst mal zur Kontrolle des Parsers. Wird ab dem 17.07 per Cronjob aktualisiert.</p>
		<form method="POST" action="">
			<?php
				echo buildRoundSelecltor();
			?>
		</form>
		<div>
		
			<?php
				echo $feed;
			?>
			
		</div>
		
	</body>
	
	<script type="text/javascript">
		<?php
		echo $feedAnimation;
		?>
	</script>
	
</html>