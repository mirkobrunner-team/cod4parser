<?php
date_default_timezone_set('Europe/Berlin');

include '../core/misc/serv_db.inc.php';
include '../core/misc/class.extendedArray.php';
include '../core/misc/helpers.php';
include '../core/rules/game_rules.php';
include '../core/rules/player_stat_array.php';
include '../core/stats/maps.php';
include '../core/stats/maps_drawer.php';

$weapons = new ExtendedArray();
$maps = new ExtendedArray();
$types = new ExtendedArray();
$hits = new ExtendedArray();
$players = new ExtendedArray();
$gameActions = new ExtendedArray();
$mods = new ExtendedArray();
$spawnPoints = new ExtendedArray();

$weapons->fill('weapons');
$maps->fill('maps');
$types->fill('gametypes');
$hits->fill('hitlocations');
$players->fill('aliases');
$gameActions->fill('gameactions');
$mods->fill('mods');
$spawnPoints->fill('spawnpoints');


$teams = array();


function getGroundStats()
{
	$db = new mbdb();
	$db->query_db("SELECT COUNT(id) FROM games_full");

}


function buildRoundSelecltor()
{
	global $maps, $types;

	$db = new mbdb();
	$db->query_db("SELECT * FROM games_full ORDER BY time ASC");

	$cont = "<select name='selmap' onchange='this.form.submit()'>";
	$cont.= "<option value='0'>-</option>";

	while($r = mysqli_fetch_array($db->result))
	{
		$time = date('m/d/Y H:i:s', $r['time']);

		$cont.= '<option value="'.$r['id'].'">'.$time." ".$types->getLogName($r['type'])." - ".$maps->getAditionalContentFromField('name', $r['map']).'</option>';
	}

	$cont.= "</select>";
	return $cont;
}

/*
	return:
		1 - suizide hit
		2 - team hit
		3 - hit
*/
function checkIfThatKD(&$r, $teamPlay)
{
	if($r['puid'] == $r['euid'])
	{
		return 1;
	}
	else if($r['team'] == $r['eteam'] && $teamPlay == 1)
	{
		return 2;
	}
	else
	{
		return 3;
	}
}

$ply = array();
$content = "";
$contB = "";
$contS = ""; //"<table cellpadding='0' cellspacing='0'>".'<td width="150">Spieler</td><td width="50">BP</td><td width="50">BD</td><td width="50">BT</td><td width="50">BL</td><td width="50">BPA</td><td width="50">BDA</td><td width="50">BPP</td><td width="50">BDP</td><td width="50">M BDA</td><td width="50">M BPA</td><td width="50">BombKill</td></tr>';
$count = 0;
$tmp_time = 0;
$stat = "";
$rr = array();

// für das Messern beim Legen oder Entschärfen
$sab_melee = array('id' => 0, 'team' => 0, 'time' => 0, 'opfer' => 0);
$sab_kill = array('id' => 0, 'team' => 0, 'time' => 0, 'opfer' => 0);

if(isset($_POST['selmap']))
{
	$id = intVal($_POST['selmap']);
	$db = new mbdb();
	$db2 = new mbdb();



	$db2->query_db("SELECT * FROM games_full WHERE id = '$id'");
	$rr = mysqli_fetch_array($db2->result);

	$isTeamPlay = $types->getAditionalContentFromField('team', $rr['type']);
	$nameGameType = $types->getLogName($rr['type']);

	$rnds = $rr['rounds'];
	$cRounds = 0;

	$content.= date('m/d/Y H:i:s', $rr['time'])."<br />";
	$content.= $maps->getAditionalContentFromField('name', $rr['map'])."<br />";
	$content.= $nameGameType."<br />";
	$content.= "Teamspiel:".(($isTeamPlay == 1) ? "ja" : "nein")."<br /><br />";

	$db->query_db("SELECT * FROM actions_full WHERE roundid IN($rnds) ORDER BY id ASC");

    $lastDuration = 0;
    $lRaumdTime = 0;



	while($r = mysqli_fetch_array($db->result))
	{
		$act = "";
		$action = $r['action'];

		if($action == 'ShutdownGame')
		{
			$cRounds++;
            $lRaumdTime+= $r['time'] - $lastDuration;
		}
        else
        {
            $lastDuration = $r['time'] + $lRaumdTime;
        }

        if($action == 'J')
        {
	        if($cRounds == 0)
	        {
		        $ply[$r['puid']] = buildPlayerStatsArray($r['puid']);
           	}
        }

		if($action == "D" || $action == "K")
		{
			$a = ($r['action'] == "K") ? "get&ouml;tet" : "getroffen";

			$checkIfThatKD = checkIfThatKD($r, $isTeamPlay);
			switch($checkIfThatKD)
			{
				case 1 :
				{
					$a.= " SUIZID ";
					break;
				}
				case 2 :
				{
					$a.= " TEAMKILL ";
					break;
				}
				case 3 :
				{
					break;
				}
			}


            $wep = $weapons->getAditionalContentFromField('name', $r['weapon']);
            $weap = ($weap != 'None') ? '<span class="brown">'.$wep.'</span>' : '<span class="slategray">'.$mods->getLogName($r['mods']).'</span>';

            $hitstr = ($r['hitlocation'] > 1) ? $hits->getAditionalContentFromField('name', $r['hitlocation']) : "Schock";
            $modstr = ($r['mod'] > 0) ? $mods->getName($r['mod']) : "";

			$pos = "";

			if($r['pos_x'] != 0 && $r['pos_y'] != 0 && $r['pos_z'] != 0)
			{
				$pos = " | <span class='lightgray'>".$r['pos_x']." | ".$r['pos_y']." ".$r['pos_z'].'</span>';
			}
			

			$act = $a." (".$hitstr.") von <span class='blue'>".$players->getAditionalContentFromField('hash', $r['euid'])."</span> <span class='red'>".$r['health']."</span> Schaden mit Waffe ".$weap." ".$modstr.$pos;

			if($action == "K")
			{
			    $ply[$r['puid']]['TD'] = $r['team'];
		        $ply[$r['euid']]['TD'] = $r['eteam'];

                $h = $checkIfThatKD;

				switch($checkIfThatKD)
				{
				    case 1 :
					{
                        $ply[$r['puid']]['D']+=1;
						$ply[$r['puid']]['S']+=1;
                        $ply[$r['puid']]['cToDeathStreak'] = 0;
                        $ply[$r['puid']]['cToKillStreak'] = 0;
						break;
					}
					case 2 :
					{
						$ply[$r['euid']]['T']+=1;
						$ply[$r['euid']]['isDeath'] = 0;
                        $ply[$r['euid']]['cToDeathStreak'] = 0;
                        $ply[$r['euid']]['cToKillStreak'] = 0;
                        
						break;
					}
					case 3 :
					{
						$ply[$r['euid']]['K']+=1;
						$ply[$r['euid']]['isDeath'] = 0;
                        $ply[$r['euid']]['cToDeathStreak'] = 0;
						$ply[$r['puid']]['D']+=1;
						$ply[$r['puid']]['isDeath'] = 1;
                        $ply[$r['puid']]['cToKillStreak'] = 0;
                        $ply[$r['puid']]['cToDeathStreak']+=1;

						break;
					}
				}
				

                if($ply[$r['euid']]['lST'] == 'prone')
                {
                    $ply[$r['euid']]['sSTKp']+=1;
                }
                else if($ply[$r['euid']]['lST'] == 'crouch')
                {
                    $ply[$r['euid']]['sSTKc']+=1;
                }
                else
                {
                    if($checkIfThatKD > 1)
                    	$ply[$r['euid']]['sSTKs']+=1;
                }

				if($checkIfThatKD == 2 || $checkIfThatKD == 3)
				{

					if($ply[$r['euid']]['isDeath'] == 0)
					{
						$ply[$r['euid']]['cToKillStreak']+=1;

                        $reached = "";

                        if($ply[$r['euid']]['cToKillStreak'] == 3)
                        {
                            $reached = "3er Killstreak erreicht";
                        }
                        else if($ply[$r['euid']]['cToKillStreak'] == 6)
                        {
                            $reached = "6er Killstreak erreicht";
                        }
                        else if($ply[$r['euid']]['cToKillStreak'] == 9)
                        {
                            $reached = "9er Killstreak erreicht";
                        }
                        else if($ply[$r['euid']]['cToKillStreak'] == 12)
                        {
                            $reached = "12er Killstreak erreicht";
                        }

                        if($reached <> "")
                        {
                            	$content.= "<tr><td>".$time."</td><td>Killstreak Reached</td><td>".$r['team']."</td><td>".$players->getAditionalContentFromField('hash', $r['puid'])."</td><td>".$reached."</td></tr>";
                        }
                        else
                        {
                            if($ply[$r['puid']]['cToDeathStreak'] > 2)
                            {
                                $content.= "<tr><td>".$time."</td><td>Deathstreak Reached</td><td>".$r['team']."</td><td>".$players->getAditionalContentFromField('hash', $r['puid'])."</td><td>".$ply[$r['puid']]['cToDeathStreak']."er Deathstreak erreicht</td></tr>";
                            }
                        }
					}
				}

				if($nameGameType == "sab" && $h == 3)
				{
					if($r['mod'] == 5)
					{
						$sab_melee = array('id' => $ply[$r['euid']]['id'], 'team' => $r['eteam'], 'time' => $r['time'], 'opfer' => $ply[$r['puid']]['id']);
					}

					$sab_kill = array('id' => $ply[$r['euid']]['id'], 'team' => $r['eteam'], 'time' => $r['time'], 'opfer' => $ply[$r['puid']]['id']);


					if($ply[$r['euid']]['Bomber'] == 1)
					{
						$ply[$r['euid']]['sKBomber']+=1;
					}
				}
			}
			else if($action == "D")
			{
				switch($checkIfThatKD)
				{
					case 1 :
					{
						$ply[$r['puid']]['DA']+=1;
						$ply[$r['puid']]['DS']+=$r['health'];
						break;
					}
					case 2 :
					{
						$ply[$r['euid']]['DA']+=1;
						$ply[$r['euid']]['DS']+=$r['health'];
						break;
					}
					case 3 :
					{
						$ply[$r['euid']]['DA']+=1;
						$ply[$r['euid']]['DS']+=$r['health'];
						break;
					}
				}
				
				if($checkIfThatKD > 1)
				{
					$ply[$r['euid']]['isDS'] = false;  
				}
			}
		}
		else if($action == "Weapon")
		{
			$act = "wechselt zu ".$weapons->getLogName($r['weapon']);
		}
		else if($action == "J" || $action == "L" || $action == "JT")
		{
			$act = "";
		}
		else if($action == 'say' || $action == 'sayteam')
		{
			$ids = $r['additional'];
			$db2->query_db("SELECT * FROM chats WHERE id = '$ids'");
			$cs = mysqli_fetch_array($db2->result);
			$act = $cs['message'];
		}
		else if($action == "ST" || $action == "SP")
		{
			if($action == "ST")
			{
			    $pos = "";
			    if($r[15] != 0 && $r[16] != 0 && $r[17] != 0)
                {
                    $pos = " | <span class='lightgray'>".$r[15]." ".$r[16]." ".$r[17].'</span>';
                }

				$act = $mods->getLogName($r['mod']).$pos;

				$ply[$r['puid']]['lST'] = $act;
				
				if($act == 'stand')
				{
					$ply[$r['puid']]['sSTs']+=1;
				}
				else if($act == 'prone')
				{
					$ply[$r['puid']]['sSTp']+=1;
				}
				else if($act == 'couch')
				{
					$ply[$r['puid']]['sSTc']+=1;
				}
			}
			else
			{
				//get spawnpoint
				//$tmp = $spawnPoints->getLogName($r['additional']);
				//$tmp = explode(",", $tmp);
                $pos = " | <span class='lightgray'>".$r[15]." ".$r[16]." ".$r[17].'</span>';
				//$place = $r['additional']." | ".$tmp[1]." ".$tmp[2]." ".$tmp[3];
				$act = "Spawn: ".$pos;
			}
		}
		else if($action == "AA")
		{
			$act = '<span class="red">'.$gameActions->getName($gameActions->getId($action)).'</span> <span class="brown">'.$gameActions->getName($r['additional']).'</span>';
		}
		else
		{
			$act = '<span class="green">'.$gameActions->getName($gameActions->getId($action))."</span>";
		}

		if($action == 'BP')
		{
			$ply[$r['puid']]['sBP']+=1;
			$ply[$r['puid']]['Bomber'] = 0;
		}
		else if($action == 'BD')
		{
			$ply[$r['puid']]['sBD']+=1;
		}
		else if($action == 'BL')
		{
			$ply[$r['puid']]['sBL']+=1;
			$ply[$r['puid']]['Bomber'] = 0;
		}
		else if($action == 'BT')
		{
			$ply[$r['puid']]['sBT']+=1;
			$ply[$r['puid']]['Bomber'] = 1;
		}
		else if($action == 'BDA')
		{
			$ply[$r['puid']]['sBDA']+=1;

			if($sab_melee['id'] != 0)
			{
				if($sab_melee['opfer'] == $r['puid'])
				{
					$ply[$sab_melee['id']]['sMBDA']+=1;
				}
				$sab_melee['id'] = 0;
			}

			if($sab_kill['id'] != 0)
			{
				if($sab_kill['opfer'] == $r['puid'])
				{
					$ply[$sab_kill['id']]['skBDA']+=1;
				}
				$sab_kill['id'] = 0;
			}

		}
		else if($action == 'BPA')
		{
			$ply[$r['puid']]['sBPA']+=1;
			$ply[$r['puid']]['Bomber'] = 0;

			if($sab_melee['id'] != 0)
			{
				if($sab_melee['opfer'] == $r['puid'])
				{
					$ply[$sab_melee['id']]['sMBPA']+=1;
				}
				$sab_melee['id'] = 0;
			}

			if($sab_kill['id'] != 0)
			{
				if($sab_kill['opfer'] == $r['puid'])
				{
					$ply[$sab_kill['id']]['skBPA']+=1;
				}
				$sab_kill['id'] = 0;
			}
		}
		else if($action == 'BPP')
		{
			$ply[$r['puid']]['sBPP']+=1;
		}
		else if($action == 'BDP')
		{
			$ply[$r['puid']]['sBDP']+=1;
		}
		else if($action == 'KC')
		{
			$ply[$r['puid']]['sKC']+=1;
		}
		else if($action == 'KD')
		{
			$ply[$r['puid']]['sKD']+=1;
		}
		else if($action == 'FT')
		{
			$ply[$r['puid']]['sFT']+=1;
		}
		else if($action == 'FC')
		{
			$ply[$r['puid']]['sFC']+=1;
		}
		else if($action == 'FR')
		{
			$ply[$r['puid']]['sFR']+=1;
		}
		else if($action == 'W')
		{
			$ply[$r['puid']]['W']+=1;
		}
		else if($action == "GPS")
		{
			$ply[$r['puid']]['sGPS']+=1;
		}
		else if($action == "KV")
		{
			$ply[$r['puid']]['sKV']+=1;
		}

		$time = secToTime($r['time']);
		$tmp_time = $r['time'];

		if($r['action'] == 'InitGame:')
		{
			$time = "00:00";
		}

		//Postions
		if($action != 'SP' && $action != 'ST' && $action != 'D' && $action != 'K')
		{
			$pos = "";
			if($r[15] != 0 && $r[16] != 0 && $r[17] != 0)
			{
	            $pos.= " | <span class='lightgray'>".$r[15]." ".$r[16]." ".$r[17].'</span>';
	            $act.= $pos;
	       	}
		}
		
		$content.= "<tr><td>".$time."</td><td>".$r['action']."</td><td>".$r['team']."</td><td>".$players->getAditionalContentFromField('hash', $r['puid'])."</td><td>".$act."</td></tr>";
		$count++;
	}

    $stat.= $count." Aktionen und ".$lastDuration." Sekunden gespielt<br/ >";
	$stat.= "Alle ".round($lastDuration / $count, 4)." Sekund(en) eine Aktionen im Schnitt<br />";
	$stat.= "".round($count / $lastDuration, 4)." Aktionen pro Sekunde im Schnitt<br />";








	//oki nun die spieler
    //array convertiren keys in separates feld
    $plyB = array();
    $c = count($ply);
    $keys = array_keys($ply);

    $winnerTeam = -1;
    $teamAPt = 0;
    $teamBPt = 0;

    for($i=0;$i<$c;$i++)
    {
        $plyB[$i] = $ply[$keys[$i]];
        $plyB[$i]['id'] = $keys[$i];

		if($plyB[$i]['W'] == 1)
		{
			$winnerTeam = $plyB[$i]['TD'];
		}

		if($plyB[$i]['sBP'] > 0)
		{
			if($plyB[$i]['TD'] == 0)
			{
				$teamAPt++;
			}
			else
			{
				$teamBPt++;
			}
		}

		if($plyB[$i]['sKC'] > 0)
		{
			if($plyB[$i]['TD'] == 0)
			{
				$teamAPt++;
			}
			else
			{
				$teamBPt++;
			}
		}

		if($plyB[$i]['sFC'] > 0)
		{
			if($plyB[$i]['TD'] == 0)
			{
				$teamAPt++;
			}
			else
			{
				$teamBPt++;
			}
		}
    }

    if($teamAPt>0 || $teamBPt>0)
    {
	    $winnerTeam = ($teamAPt > $teamBPt) ? 0 : 1;
    }

    $stat.= "<br />Team: ".$winnerTeam." hat gewonnen<br />";

    //sortieren
    usort($plyB, function($a, $b) {
        return $a['K'] < $b['K'];
    });

	$c = count($plyB);

    //ausgeben
	for($i=0;$i<$c;$i++)
	{
        $playname = ($plyB[$i]['id'] != 0) ? $players->getAditionalContentFromField('hash', $plyB[$i]['id']) : "Mapkill";

		$dnum = ($plyB[$i]['DA'] == 0) ? 1 : $plyB[$i]['DA'];
		$knum = ($plyB[$i]['K'] == 0) ? 1 : $plyB[$i]['K'];

		$effi = $plyB[$i]['K'] / $plyB[$i]['DA'];

		$pkt = 0;
		$won = ($plyB[$i]['TD'] == $winnerTeam) ? 1 : 0;

		if($nameGameType == "sab")
		{
			if($contS == "")
				$contS = "<table cellpadding='0' cellspacing='0'>".'<td width="150">Spieler</td><td width="50">BP</td><td width="50">BD</td><td width="50">BT</td><td width="50">BL</td><td width="50">BPA</td><td width="50">BDA</td><td width="50">BPP</td><td width="50">BDP</td><td width="50">M BDA</td><td width="50">M BPA</td><td width="50">BombKill</td></tr>';

			$pkt = computePoints_sab2($plyB[$i]['K'], $plyB[$i]['D'], $plyB[$i]['sBD'], $plyB[$i]['sBP'], $plyB[$i]['sBDP'], $plyB[$i]['sBPP'], $plyB[$i]['skBDA'], $plyB[$i]['skBPA'], $plyB[$i]['sMBPA'], $plyB[$i]['sMBDA'], $plyB[$i]['sKBomber'], $plyB[$i], $won=0);

			$contS.= "<tr><td>".$playname."</td><td>".$plyB[$i]['sBP']."</td><td>".$plyB[$i]['sBD']."</td><td>".$plyB[$i]['sBT']."</td><td>".$plyB[$i]['sBL']."</td><td>".$plyB[$i]['sBPA']."</td><td>".$plyB[$i]['sBDA']."</td><td>".$plyB[$i]['sBPP']."</td><td>".$plyB[$i]['sBDP']."</td><td>".$plyB[$i]['sMBDA']."</td><td>".$plyB[$i]['sMBPA']."</td><td>".$plyB[$i]['sKBomber']."</td></tr>";
		}
		else if($nameGameType == "kc")
		{
			//kc
			if($contS == "")
				$contS = '<table cellpadding="0" cellspacing="0"><td width="150">Spieler</td><td width="50">KC</td><td width="50">KD</td></tr>';
			
            $pkt = computePoints_kc($plyB[$i]['K'], $plyB[$i]['D'], $plyB[$i]['sKC'], $plyB[$i]['sKD'], $won);

			$contS.= "<tr><td>".$playname."</td><td>".$plyB[$i]['sKC']."</td><td>".$plyB[$i]['sKD']."</td></tr>";
		}
		else if($nameGameType == "ctf")
		{
			//ctf
			if($contS == "")
				$contS = '<table cellpadding="0" cellspacing="0"><td width="150">Spieler</td><td width="50">FT</td><td width="50">FR</td><td width="50">FC</td></tr>';
			
			$pkt = computePoints_TDM($plyB[$i]['K'], $plyB[$i]['D'], $won);
			$contS.= "<tr><td>".$playname."</td><td>".$plyB[$i]['sFC']."</td><td>".$plyB[$i]['sFR']."</td><td>".$plyB[$i]['sFC']."</td><td>FT</td><td>FR</td><td>FC</td><td></td></tr>";
		}
		else if($nameGameType == "dom")
		{
			if($contS == "")
				$contS = '<table cellpadding="0" cellspacing="0"><td width="150">Spieler</td><td width="50">FC</td></tr>';
			
			$contS.= "<tr><td>".$playname."</td><td>".$plyB[$i]['sFC']."</td></tr>";			
		
		}
		else if($nameGameType == "grd")
		{
			if($contS == "")
				$contS = '<table cellpadding="0" cellspacing="0"><td width="150">Spieler</td><td width="50">GPS</td></tr>';
			
			$contS.= "<tr><td>".$playname."</td><td>".$plyB[$i]['sGPS']."</td></tr>";
		}
		else if($nameGameType == "ass")
		{
			if($contS == "")
				$contS = '<table cellpadding="0" cellspacing="0"><td width="150">Spieler</td><td width="50">KV</td></tr>';
			
			$contS.= "<tr><td>".$playname."</td><td>".$plyB[$i]['sKV']."</td></tr>";
		
		}
		else if($nameGameType == "bel")
		{
			//keine gesonderten Logeinträge
		}
		else if($nameGameType == "oitc")
		{
			//keine gesonderten Logeinträge
		}
		else if($nameGameType == "ss")
		{
			//keine gesonderten Logeinträge
		}
		else if($nameGameType == "ta")
		{
			// TA Eintrag aber nur manchmal ansonsten nichts
		}
		else
		{
			if($isTeamPlay == 1)
			{
				$pkt = computePoints_TDM($plyB[$i]['K'], $plyB[$i]['D'], $won);
			}
			else
			{
				$pkt = computePoints_DM($plyB[$i]['K'], $plyB[$i]['D']);
			}
		}


		$contB.= "<tr><td>".$plyB[$i]['TD']."</td><td>".$playname."</td><td>".$plyB[$i]['K']."</td><td>".$plyB[$i]['T']."</td><td>".$plyB[$i]['S']."</td><td>".$plyB[$i]['D']."</td><td>".$plyB[$i]['DA']."</td><td>".$plyB[$i]['DS']."</td><td>".round($effi, 4)."</td><td>".round($pkt, 4)."</td></tr>";
	}

	$contS.="</table>";

    $image ="<br /><br /><br /><br />";
    $image.= printSpawnPoints_new_($rnds);

}


?>
<html lang="de-DE">
	<head>
		<meta charset="iso-8859-1">
		<title>CoD4 Log Parser - Test</title>

		<link rel="stylesheet" href="res/css/interface.css" >

	</head>

	<body>
		<nav role="main">
			<a href="http://k4f-in-berlin.de">K4F Home</a>&nbsp;<a href="server.php">Server Stats</a>&nbsp;<a href="index.php">Runden Stats</a>&nbsp;<a href="player.php">Spieler Stats</a>&nbsp;<a href="gametypes.php">Spielarten Stats</a>&nbsp;<a href="maps.php">Maps Stats</a>&nbsp;<a href="weapons.php">Waffen Stats</a>&nbsp;<a href="game.php">Koord Tests</a>&nbsp;<a href="challenges.php">Herausforderungen</a>
		</nav>
		<p>Dient erst mal zur Kontrolle des Parsers.</p>
		<form method="POST" action="">
			<?php
				echo buildRoundSelecltor();
			?>
		</form>

		<div>
			<a href="feed.php">Zeige Spiele als Feed (like Twitter)</a>
		</div>
		<div>
			<table width="" cellpadding="0" cellspacing="0" class="roundtable">
				<tr>
					<td width="50">Zeit</td>
					<td width="150">Aktion</td>
					<td width="50">Team</td>
					<td width="180">Spieler</td>
					<td>Action</td>
				</tr>
			<?php
				echo $content;
			?>

			</table>
			<br />

			<p>
				<?php echo $stat; ?>
			</p>

			<table width="" cellpadding="0" cellspacing="0">
				<tr>
                    <td width="50">Team</td>
					<td width="220">Spieler</td>
					<td width="50">Kills</td>
					<td width="50">Teamkill</td>
					<td width="50">Suizid</td>
					<td width="50">Death</td>
					<td width="70">Damage</td>
					<td width="100">Damage Sum</td>
					<td width="100">Effi (Damage / Kill)</td>
					<td width="100">Punkte</td>
				</tr>
				<?php
					echo $contB;
				?>
			</table>

			<br />
			<?php
				echo $contS;
				
				echo "<br /><br />";
			?>
			
			<?php
				
				echo buildTable($ply);
				
			?>
			
			<?php
			    echo $image;
				echo "<br /><br />".memory_get_peak_usage()." peak Mem | ".memory_get_usage()." norm Mem usage (bytes)<br />";
			?>
		</div>

	</body>
</html>