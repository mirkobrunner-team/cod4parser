<?php
date_default_timezone_set('Europe/Berlin');

include('/var/customers/webs/Mirko/verwackeltes/test/cod4parser/core/misc/serv_db.inc.php');
include('/var/customers/webs/Mirko/verwackeltes/test/cod4parser/core/misc/helpers.php');
include '../core/stats/weapons.php';

$db = new mbdb();

$t = array();

$content = "";
$weapon_id = -1;
$type_id = -1;


if(isset($_POST['weapons']) && $_POST['weapons'] > 0)
{

}
else if(isset($_POST['weapons_type']) && $_POST['weapons_type'] > 0)
{

}
else
{
	$content = "Treffer nach Waffe<br />";
	$content.= $db->show_in_table(getCountedListHitsPerWeapon());
	$content.= "<br /><br />";
	$content.= "Absch&uuml;sse nach Waffe<br />";
	$content.= $db->show_in_table(getCountedListKillsPerWeapon());
	$content.= "<br /><br />";
	$content.= "Schaden und Treffer nach Waffe<br />";
	$content.= $db->show_in_table(getCountedListDamagePerWeapon());
	$content.= "<br /><br />";
	$content.= "Treffer nach Waffe gruppiert<br />";
	$content.= $db->show_in_table(getCountedListHitsPerWeaponByType());
	$content.= "<br /><br />";
	$content.= "Absch&uuml;sse nach Waffe gruppiert<br />";
	$content.= $db->show_in_table(getCountedListKillsPerWeaponByType());
	$content.= "<br /><br />";
	$content.= "Schaden und Treffer nach Waffe gruppiert<br />";
	$content.= $db->show_in_table(getCountedListDamagePerWeaponByType());
	$content.= "<br /><br />";
	$content.= "Effi(?)<br />";
	$content.= "effi: Anzahl Kill / Anzahl Damage<br />";
	$content.= "damage_needed_per_kill: Summe Damage / Kill<br />";
	$content.= $db->show_in_table(getKillDamageRationPerWeapon());
	$content.= "<br /><br />";
	$content.= "Effi(?) per Typ<br />";
	$content.= "effi: Anzahl Kill / Anzahl Damage<br />";
	$content.= "damage_needed_per_kill: Summe Damage / Kill<br />";
	$content.= $db->show_in_table(getKillDamageRationPerWeaponByType());
	$content.= "<br /><br />";
	
	
}

?>
<html lang="de-DE">
	<head>
		<meta charset="iso-8859-1">
		<title>CoD4 Log Parser - Waffen</title>

		<link rel="stylesheet" href="res/css/interface.css" >

	</head>
	<body>
		<nav role="main">
			<a href="http://k4f-in-berlin.de">K4F Home</a>&nbsp;<a href="server.php">Server Stats</a>&nbsp;<a href="index.php">Runden Stats</a>&nbsp;<a href="player.php">Spieler Stats</a>&nbsp;<a href="gametypes.php">Spielarten Stats</a>&nbsp;<a href="maps.php">Maps Stats</a>&nbsp;<a href="weapons.php">Waffen Stats</a>&nbsp;<a href="game.php">Koord Tests</a>&nbsp;<a href="challenges.php">Herausforderungen</a>
		</nav>
		<p>Test Gametypes Statistiken</p>
		<form method="POST" action="">
			<?php
				echo buildWeaponSelector($weapon_id);
			?>
			<br />
			<?php
				echo buildWeaponTypeSelector($type_id);
			?>
		</form>
		
		
		
		<div style="margin-top: 50px;">
			
			<?php echo $content; ?>
			
			<?php
			
				echo "<br /><br />".memory_get_peak_usage()." peak Mem | ".memory_get_usage()." norm Mem usage (bytes)<br />";
			?>
		</div>
		
	</body>
</html>
	
	