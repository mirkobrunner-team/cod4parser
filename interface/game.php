<?php

include '../core/misc/serv_db.inc.php';
include '../core/misc/class.extendedArray.php';
include '../core/misc/helpers.php';
include '../core/stats/maps_drawer.php';
include '../core/stats/games.php';


$weapons = new ExtendedArray();
$maps = new ExtendedArray();
$types = new ExtendedArray();
$hits = new ExtendedArray();
$players = new ExtendedArray();
$gameActions = new ExtendedArray();
$mods = new ExtendedArray();
$spawnPoints = new ExtendedArray();

$weapons->fill('weapons');
$maps->fill('maps');
$types->fill('gametypes');
$hits->fill('hitlocations');
$players->fill('aliases');
$gameActions->fill('gameactions');
$mods->fill('mods');
$spawnPoints->fill('spawnpoints');

function buildPlayerSelector($player_id, $game_id)
{
	if(isset($game_id))
	{
		$db = new mbdb();
		
		$db->query_db("SELECT DISTINCT(teams.playerid) as tid, aliases.hash as hash FROM teams, aliases WHERE gameid = '$game_id' AND aliases.id = teams.playerid ORDER BY aliases.hash ASC");
	
		$sel = ($player_id == -1) ? "selected" : "";
	
		$select = '<select name="player" id="player">';
		$select.= '<option value="-1" '.$sel.'>Alle</option>';
		while($arr = mysqli_fetch_array($db->result))
		{
			$sel = ($player_id == $arr['tid']) ? 'selected' : '';
			$select.= '<option value="'.$arr['tid'].'" '.$sel.'>'.$arr['hash'].'</option>';
		}

		$select.= '</select>';	
	}
	else
	{
		$db = new mbdb();
		$db->query_db("SELECT * FROM aliases ORDER BY hash ASC");
	
		$select = '<select name="player" id="player">';
		
		$sel = ($player_id == -1) ? "selected" : "";
		
		$select.= '<option value="-1" '.$sel.'>Alle</option>';
		while($arr = mysqli_fetch_array($db->result))
		{
			$sel = ($player_id == $arr['id']) ? 'selected' : '';
			$select.= '<option value="'.$arr['id'].'" '.$sel.'>'.$arr['hash'].'</option>';
		}

		$select.= '</select>';
	
	}
	
	return $select;
}


$t = array();

$content = "";
$map_id = -1;
$id = -1;
$player_id = -1;
$selmap  = null;
$showKills = 0;
$showTeam = -1;

if(isset($_POST['selmap']) && (is_numeric($_POST['selmap'])== true) && ($_POST['selmap']>0))
{


	$id = intVal($_POST['selmap']);
	$db = new mbdb();

    $player_id = $_POST['player'];
	$selmap = $_POST['selmap'];
    $showKills = $_POST['showkills'];
    $showTeam = $_POST['team'];

	$db->query_db("SELECT * FROM games_full WHERE id =".$id);
	$rr = mysqli_fetch_array($db->result);
	$rnds = $rr['rounds'];

    $map_id  = $rr['map'];
	$mapPath = getMapPath($maps->getLogName($rr['map']));


	$sql = "SELECT * FROM maps_config WHERE mapid = '$map_id'";
	$config = $db->query_assoc($sql);

	if($rr['map'] == 75)
	{
		$mapPath = "../maps/compass_map_mp_k4f_test.jpg";
	}
		
	if($config == null)
	{
		$factor = 1;
		$flip = 1;
		$angle = 0;
		$pos_x = 0;
		$pos_y = 0;
	}
	else
	{
		$factor = $config[0]['factor'];
		$flip = $config[0]['flip_x'];
		$angle = $config[0]['angle'];
		$pos_x = $config[0]['pos_x'];
		$pos_y = $config[0]['pos_y'];
	}
	
	printSpawnPoints_new_($rnds, true, $flip, $factor, $angle);
}

?>
<html lang="de-DE">
	<head>
		<meta charset="iso-8859-1">
		<title>CoD4 Log Parser - Game</title>

			
		<link rel="stylesheet" href="res/css/interface.css" >
		<link rel="stylesheet" href="res/css/player.css" >
		<link rel="stylesheet" href="../interface/res/css/maps.css" >
		
		<script type="text/javascript" src="res/js/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="res/js/heatmap.js"></script>
	</head>
	<body>


		<nav role="main">
			<a href="http://k4f-in-berlin.de">K4F Home</a>&nbsp;<a href="server.php">Server Stats</a>&nbsp;<a href="index.php">Runden Stats</a>&nbsp;<a href="player.php">Spieler Stats</a>&nbsp;<a href="gametypes.php">Spielarten Stats</a>&nbsp;<a href="maps.php">Maps Stats</a>&nbsp;<a href="weapons.php">Waffen Stats</a>&nbsp;<a href="game.php">Koord Tests</a>&nbsp;<a href="challenges.php">Herausforderungen</a>
		</nav>
		<p>Test Maps Statistiken</p>
		
		<form method="POST" action="">
			<?php
				echo buildGameSelector($id);
			?>
			<p>Multiplikator f&uuml;r Intervall (1000 == realtime oder 100 f&uuml;r zehn mal schneller)</p> 
 			<input type="number" name="timer_mult" value="<?php echo (isset($selmap)) ? $_POST['timer_mult'] : "1000" ?>" id="time_mult">
 			<p>Zeige Pfade f&uuml;r Spieler (! ERST NACH MAPWAHL &Auml;NDERN !)</p>
                <?php
                    echo buildPlayerSelector($player_id, $selmap);
                ?>
 			<p>Zeige meine Absch&uuml;sse oder vom wem ich getroffen wurde</p>
 			<select name="showkills" id="showkills">
	 			<option value="0" <?php echo ($showKills==0) ? "selected" : ""; ?>>-</option>
	 			<option value="1" <?php echo ($showKills==1) ? "selected" : ""; ?>>Ich</option>
	 			<option value="2" <?php echo ($showKills==2) ? "selected" : ""; ?>>Andere</option>
 			</select>
            <p>Zeige Team</p>
            <select name="team" id="team">
                <option value="-1" <?php echo ($showTeam==-1) ? "selected" : ""; ?>>-</option>
                <option value="0" <?php echo ($showTeam==0) ? "selected" : ""; ?>>Alies</option>
	 			<option value="1" <?php echo ($showTeam==1) ? "selected" : ""; ?>>Axes</option>
            </select>
 			<p>Werte k&ouml;nnen w&auml;hrend der Animation ge&auml;ndert werden. Enter nicht dr&uuml;cken.</p>
 			
 			
 			
 			<input type="hidden" id="pos_x" value="<?php echo $config[0]['pos_x']; ?>" />
 			
 			<input type="hidden" id="pos_y" value="<?php echo $config[0]['pos_y']; ?>" />
 			
		</form>
		
		

		

		<div style="margin-top: 50px;">

			<?php echo $content; ?>
			<p>Auf Punkt klicken f&uuml;r Info:</p>
			
			<div id="info">&nbsp;</div>
			
			<div id="canvas_wrapper">
				<img src="<?php echo $mapPath;?>" width="524" height="524" border="0" id="map_image" >
				<canvas id="map" width="524" height="524" style="border:solid 1px black"></canvas>
				<div id="heatmap" style=""></div>
                <!-- <img id="heapmap" src="../maps/<?php echo renderHeapMap($map_id, 10, "/var/customers/webs/Mirko/verwackeltes/test/cod4parser/maps/"); ?>" width="524" height="524" border="0" /> -->
			</div>

			<div class="logger" id="logger"></div>
			
			<br />

			<canvas id="control" width="524" height="30" style="border:solid 1px black"></canvas>
			
			<br />
			
			<div class="startstop">
				<span onclick="javascript:restart();">Restart</span><span onclick="javascript:pause();">Pause</span><span onclick="javascript:start();">Play</span>
			</div>
			<div class="startstop" id="other_controls">
				<span onclick="javascript:switchElem('#heatmap');">Heatmap</span>
			</div>

			<div class="clearfix"></div>
			
			<div id="actions"></div>
	
			
	
			<ul>
				<li>Kreis: Spawnpunkt</li>
				<li>Dunkelgr&uuml;n: Haltung ge&auml;ndert</li>
				<li>Gr&uuml;n: Schaden verursacht (Linie Schusslinie)</li>
				<li>Blau: Kill verursacht (Lini Schusslinie)</li>
				<li>Grau: Bewegungspfad des gew&auml;ten Spielers</li>
			</ul>	
			<ul>
				<li>Restart / Pause</li>
				<li>Bombenpl&auml;tze / Flaggen / HQ´s</li>
			</ul>

			<?php
				echo "<br /><br />".memory_get_peak_usage()." peak Mem | ".memory_get_usage()." norm Mem usage (bytes)<br />";

				//echo $content;
			?>
		</div>


		<script type="text/javascript">


			var data = <?php ($selmap != null) ? buildJSDataArray($data).";" : 'new Array();'; ?>;
			var maprect = <?php ($selmap != null) ? buildJSDataArray($mapRect).";" : 'new Array();'; ?>;
			var playerNames = <?php ($selmap != null) ? buildJSPlayerArray($selmap).";" : "new Array();"; ?>;

			var count = data.length;
            console.log('count: '+count);

			//place the maprect (canvas) in the center above the img; 
		    ty = ($('#pos_x').val() * 1);
			tx = ($('#pos_y').val() * 1);

			$('#map').attr('width', maprect['x2']);
			$('#map').attr('height', maprect['y2']);
			$('#map').css({'top': (ty+524)+'px' , 'margin-left':tx+'px', 'width': maprect['x2']+'px', 'height' : maprect['y2']+'px'});
			

			var heatMap = <?php ($selmap != null) ? buildJSDataArray(computeHeatMap($map_id, 5, 1, 1)).";" : "new Array();"; ?>;
			
			$('#heatmap').attr('width', maprect['x2']);
			$('#heatmap').attr('height', maprect['y2']);
			$('#heatmap').css({'top': (ty)+'px' , 'margin-left':tx+'px', 'width': maprect['x2']+'px', 'height' : maprect['y2']+'px'});
			
			var config = {
			  container: document.getElementById('heatmap'),
			  radius: 10,
			  maxOpacity: .5,
			  minOpacity: 0,
			  blur: .75
			};
			
			var heatmap = h337.create(config);
			
			heatmap.setData(
				{
					min:0,
					max: heatMap['max'],
					data: heatMap['data'],
				});
			
			heatmap.repaint();



			
		</script>
		
		<script type="text/javascript" src="res/js/mapdrawer.js"></script>


	</body>
</html>
	