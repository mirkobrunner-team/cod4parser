
function showFeed(countFeed)
{
	var actual = 0;
	var duration = 600;
	
	show();
	function show()
	{
		$('#feed'+actual).css({'display': 'block'});
		
		$('html, body').animate({
        	scrollTop: $('#feed'+actual).offset().top
    	}, duration);
		
		$('#feed'+actual).fadeTo(duration, 1, function()
		{
			actual++;
			if(actual < countFeed)
			{
				setTimeout(show, duration);
			}	
		});		
	}
}

function triggerFeed(countFeed)
{
	var actual = 1;
	var duration = 600;
	show();
	function show()
	{
		console.log(actual);
		$('#feed'+actual).css({'display': 'block'});

		$('#feed'+actual).fadeTo(duration, 1, function()
		{
			actual++;
			if(actual < countFeed)
			{
				setTimeout(show, duration);
			}
		});	
		
		$('#feedResult').css({'display': 'block'});
		$('#feedResult').fadeTo(duration, 1);	
	}
}