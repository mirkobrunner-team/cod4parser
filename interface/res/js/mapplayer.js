
var Vector = function()
{
    this.x = 0;
    this.y = 0;
    this.z = 0;
};

Vector.prototype.set = function(nx, ny, nz)
{
	this.x = nx;
    this.y = ny;
    this.z = nz;
}




var MBMapPlayer = function()
{
	this.canvas = null;
	this.width = 512;
	this.height = 20;
	
	this.timeSteps = -1;
	this.currentStep = -1;
	this.duration = 1;
	
	this.borderColor = "#000";
	this.backgroundColor = "#efefef";
	this.fillColor = "#3423ef";
	this.slider = "#56457e";
	
	this.position = new Vector();
	this.lastMousePosition = null;
	
    this.ctx = null;

    this.alert = true;
};

MBMapPlayer.prototype.buildControl = function(divId)
{

    this.canvas = document.createElement("canvas");
    this.canvas.setAttribute("id", "mbmapplayer");
    this.canvas.setAttribute("width", this.width + 1+"px");
    this.canvas.setAttribute("height", this.height + 1+"px");
    this.canvas.setAttribute("title", "Map Player");

    document.getElementById(divId).appendChild(this.canvas);

    //get the position from our canvas element
    //we need this later for mouse position calculations
    var rect = this.canvas.getBoundingClientRect();
    //this.position;
    this.position.set(rect.left, rect.top, 0);

    this.bindMouseEvents();

	this.drawPlayer(0);
};



MBMapPlayer.prototype.drawPlayer = function(cureentPosition)
{
    //we have a context yet?
    if(this.ctx == null)
    {
        this.ctx = this.canvas.getContext("2d");
    }

    //we do the fast way ;)
    var cx = this.ctx;

    //the border
    cx.beginPath();
	cx.strokeStyle = this.borderColor;
	cx.rect(0, 0, this.width, this.height);
	cx.closePath();
	cx.stroke();

    //push the context reference pointer back to property
    //because some browsers (e.g. IE) make copies instat of referrencing
    this.ctx = cx;


    //cx.beginnPath();
};


MBMapPlayer.prototype.bindMouseEvents = function(e)
{
    this.canvas.addEventListener("mousedown", this.mouseDown, false);
};

MBMapPlayer.prototype.mouseDown = function(e)
{
    if(MBMapPlayer.lastMousePosition == null)
    {
        //bindMouseEvents();
        MBMapPlayer.lastMousePosition = new Vector();
    }

	console.log(this.position);

    MBMapPlayer.lastMousePosition.set(e.pageX - MBMapPlayer.position.x, e.pageY - MBMapPlayer.position.y, 0.0);
	
	console.log(this.lastMousePosition);

    return MBMapPlayer.lastMousePosition;
};

MBMapPlayer.prototype.valueToPostionX = function(value)
{

};


MBMapPlayer.prototype.reset = function()
{
    this.canvas = null;
    this.width = 512;
    this.height = 10;

    this.timeSteps = -1;
    this.currentStep = -1;
    this.duration = 1;

    this.borderColor = '#000';
    this.backgroundColor = '#efefef';
    this.fillColor = '3423ef';
    this.slider = '#56457e';

    this.position = null;

    this.ctx = null;

    this.valueMax = null;
    this.valueMin = null;
    this.mul = null;

    this.alert = true;
};

MBMapPlayer.prototype.log = function(msg)
{
    if(this.alert == true) alert(msg);
    console.log(msg);
};






