<?php
date_default_timezone_set('Europe/Berlin');

include('../core/misc/serv_db.inc.php');
include('../core/misc/helpers.php');
include '../core/stats/players.php';
include '../core/stats/challenges.php';

$starttime = microtime_float();

$db = new mbdb();

$t = array();

$content = "";
$player_id = -1;





if(isset($_POST['player']))
{
	$player_id = $_POST['player'];


    $content.= $d."<br />";
	
	$content.= '<h2>Waffenmeister und Scharfsch&uuml;tze</h2>';
	$content.= 'Meistersch&uuml;tze Pistole:<br />';
	$content.= $db->show_in_table(processMasterShooter($player_id, 6));
	$content.= '<br /><br />';
	$content.= 'Meistersch&uuml;tze Maschinenpistole:<br />';
	$content.= $db->show_in_table(processMasterShooter($player_id, 3));
	$content.= '<br /><br />';
	$content.= 'Meistersch&uuml;tze Sturmgewehr:<br />';
	$content.= $db->show_in_table(processMasterShooter($player_id, 1));
	$content.= '<br /><br />';
	$content.= 'Meistersch&uuml;tze Maschinengewehr:<br />';
	$content.= $db->show_in_table(processMasterShooter($player_id, 5));
	$content.= '<br /><br />';
	$content.= 'Meistersch&uuml;tze Schrottflinte:<br />';
	$content.= $db->show_in_table(processMasterShooter($player_id, 2));
	$content.= '<br /><br />';
	$content.= 'Meistersch&uuml;tze Scharfsch&uuml;tzengewehr:<br />';
	$content.= $db->show_in_table(processMasterShooter($player_id, 4));
	$content.= '<br /><br />';
	$content.= 'Absch&uuml;sse mit Claymore:<br />';
	$content.= $db->show_in_table(processExtraWeaponCount($player_id, 4, 5));
	$content.= '<br /><br />';
	$content.= 'Absch&uuml;sse mit Frag Granate:<br />';
	$content.= $db->show_in_table(processExtraWeaponCount($player_id, 28, 10));
	$content.= '<br /><br />';
	$content.= 'Kills mit Messer:<br />';
	$content.= $db->show_in_table(processExtraModCount($player_id, 9, 15));
	$content.= '<br /><br />';
	
	
	$content.= '<h2>Operationen</h2>';
	$content.= 'Frei f&uumlr alle Sieger<br />';
	$content.= $db->show_in_table(processGameWinner($player_id, 2, 10));
	$content.= '<br /><br />';
	$content.= 'Teamspieler<br />';
	$content.= $db->show_in_table(processTeamGameWinner($player_id, 30));
	$content.= '<br /><br />';
	$content.= 'Suchen und Zerst&ouml;ren<br />';
	$content.= $db->show_in_table(processGameWinner($player_id, 16, 10));
	$content.= '<br /><br />';
	$content.= 'Hardcore-Teamspieler (war)<br />';
	$content.= $db->show_in_table(processGameWinner($player_id, 1, 15));
	$content.= '<br /><br />';
	$content.= 'Sabotage Sieger<br />';
	$content.= $db->show_in_table(processGameWinner($player_id, 7, 30));
	$content.= '<br /><br />';
	
	
	$content.= '<h2>Trainingslager Herausforderungen</h2>';
	$content.= 'Absch&uuml;sse nach Position:<br />';
	$content.= $db->show_in_table(processMasterStandingShooter($player_id));
	$content.= '<br /><br />';
	$content.= 'Marathon 26 (Miles):<br />';
	$content.= (processWalkingDistanceTotal($player_id) * 0.621371192)." miles";
	$content.= '<br /><br />';

	$content.= 'Bomber Marathon 26 (Miles):<br />';
	$content.= (processWalkingDistanceWithBomb_sab($player_id) * 0.621371192)." miles";
	$content.= '<br /><br />';
	

	$content.= '<h2>Killer Herausfoerderungen</h2>';
	$content.= 'Volltreffer Strumgewehr:<br />';
	$content.= $db->show_in_table(processMasterHeadShooter($player_id, 1));
	$content.= '<br /><br />';
	$content.= 'Volltreffer Maschinenpistole:<br />';
	$content.= $db->show_in_table(processMasterHeadShooter($player_id, 3));
	$content.= '<br /><br />';
	$content.= 'Volltreffer Maschinengewehr:<br />';
	$content.= $db->show_in_table(processMasterHeadShooter($player_id, 5));
	$content.= '<br /><br />';
	$content.= 'Absch&uuml;sse mit Airstrike:<br />';
	$content.= $db->show_in_table(processExtraWeaponCount($player_id, 22, 30));
	$content.= '<br /><br />';
	$content.= 'Absch&uuml;sse mit Hubschrauber:<br />';
	$content.= $db->show_in_table(processExtraWeaponCount($player_id, 71, 30));
	$content.= '<br /><br />';
	$content.= 'Multikill mit Raketenwerder:<br />';
	$content.= $db->show_in_table(processMultiWeaponCount($player_id, 33, 2));
	$content.= '<br /><br />';
	$content.= 'Multikill mit Granate:<br />';
	$content.= $db->show_in_table(processMultiWeaponCount($player_id, 28, 2));
	$content.= '<br /><br />';
	$content.= 'Multikill mit Claymore:<br />';
	$content.= $db->show_in_table(processMultiWeaponCount($player_id, 4, 2));
	$content.= '<br /><br />';
	$content.= 'Kills mit Silencer:<br />';
	$content.= $db->show_in_table(processSilencerKills($player_id, 50));
	
	
	
	$content.= '<h2>Dem&uuml;tigung</h2>';
	$content.= 'Teppichbombe<br />';
	$content.= $db->show_in_table(processMultiAirstrikeCount($player_id, 22, 5));
	$content.= '<br /><br />';
	$content.= 'Multi C4:<br />';
	$content.= $db->show_in_table(processMultiWeaponCount($player_id, 78, 2));
	$content.= '<br /><br />';

	
}
else
{

}

?>
<html lang="de-DE">
	<head>
		<meta charset="iso-8859-1">
		<title>CoD4 Log Parser - Challlenges</title>

		<link rel="stylesheet" href="res/css/interface.css" >

	</head>
	<body>
		<nav role="main">
			<a href="http://k4f-in-berlin.de">K4F Home</a>&nbsp;<a href="server.php">Server Stats</a>&nbsp;<a href="index.php">Runden Stats</a>&nbsp;<a href="player.php">Spieler Stats</a>&nbsp;<a href="gametypes.php">Spielarten Stats</a>&nbsp;<a href="maps.php">Maps Stats</a>&nbsp;<a href="weapons.php">Waffen Stats</a>&nbsp;<a href="game.php">Koord Tests</a>&nbsp;<a href="challenges.php">Herausforderungen</a>
		</nav>
		<p>Test Player Statistiken</p>
		<form method="POST" action="">
			<?php
				echo buildPlayerSelector($player_id);
			?>
		</form>



		<div style="margin-top: 50px;">

			<?php echo $content; ?>

			<?php

				echo "<br /><br />".memory_get_peak_usage()." peak Mem | ".memory_get_usage()." norm Mem usage (bytes)<br />";
                echo "<br /><br />Gebrauchte Zeit: ".(microtime_float() - $starttime)." Sekunden";
			?>
		</div>

	</body>
</html>
