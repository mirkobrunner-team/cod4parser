<?php
date_default_timezone_set('Europe/Berlin');

require_once '../core/misc/serv_db.inc.php';
require_once '../core/misc/class.extendedArray.php';
require_once '../core/misc/helpers.php';
require_once '../core/rules/game_rules.php';
require_once '../core/rules/player_stat_array.php';

$weapons = new ExtendedArray();
$maps = new ExtendedArray();
$types = new ExtendedArray();
$hits = new ExtendedArray();
$players = new ExtendedArray();
$gameActions = new ExtendedArray();
$mods = new ExtendedArray();

$weapons->fill('weapons');
$maps->fill('maps');
$types->fill('gametypes');
$hits->fill('hitlocations');
$players->fill('aliases');
$gameActions->fill('gameactions');
$mods->fill('mods');



function buildRoundSelecltor()
{
	global $maps, $types;
	
	$db = new mbdb();
	$db->query_db("SELECT * FROM games_full ORDER BY time ASC");

	$cont = "<select name='selmap' onchange='this.form.submit()'>";
	$cont.= "<option value='0'>-</option>";
	
	while($r = mysqli_fetch_array($db->result))
	{
		$time = date('m/d/Y H:i:s', $r['time']);
		
		$cont.= '<option value="'.$r['id'].'">'.$time." ".$types->getLogName($r['type'])." ".$maps->getLogName($r['map']).'</option>';
	}
	
	$cont.= "</select>";
	return $cont;
}

/*
	return:
		1 - suizide hit
		2 - team hit
		3 - hit
*/
function checkIfThatKD(&$r, $type)
{
	if($r['puid'] == $r['euid'])
	{
		return 1;
	}
	else if(($r['team'] == $r['eteam']) && ($type > 2))
	{
		return 2;
	}
	else
	{
		return 3;
	}
}

$ply = array();
$content = "";
$contB = "";
$feed = "";
$i = 0;
$count = 0;

$arr = array();

if(isset($_POST['selmap']))
{	
	$id = intVal($_POST['selmap']);
	$db = new mbdb();
	$db2 = new mbdb();
	
	
	
	$db2->query_db("SELECT * FROM games_full WHERE id = '$id'");
	$rr = mysql_fetch_array($db2->result);
	
	
	
	$rnds = $rr['rounds'];
	$cRounds = explode(",", $rnds);
	
	$content.= date('m/d/Y H:i:s', $rr['time'])."<br />";
	$content.= $maps->getLogName($rr['map'])."<br />";
	$content.= $types->getLogName($rr['type'])."<br /><br />";
	
	$db->query_db("SELECT * FROM actions_full WHERE roundid IN($rnds) ORDER BY id DESC");
	
	$i = $db->nrows;
	$count = $i;
	
	while($r = mysqli_fetch_array($db->result))
	{
		$act = "";

        if($r['action'] == 'J')
        {
	        if($cRounds == 1)
           		$ply[$r['puid']] = array('K' => 0, 'D' => 0, 'T' => 0, 'S' => 0, 'TD' => 0, 'id' => $r['puid']);
        }
        
        $euidName = $players->getAditionalContentFromField('hash', $r['euid']);
        $puidName = $players->getAditionalContentFromField('hash', $r['puid']);
        
		if($r['action'] == "D" || $r['action'] == "K")
		{
			$a = ($r['action'] == "K") ? "get&ouml;tet" : "getroffen";
			$a.= (($rr['type'] <= 2) ? "" : (($r['team']==$r['eteam']) ? (($r['puid']==$r['euid']) ? " SUIZID " : " TEAMKILL ") : ""));

            $weap = $weapons->getLogName($r['weapon']);
            $weap = ($weap != 'none') ? $weap : $mods->getLogName($r['mods']);

            $hitstr = ($r['hitlocation'] > 1) ? $hits->getName($r['hitlocation']) : "Schock";
            $modstr = ($r['mod'] > 0) ? $mods->getName($r['mod']) : "";

			$act = $a." (".$hitstr.") von ".$euidName." ".$r['health']." Schaden mit Waffe ".$weap." ".$modstr;
			
			if($r['action'] == "K")
			{
				$h = checkIfThatKD($r, $rr['type']);
				
			    $ply[$r['puid']]['TD'] = $r['team'];
		        $ply[$r['euid']]['TD'] = $r['eteam'];
		
				switch($h)
				{
					case 1 :
					{
						$ply[$r['puid']]['S']+=1;
						break;
					}
					case 2 :
					{
						$ply[$r['euid']]['T']+=1;
						break;
					}
					case 3 :
					{
						$ply[$r['euid']]['K']+=1;
						$ply[$r['puid']]['D']+=1;
						break;
					}
				}
			}
		}
		else if($r['action'] == "Weapon")
		{
			$act = "wechselt zu ".$weapons->getLogName($r['weapon']);
		}
		else if($r['action'] == "J" || $r['action'] == "L" || $r['action'] == "JT")
		{
			$act = "";
		}
		else if($r['action'] == 'say' || $r['action'] == 'sayteam')
		{
			$ids = $r['additional'];
			$db2->query_db("SELECT * FROM chats WHERE id = '$ids'");
			$cs = mysqli_fetch_array($db2->result);
			$act = $cs['message'];
		}	
		else
		{
			$act = $gameActions->getName($gameActions->getId($r['action']));
		}
		
		$time = secToTime($r['time']);
		
		if($r['action'] == 'InitGame:')
		{
			$time = "00:00";
		}
		
		
		$class = "";
		$icon = "";
		
		if($r['action'] == "J")
		{
			$class = "join";
			$icon = "pinch.png";
		}
		else if($r['action'] == "JT")
		{
			$class = "joinTeam";
			$icon = "team2.png";
		}
		else if($r['action'] == "L")
		{
			$class = "launch";
			$icon = "arrows7.png";		
		}
		else if($r['action'] == "Q")
		{
			$class = "launch";
			$icon = "door9.png";		
		}
		else if($r['action'] == "D")
		{
			$class = "damage";
			$icon = "damage.png";
		}
		else if($r['action'] == "K")
		{
			$class = "kill";
			$icon = "unicorn2.png";
		}
		else if($r['action'] == "Weapon")
		{
			$class = "Weapon";
			$icon = "gun9.png";
		}
		else if($r['action'] == "KC")
		{
			$icon = "mark9.png";
		}
		else if($r['action'] == "KD")
		{
			$icon = "cross-mark1.png";
		}
		else if($r['action'] == "BD" || $r['action'] == "BP" || $r['action'] == "BT"  || $r['action'] == "BL" || $r['action'] == "BPA")
		{
			$icon = "bomb13.png";
			$class = "flag";
		}
		else if($r['action'] == "BL" || $r['action'] == "BDA" || $r['action'] == "BDP" || $r['action'] == "BPP")
		{
			$icon = "bomb13.png";
			$class = "flag";
		}
		else if($r['action'] == "W")
		{
			$icon = "flag88.png";
		}
		else if($r['action'] == "InitGame:")
		{
			$icon = "transport305.png";
		}
		else if($r['action'] == "ExitLevel: executed")
		{
			$icon = "door9.png";
		}
		else if($r['action'] == "ShutdownGame")
		{
			$icon = "shut-down2.png";
		}
		else if($r['action'] == "FT")
		{
			$icon = "man22.png";
			$class = "flag";		
		}
		else if($r['action'] == "FR")
		{
			$icon = "astronaut8.png";
			$class = "flag";
		}
		else if($r['action'] == "FC")
		{
			$icon = "castles.png";
			$class = "flag";
		}
		else if($r['action'] == "W")
		{
			$icon = "celebration.png";
		}
		else if($r['action'] == "say" || $r['action'] == "sayteam")
		{
			$icon = "audio11.png";
		}
		
		$feed = '<div class="feed '.$class.'" id="feed'.$i.'">';
		$feed.= '<div><img class="icon" src="res/icons/'.$icon.'" /></div>';
		$feed.= '<div class="msg">'.$time.' - '.$r['action'].' - '.$puidName.' > '.$act.'</div>';
		$feed.= '</div>';
		
		
		array_push($arr, $feed);
		
		//$content.= "<tr><td>".$time."</td><td>".$r['action']."</td><td>".$r['team']."</td><td>".$players->getLogName($r['puid'])."</td><td>".$act."</td></tr>";
		
		$i--;
	}
	
	$feed = '<div class="feed '.$class.'" id="feed'.($count + 1).'">';
	$feed.= '<table width="350" cellpadding="0" cellspacing="0">
				<tr>
                    <td width="50">Team</td>
					<td width="150">Spieler</td>
					<td width="50">Kills</td>
					<td width="50">Teamkill</td>
					<td width="50">Suizid</td>
					<td width="50">Death</td>
				</tr>';
	
	
	//$arr = array_reverse($arr);
	//oki nun die spieler
    //array convertiren keys in separates feld
    $plyB = array();
    $c = count($ply);
    $keys = array_keys($ply);
    for($i=0;$i<$c;$i++)
    {
        $plyB[$i] = $ply[$keys[$i]];
        $plyB[$i]['id'] = $keys[$i];
    }

    //sortieren
    usort($plyB, function($a, $b) {
        return $a['K'] < $b['K'];
    });

	$c = count($plyB);

    //ausgeben
	for($i=0;$i<$c;$i++)
	{
        $playname = ($plyB[$i]['id'] != 0) ? $players->getLogName($plyB[$i]['id']) : "Mapkill";

		$contB.= "<tr><td>".$plyB[$i]['TD']."</td><td>".$playname."</td><td>".$plyB[$i]['K']."</td><td>".$plyB[$i]['T']."</td><td>".$plyB[$i]['S']."</td><td>".$plyB[$i]['D']."</td></tr>";
	}
	
	$feed.= $contB."</table></div>";
	
	array_push($arr, $feed);

	$feed = "";
	
	$count++;
	
	for($i=0;$i<$count;$i++)
	{
		$feed.= $arr[$i];
	}
	
	$feedAnimation = "triggerFeed(".$count.");";
}


?>
<html lang="de-DE">
	<head>
		<meta charset="iso-8859-1">
		<title>CoD4 Log Parser - Feed</title>
		
		<link rel="stylesheet" href="res/css/interface.css" />
		<script type="text/javascript" src="res/js/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="res/js/feed.js"></script>
		
	</head>
	
	<body>
		<p>Dient erst mal zur Kontrolle des Parsers. Wird ab dem 17.07 per Cronjob aktualisiert.</p>
		<form method="POST" action="">
			<?php
				echo buildRoundSelecltor();
			?>
		</form>
		<div>
		
			<?php
				echo $feed;
			?>
			
		</div>
		
	</body>
	
	<script type="text/javascript">
		<?php
		echo $feedAnimation;
		?>
	</script>
	
</html>