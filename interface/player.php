<?php
date_default_timezone_set('Europe/Berlin');

include('/var/customers/webs/Mirko/verwackeltes/test/cod4parser/core/misc/serv_db.inc.php');
include('/var/customers/webs/Mirko/verwackeltes/test/cod4parser/core/misc/helpers.php');
include '../core/stats/players.php';
include '../core/stats/chanllenges.php';

$starttime = microtime_float();

$db = new mbdb();

$t = array();

$content = "";
$player_id = -1;

function buildWeaponsTable($arr, $styles = NULL)
{
	$count = count($arr);
	$output = "";
	$class = "";

	if($count>0)
	{
		reset($arr);
		$num = count(current($arr));


		$output.= "<table class=\"".$class."\" align=\"".$styles['align']."\" border=\"".$styles['border']."\" cellpadding=\"".$styles['cellpadding']."\" cellspacing=\"".$styles['cellspacing']."\" width=\"".$styles['width']."\">\n";
		$output.= "<tr>\n";

		foreach(current($arr) as $key => $value)
		{
			$output.= "<th>";
			$output.= $key."&nbsp;";
			$output.= "</th>\n";
    	}

		$output.="</tr>\n";

		while ($curr_row = current($arr))
		{
			$output.= "<tr>\n";
			$col = 1;

			while (false !== ($curr_field = current($curr_row)))
			{
				$output.="<td>";
				
				if($col == 3)
				{
					$output.= extractWeaponGroupName($curr_field)."&nbsp;";
				}
				else
				{
					$output.= $curr_field."&nbsp;";
				}
				
				$output.="</td>\n";
				next($curr_row);
				$col++;
			}
			
			while($col <= $num)
			{
				$output.= "<td>&nbsp;</td>\n";
				$col++;
			}
			$output.= "</tr>\n";
			next($arr);
		}
		 $output.= "</table>\n";
	}

	return $output;
}




if(isset($_POST['player']))
{
	$player_id = $_POST['player'];
	
	$kills = getBasicPlayerKillStats($player_id);
	$deaths = getBasicPlayerDeathStats($player_id);
	$ratio = getKDRatio($kills['Kills'], $deaths['Kills']);
	
	
	$content.= 'Kills: '.$kills['Kills']."<br />";
	$content.= 'davon teamKills: '.$kills['TeamKills']."<br /><br />";
	$content.= 'Deaths: '.$deaths['Kills']."<br />";
	$content.= 'davon teamDeaths: '.$deaths['TeamKills']."<br />";
	$content.= 'davon Suizide: '.$deaths['Suizide']."<br /><br />";
	$content.= 'K/D Ratio: '.$ratio.'<br /><br /><br />';
	$content.= 'Meist abgeschossene Gegener:<br />';
	$content.= $db->show_in_table(getKillsByPlayerList($player_id));
	$content.= '<br /><br />';
	$content.= 'Meist abgeschossene durch Gegener:<br />';
	$content.= $db->show_in_table(getDeathsByPlayerList($player_id));
   	$content.= '<br /><br />';
	$content.= 'Teamkills bei Spieler<br />';
	$content.= $db->show_in_table(countTeamKillsPlayerList($player_id));
    $content.= '<br /><br />';
	$content.= 'TeamDeaths durch Spieler<br />';
	$content.= $db->show_in_table(countTeamDeathsByEnemyList($player_id));
    $content.= '<br /><br />';
	$content.= 'Kills nach Waffen (Typ zusamengefasst)<br />';
	$content.= buildWeaponsTable(getWeaponKillCountByType($player_id));
	$content.= "<br /><br />";
	$content.= 'Deaths nach Waffen (Typ zusamengefasst)<br />';
	$content.= buildWeaponsTable(getWeaponDeathsCountByType($player_id));
	$content.= "<br /><br />";
	$content.= 'Kills nach Waffen (aufgeschl&uuml;sselt)<br />';
	$content.= buildWeaponsTable(getWeaponKillCount($player_id));
	$content.= "<br /><br />";
	$content.= 'Deaths nach Waffen (aufgeschl&uuml;sselt)<br />';
	$content.= buildWeaponsTable(getWeaponDeathsCount($player_id));
	$content.= "<br /><br />";
	$content.= 'Gespielte Karten<br />';
	$content.= $db->show_in_table(getPlayerMapsCountList($player_id));
	$content.= "<br /><br />";
	$content.= 'Gespielte Speilarten:<br />';
	$content.= $db->show_in_table(getPlayerGametypeCountList($player_id));
    $content.= "<br /><br />";
	$content.= 'Summe erhaltener Schaden nach K&ouml;perregion:<br />';
    $content.= buildBody(getPlayerHitLocationsInflictedTotal($player_id));
	$content.= "<br /><br />";
	$content.= 'Summe zugef&uuml;gter Schaden nach K&ouml;perregion:<br />';
    $content.= buildBody(getPlayerHitLocationsRecievedTotal($player_id));
	$content.= "<br /><br />";
	$content.= 'Summe erhaltener Schaden nach K&ouml;perregion:<br />';
    $content.= $db->show_in_table(getPlayerHitLocationsRecievedTotal($player_id));
    $content.= "<br /><br />";
	$content.= 'Summe erhaltener Schaden:<br />';
    $content.= $db->show_in_table(sumDamageRecievedTotal($player_id));
    $content.= "<br /><br />";
	$content.= 'Alle Aktionen<br />(Gez&auml;hlt wird wenn der Spieler die Aktion getriggert hat<br />';
    $content.= $db->show_in_table(getPlayersActionCountList($player_id));

}
else
{
	$content = 'Anzahl der Spieler auf dem Server pro Tag:<br />';
	$content.= buildTable(getPlayersCountByTimeInterval('d'));
	$content.= '<br /><br />';
	$content.= 'Highscore:<br />';
	$content.= $db->show_in_table(getPlayersTotalPointsListed());
	$content.= "<br /><br />";
	$content.= 'Spieler nach Kills:<br />';
	$content.= $db->show_in_table(getPlayersTotalKillsListed());
	$content.= "<br /><br />";
	$content.= 'Spieler nach Deaths:<br />';
	$content.= $db->show_in_table(getPlayersTotalDeathsListed());
	$content.= "<br /><br />";
	$content.= 'Spieler nach Teamkills:<br />';
	$content.= $db->show_in_table(getPlayersTotalTeamKillsListed());
}

?>
<html lang="de-DE">
	<head>
		<meta charset="iso-8859-1">
		<title>CoD4 Log Parser - Spieler</title>

		<link rel="stylesheet" href="res/css/interface.css" >

	</head>
	<body>
		<nav role="main">
			<a href="http://k4f-in-berlin.de">K4F Home</a>&nbsp;<a href="server.php">Server Stats</a>&nbsp;<a href="index.php">Runden Stats</a>&nbsp;<a href="player.php">Spieler Stats</a>&nbsp;<a href="gametypes.php">Spielarten Stats</a>&nbsp;<a href="maps.php">Maps Stats</a>&nbsp;<a href="weapons.php">Waffen Stats</a>&nbsp;<a href="game.php">Koord Tests</a>&nbsp;<a href="challenges.php">Herausforderungen</a>
		</nav>
		<p>Test Player Statistiken</p>
		<form method="POST" action="">
			<?php
				echo buildPlayerSelector($player_id);
			?>
		</form>

		

		<div style="margin-top: 50px;">

			<?php echo $content; ?>

			<?php
			
				echo "<br /><br />".memory_get_peak_usage()." peak Mem | ".memory_get_usage()." norm Mem usage (bytes)<br />";
                echo "<br /><br />Gebrauchte Zeit: ".(microtime_float() - $starttime)." Sekunden";
			?>
		</div>

	</body>
</html>

