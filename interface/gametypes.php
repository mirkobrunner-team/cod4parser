<?php
date_default_timezone_set('Europe/Berlin');

include('/var/customers/webs/Mirko/verwackeltes/test/cod4parser/core/misc/serv_db.inc.php');
include('/var/customers/webs/Mirko/verwackeltes/test/cod4parser/core/misc/helpers.php');
include '../core/stats/gametypes.php';

$db = new mbdb();

$t = array();

$content = "";
$type_id = -1;


if(isset($_POST['types']) && $_POST['types'] > 0)
{
	$type_id = $_POST['types'];
	
	$content = "Wie oft auf Karte gespielt<br />";
	$content.= $db->show_in_table(getMapGametypeCount($type_id));
	$content.= "<br /><br />";
	
}
else
{
	$content = $db->show_in_table(getGameTypeCountList());
}

?>
<html lang="de-DE">
	<head>
		<meta charset="iso-8859-1">
		<title>CoD4 Log Parser - Gametypes</title>

		<link rel="stylesheet" href="res/css/interface.css" >

	</head>
	<body>
		<nav role="main">
			<a href="http://k4f-in-berlin.de">K4F Home</a>&nbsp;<a href="server.php">Server Stats</a>&nbsp;<a href="index.php">Runden Stats</a>&nbsp;<a href="player.php">Spieler Stats</a>&nbsp;<a href="gametypes.php">Spielarten Stats</a>&nbsp;<a href="maps.php">Maps Stats</a>&nbsp;<a href="weapons.php">Waffen Stats</a>&nbsp;<a href="game.php">Koord Tests</a>&nbsp;<a href="challenges.php">Herausforderungen</a>
		</nav>
		<p>Test Gametypes Statistiken</p>
		<form method="POST" action="">
			<?php
				echo buildGametypeSelector($type_id);
			?>
		</form>
		
		
		
		<div style="margin-top: 50px;">
			
			<?php echo $content; ?>
			
			<?php
			
				echo "<br /><br />".memory_get_peak_usage()." peak Mem | ".memory_get_usage()." norm Mem usage (bytes)<br />";
			?>
		</div>
		
	</body>
</html>
	
	