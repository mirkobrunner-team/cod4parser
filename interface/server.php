<?php
date_default_timezone_set('Europe/Berlin');

include('/var/customers/webs/Mirko/verwackeltes/test/cod4parser/core/misc/serv_db.inc.php');
include('/var/customers/webs/Mirko/verwackeltes/test/cod4parser/core/misc/helpers.php');
include '../core/stats/games.php';

$db = new mbdb();

$t = array();

$content = "";
$type_id = -1;

$content.= "<br /><br />";
$content.= "Anzahl der Spieler pro Tag:<br />";
$content.= buildTable(getPlayersCountByTimeInterval('d'));	
$content.= "<br /><br />";
$content.= "Bekannte Maps (Anzahl):<br />";
$content.= $db->show_in_table(countMaps());
$content.= "<br /><br />";
$content.= "Bekannte Spielarten (Anzahl):<br />";
$content.= $db->show_in_table(countGametypes());
$content.= "<br /><br />";
$content.= "Bekannte Spieler (Anzahl):<br />";
$content.= $db->show_in_table(countPlayers());
$content.= "<br /><br />";
$content.= "Bekannte SpawnPoints (Anzahl):<br />";
$content.= $db->show_in_table(countSpawnPoints());
$content.= "<br /><br />";
$content.= "Meist genutzte Waffe:<br />";
$content.= $db->show_in_table(getMostUsedWeapon());
$content.= "<br /><br />";
$content.= "Meist gespielte Karte:<br />";
$content.= $db->show_in_table(getMostPlayedMap());
$content.= "<br /><br />";
$content.= "Meist gespielter Spieltyp:<br />";
$content.= $db->show_in_table(getMostPlayedGametype());
$content.= "<br /><br />";
$content.= "gespielte Spiele:<br />";
$content.= $db->show_in_table(countGames());
$content.= "<br /><br />";
$content.= "gespielte Runden:<br />";
$content.= $db->show_in_table(countRounds());
$content.= "<br /><br />";
$content.= "getrackte Aktionen:<br />";
$content.= $db->show_in_table(countActions());
$content.= "<br /><br />";
$content.= "Gespielte Zeit gesammt:<br />";
$content.= $db->show_in_table(totalPlayTime());
$content.= "<br /><br />";
$content.= "L&auml;ngste Spiel ever:<br />";
$content.= $db->show_in_table(getLongestPlayedGame());
$content.= "<br /><br />";
$content.= "Kills gesammt:<br />";
$content.= $db->show_in_table(totalKillSum());
$content.= "<br /><br />";
$content.= "Treffer gesammt:<br />";
$content.= $db->show_in_table(totalHitsSum());
$content.= "<br /><br />";
$content.= "Actions  gesammt:<br />";
$content.= $db->show_in_table(getActionCountList());


?>
<html lang="de-DE">
	<head>
		<meta charset="iso-8859-1">
		<title>CoD4 Log Parser - Server</title>

		<link rel="stylesheet" href="res/css/interface.css" >

	</head>
	<body>
		<nav role="main">
			<a href="http://k4f-in-berlin.de">K4F Home</a>&nbsp;<a href="server.php">Server Stats</a>&nbsp;<a href="index.php">Runden Stats</a>&nbsp;<a href="player.php">Spieler Stats</a>&nbsp;<a href="gametypes.php">Spielarten Stats</a>&nbsp;<a href="maps.php">Maps Stats</a>&nbsp;<a href="weapons.php">Waffen Stats</a>&nbsp;<a href="game.php">Koord Tests</a>&nbsp;<a href="challenges.php">Herausforderungen</a>
		</nav>
		<p>Test Server Statistiken</p>

		
		<div style="margin-top: 50px;">
			
			<?php echo $content; ?>
			
			<?php
			
				echo "<br /><br />".memory_get_peak_usage()." peak Mem | ".memory_get_usage()." norm Mem usage (bytes)<br />";
			?>
		</div>
		
	</body>
</html>
	
	